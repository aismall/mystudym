package com.aismall.testHashMap;

import java.util.HashMap;


public class TestHashMap01 {
    public static void main(String[] args) {
        HashMap<String,String> hashMap=new HashMap<String, String>();
        hashMap.put("1","1");
        hashMap.put("2","2");

        for (String key:hashMap.keySet()) {
            if(key=="1"){
                System.out.println(key);
                hashMap.remove(key);
            }
            
        }

        //对应反编译的代码
        /*
        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("1", "1");
        hashMap.put("2", "2");

        //for循环反编译的代码，调用keySet()方法
        //keySet()方法返回一个KeySet类的实例，调用KeySet的iterator()方法
        //iterator()方法中返回了一个KeyIterator()的实例，该实例是专门用来遍历HashMap中Key的集合的
        //为了方便，遍历集合都有自己专属的迭代器，HashMap的是HashIterator，KeyIterator为HashIterator的子类
        //在new子类的时候，父类HashIterator中有个属性，获取了HashMap的modCount属性
        //======================================================================
        //hashMap的modCount属性初始值为0，每次put值的时候才会加1，所以现在这个值为2
        //expectedModCount = modCount=2;
         HashIterator() {
            expectedModCount = modCount;
            Node<K,V>[] t = table;
            current = next = null;
            index = 0;
            if (t != null && size > 0) { // advance to first entry
                do {} while (index < t.length && (next = t[index++]) == null);
            }
        }
        //======================================================================
        Iterator var2 = hashMap.keySet().iterator();
        //调用key专属迭代器中的方法，hasNext()，判断是否遍历结束

        while(var2.hasNext()) {
            //KeyIterator.next()中调用了父类的nextNode()方法
            //============================================
            final Node<K,V> nextNode() {
            Node<K,V>[] t;
            Node<K,V> e = next;
            //异常出现的位置，如果modCount != expectedModCount
            //我们在遍历的时候删除了有个key这时候modCount值会加一，造成不相等
            //也就是，我们在遍历的时候是不能修改HashMap的机构的，否则就会报并发修改异常
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
            if (e == null)
                throw new NoSuchElementException();
            if ((next = (current = e).next) == null && (t = table) != null) {
                do {} while (index < t.length && (next = t[index++]) == null);
            }
            return e;
            }
            //============================================
            String key = (String)var2.next();
            if (key == "1") {
                System.out.println(key);
                hashMap.remove(key);
            }
        }
        */
        //报的错误为并发修改异常
        /*
        * Exception in thread "main" java.util.ConcurrentModificationException
	      at java.util.HashMap$HashIterator.nextNode(HashMap.java:1445)
	      at java.util.HashMap$KeyIterator.next(HashMap.java:1469)
	      at com.aismall.testHashMap.TestHashMap01.main(TestHashMap01.java:11)
	      */
    }
}
