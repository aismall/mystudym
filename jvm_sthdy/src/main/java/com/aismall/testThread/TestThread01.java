package com.aismall.testThread;

import java.util.concurrent.TimeUnit;

//1.编写一个类实现Runnable接口
class TestThread implements Runnable{
    //2.重写父接口的run()方法
    public void run(){
        for (int i=0;i<3;i++){
            System.out.println("TestThread执行了。。。");
        }
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
public class TestThread01 {
    public static void main(String[] args) throws Exception {
        //3.创建Runnable子类的实例化对象
        TestThread t=new TestThread();
        //4.通过 Thread 类的 start()方法，启动多线程序
        new Thread(t).start();

        //在主线程中定义一个循环
        for(int i=0;i<3;i++){
            System.out.println("main执行了。。。");
            TimeUnit.MILLISECONDS.sleep(100);
        }
    }
}
