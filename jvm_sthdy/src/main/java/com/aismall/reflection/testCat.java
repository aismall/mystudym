package com.aismall.reflection;
public class testCat {
    public static void main(String[] args) throws ClassNotFoundException {
        // 一般都是通过配置文件读取，我们此处为了方便，直接写死
        String classPath="com.aismall.reflection.Cat";
        // 方式一
        Class cls1=Class.forName(classPath);
        System.out.println(cls1);    // class com.aismall.reflection.Cat

        //方式二
        Class cls2=Cat.class;
        System.out.println(cls2); //class com.aismall.reflection.Cat

        // 方式三
        Cat cat=new Cat();
        Class cls3=cat.getClass();
        System.out.println(cls3);   //class com.aismall.reflection.Cat

        //方式四：通过类加载器来获取类的对象
        // 先拿到Cat的类加载器
        ClassLoader classLoader=cat.getClass().getClassLoader();
        // 通过类加载器得到对应的对象
        Class cls4=classLoader.loadClass(classPath);
        System.out.println(cls4);     //class com.aismall.reflection.Cat

        System.out.println(cls1.hashCode());    //617901222
        System.out.println(cls2.hashCode());    //617901222
        System.out.println(cls3.hashCode());    //617901222
        System.out.println(cls4.hashCode());    //617901222

        //方式五：基本数据类型按照如下方式获得Class对象
        Class<Integer> cls5=int.class;
        System.out.println(cls5);

        //方式六：基本数据类型对应的包装类，可以通过包装类.TYPE来获取对应的Class对象
        Class cls6=Integer.TYPE;
        System.out.println(cls6);

        System.out.println(cls5.hashCode());    //1159190947
        System.out.println(cls6.hashCode());    //1159190947
    }
}
