package com.aismall.reflection;

public class Cat {
    //属性
    public String name;
    public int age;

    // 无参构造
    public Cat() {
    }

    //// 有参构造：一个参数
    //public Cat(String name) {
    //    this.name = name;
    //}
    //
    //// 有参构造：两个参数
    //public Cat(String name, int age) {
    //    this.name = name;
    //    this.age = age;
    //}

    public void  catchMouse(){
        System.out.println("猫爪老鼠");
    }
}
