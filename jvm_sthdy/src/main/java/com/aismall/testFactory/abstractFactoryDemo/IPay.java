package com.aismall.testFactory.abstractFactoryDemo;

public interface IPay {
    public void pay();
}
