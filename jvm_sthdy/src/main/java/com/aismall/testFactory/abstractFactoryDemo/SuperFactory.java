package com.aismall.testFactory.abstractFactoryDemo;

public class SuperFactory {
    public static IFactory getFactory(String type){
        if(type=="WeChat"){
            return new WeChatFactory();
        }else if (type=="ALi"){
            return new ALiFactory();
        }
        return null;
    }
}
