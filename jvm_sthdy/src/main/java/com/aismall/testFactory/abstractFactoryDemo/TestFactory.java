package com.aismall.testFactory.abstractFactoryDemo;

public class TestFactory {
    public static void main(String[] args) {
        IFactory weChatFactory=SuperFactory.getFactory("WeChat");
        weChatFactory.createPay().pay();
        weChatFactory.createFund().refund();

        IFactory aLiFactory=SuperFactory.getFactory("ALi");
        aLiFactory.createPay().pay();
        aLiFactory.createFund().refund();

    }
}
