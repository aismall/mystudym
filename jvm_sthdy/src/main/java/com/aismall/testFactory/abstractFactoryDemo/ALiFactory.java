package com.aismall.testFactory.abstractFactoryDemo;

public class ALiFactory implements  IFactory {
    @Override
    public IPay createPay() {
        return new ALiPay();
    }

    @Override
    public IRefund createFund() {
        return new ALiRefund();
    }
}
