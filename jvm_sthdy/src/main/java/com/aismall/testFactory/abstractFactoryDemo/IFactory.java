package com.aismall.testFactory.abstractFactoryDemo;

public interface IFactory {
    IPay createPay();
    IRefund createFund();
}
