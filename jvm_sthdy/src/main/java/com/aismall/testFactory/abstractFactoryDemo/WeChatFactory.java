package com.aismall.testFactory.abstractFactoryDemo;

public class WeChatFactory implements IFactory {
    @Override
    public IPay createPay() {
        return new WeChatPay();
    }

    @Override
    public IRefund createFund() {
        return new WeChatRefund();
    }
}
