package com.aismall.testClassLoader;

public class TestClassLoader01 {
    public static void main(String[] args) {
        //获取系统类加载器
        ClassLoader systemClassLoader=ClassLoader.getSystemClassLoader();
        System.out.println("系统类加载器："+systemClassLoader);
        //获取其上层的：扩展类加载器
        ClassLoader extensionClassLoader=systemClassLoader.getParent();
        System.out.println("拓展类加载器："+extensionClassLoader);
        //试图获取 根加载器
        ClassLoader bootstrapCLassloader=extensionClassLoader.getParent();
        System.out.println("引导类加载器："+bootstrapCLassloader);

        // 获取自定义加载器
        ClassLoader classLoader = TestClassLoader01.class.getClassLoader();
        System.out.println("自定义类加载器："+classLoader);

        // 获取String类型的加载器
        ClassLoader classLoader1 = String.class.getClassLoader();
        System.out.println("String类型的加载器："+classLoader1);
    }
}
