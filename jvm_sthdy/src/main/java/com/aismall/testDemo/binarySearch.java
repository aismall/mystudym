package com.aismall.testDemo;

import java.util.Arrays;

public class binarySearch {
    /*
    * Java中提供了一个获取随机数的一个类（Math）
    * Math：此类在java.lang包下，jvm会自动导入，所以无需import导包
    * 生成随机数要使用Math类下的方法：random()　　方法的返回值是[0.0 - 1.0)
    * 1.获取上述范围内的随机数：
    *   double d = Math.random();
    * 2.强转为整形
    *   int i = (int)(Math.random());
    * 3.获取一个1~100之间的随机数（int型）
    *   int num = (int)(Math.random()*100+1);
    * 4.获取一个任意范围（n~m）之间的随机整数（int型）
    *  int num = (int)(Math.random()*(m-n+1)+m);
    *  注：一定要大数减去小数
    * */
    public static void main(String[] args) {
        int[] arr=new int[20];
        for(int i=0;i<arr.length;i++){
            arr[i]=(int)(Math.random()*100+1);
        }
        Arrays.sort(arr);
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
        System.out.println("================================");

        int result=binarySearchDemo(arr,58);
        System.out.println(result);

    }
    public static  int binarySearchDemo(int[] arr,int key){
        int left=0;
        int right=arr.length-1;
        while(left<=right){
            int mid=(left+right)/2;
            if(arr[mid]<key){
                left=mid+1;
            }else if(arr[mid]>key){
                right=mid-1;
            }else {
                return arr[mid];
            }
        }
        return -1;
    }
}
