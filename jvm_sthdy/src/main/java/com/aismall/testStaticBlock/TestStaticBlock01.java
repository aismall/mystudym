package com.aismall.testStaticBlock;

public class TestStaticBlock01 {
    public static int a=1;
    static{
        a=2;
    }

    public static void main(String[] args) {
        TestStaticBlock01.a=3;
        System.out.println(TestStaticBlock01.a);
    }
}
