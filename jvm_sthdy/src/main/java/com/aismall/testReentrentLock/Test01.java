package com.aismall.testReentrentLock;

import java.util.concurrent.TimeUnit;

public class Test01 {
    static Object objectLock = new Object();

    public static void main(String[] args) {
        new Thread(() -> {
            synchronized (objectLock) {
                System.out.println(Thread.currentThread().getName() + "运行");
                try {
                    System.out.println(Thread.currentThread().getName() + "阻塞");
                    objectLock.wait(); // 等待
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() +  "被唤醒");
            }
        }, "线程1").start();

        new Thread(() -> {
            try {
                //使线程1先获得执行权
                TimeUnit.SECONDS.sleep(3L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (objectLock) {
                objectLock.notify(); // 唤醒
                System.out.println(Thread.currentThread().getName() +  "唤醒阻塞线程");
            }
        }, "线程2").start();
    }
}
