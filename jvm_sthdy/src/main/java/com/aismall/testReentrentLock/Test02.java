package com.aismall.testReentrentLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Test02 {
    static Lock lock = new ReentrantLock();
    static Condition condition = lock.newCondition();

    public static void main(String[] args) {
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName() +"运行");
                try {
                    System.out.println(Thread.currentThread().getName() +"阻塞");
                    condition.await();//阻塞
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"被唤醒");
            } finally {
                lock.unlock();
            }
        }, "线程1").start();


        new Thread(() -> {
            try {
                //使线程1先获得执行权
                TimeUnit.SECONDS.sleep(3L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.lock();
            try {
                condition.signal(); //唤醒
                System.out.println(Thread.currentThread().getName() +  "唤醒阻塞线程");
            } finally {
                lock.unlock();
            }
        }, "线程2").start();
    }
}
