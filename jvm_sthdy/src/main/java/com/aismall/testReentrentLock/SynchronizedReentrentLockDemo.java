package com.aismall.testReentrentLock;

public class SynchronizedReentrentLockDemo {
    public  void methodA(){
        System.out.println(Thread.currentThread().getName()+"=========外层锁==========");
    }
    public  void methodB(){
        System.out.println("=========内层锁==========");
    }

    public static void main(String[] args) {
        SynchronizedReentrentLockDemo SBL=new SynchronizedReentrentLockDemo();
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (SBL){
                    System.out.println(Thread.currentThread().getName()+"获得锁");
                    SBL.methodA();
                    synchronized (SBL){
                        SBL.methodB();
                    }
                    System.out.println(Thread.currentThread().getName()+"释放锁");
                }
            }
        },"线程1");
        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (SBL){
                    System.out.println(Thread.currentThread().getName()+"获得锁");
                    SBL.methodA();
                    synchronized (SBL){
                        SBL.methodB();
                    }
                    System.out.println(Thread.currentThread().getName()+"释放锁");
                }
            }
        },"线程2");
        t1.start();
        t2.start();
    }
}
