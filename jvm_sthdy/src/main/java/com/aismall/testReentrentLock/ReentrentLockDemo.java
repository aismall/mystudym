package com.aismall.testReentrentLock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrentLockDemo {
    public  void methodA(){
        System.out.println("=========外层锁==========");
    }
    public  void methodB(){
        System.out.println("=========内层锁==========");
    }
    // 创建一个锁实例
    static Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        ReentrentLockDemo RLD=new ReentrentLockDemo();
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();
                System.out.println(Thread.currentThread().getName()+"获得锁");
                RLD.methodA();
                lock.lock();
                RLD.methodB();
                lock.unlock();
                lock.unlock();
                System.out.println(Thread.currentThread().getName()+"释放锁");
            }
        },"线程1");
        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();
                System.out.println(Thread.currentThread().getName()+"获得锁");
                lock.unlock();
                System.out.println(Thread.currentThread().getName()+"释放锁");
            }
        },"线程2");

        t1.start();
        t2.start();
    }
}
