package com.aismall.testReentrentLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class testLockSupport {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "运行");
            System.out.println(Thread.currentThread().getName() + "阻塞");
            LockSupport.park(); // 线程A调用一次park()方法
            LockSupport.park(); // 线程A调用两次park()方法
            System.out.println(Thread.currentThread().getName() + "被唤醒");
        }, "线程1");

        Thread t2=new Thread(() -> {
            try {
                //使线程1先获得执行权
                TimeUnit.SECONDS.sleep(3L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            LockSupport.unpark(t1); // B 线程唤醒线程 A
            /*try {
                //使线程1先获得执行权
                TimeUnit.SECONDS.sleep(3L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            LockSupport.unpark(t1); // B 线程唤醒线程 A
            System.out.println(Thread.currentThread().getName() + "唤醒阻塞线程");
        }, "线程2");

        t1.start();
        t2.start();
    }
}
