package com.aismall.testConcurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

public class testAtomicLongAndLongAdder {
    public static void main(String[] args) throws Exception {
        testAtomicLongAdder(1, 10000000);
        testAtomicLongAdder(10, 10000000);
        testAtomicLongAdder(100, 10000000);
    }

    static void testAtomicLongAdder(int threadCount, int times) throws Exception {
        //打印创建的线程数和加一的次数
        System.out.println("threadCount: " + threadCount + ", times: " + times);
        //获取当前系统时间
        long start = System.currentTimeMillis();
        //调用：测试LongAdder类
        testLongAdder(threadCount, times);
        //获取执行代码的总时间：差值
        System.out.println("LongAdder 耗时：" + (System.currentTimeMillis() - start) + "ms");
        //打印创建的线程数和加一的次数
        System.out.println("threadCount: " + threadCount + ", times: " + times);
        //获取当前系统时间
        long atomicStart = System.currentTimeMillis();
        //测试AtomicLong类
        testAtomicLong(threadCount, times);
        //获取执行代码的总时间：差值
        System.out.println("AtomicLong 耗时：" + (System.currentTimeMillis() - atomicStart) + "ms");
        System.out.println("----------------------------------------");
    }

    //测试AtomicLong类
    static void testAtomicLong(int threadCount, int times) throws Exception {
        //创建一个AtomicLong的实例
        AtomicLong atomicLong = new AtomicLong();
        //创建一个集合用来存储线程
        List<Thread> list = new ArrayList();
        //创建threadCount个数的线程
        for (int i = 0; i < threadCount; i++) {
            //将创建的线程存入集合
            list.add(new Thread(()->{
                //在线程中调用加一操作
                for(int j = 0; j < times; j++) {
                    atomicLong.incrementAndGet();
                }
            }));
        }
        //使用增强for循环遍历集合中的线程实例，启动线程
        for (Thread thread : list) {
            thread.start();
        }
        //等待这些线程死亡
        for (Thread thread : list) {
            thread.join();
        }
        //打印这个值
        System.out.println("AtomicLong value is : " + atomicLong.get());
    }
    //测试LongAdder类
    static void testLongAdder(int threadCount, int times) throws Exception {
        LongAdder longAdder = new LongAdder();
        List<Thread> list = new ArrayList();
        for (int i = 0; i < threadCount; i++) {
            list.add(new Thread(() -> {
                for (int j = 0; j < times; j++) {
                    longAdder.increment();
                }
            }));
        }

        for (Thread thread : list) {
            thread.start();
        }

        for (Thread thread : list) {
            thread.join();
        }

        System.out.println("LongAdder value is : " + longAdder.longValue());
    }
}
