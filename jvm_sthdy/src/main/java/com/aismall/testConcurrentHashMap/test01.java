package com.aismall.testConcurrentHashMap;

import sun.misc.Unsafe;

import java.util.concurrent.ConcurrentHashMap;

/*测试Integer的一个方法：*/
public class test01 {
    public static void main(String[] args) {
        /*
         * Integer.numberOfLeadingZeros(scale)
         *   Integer是32为的二进制数
         *   numberOfLeadingZeros(scale)：返回前面0的个数
         *       例如：0000 0000 0000 0000 0000 0000 0000 0001=1
         *            numberOfLeadingZeros(1)=31
         *            0000 0000 0000 0000 0000 0000 0000 0011=3
         *            0000 0000 0000 0000 0000 0000 0000 001=2
         *            numberOfLeadingZeros(2)=30
         *            numberOfLeadingZeros(3)=30
         *
         * ASHIFT = 31 - Integer.numberOfLeadingZeros(1)=31-31=0;
         * 也就是，如果是1，就不用移位
         * ASHIFT = 31 - Integer.numberOfLeadingZeros(3)=31-30=1
         * ASHIFT = 31 - Integer.numberOfLeadingZeros(2)=31-30=1
         * 也就是，左移移位
         *
         * 保证了都是2的幂
         * */
        System.out.println(31-Integer.numberOfLeadingZeros(1));
        System.out.println(31-Integer.numberOfLeadingZeros(2));
        System.out.println(31-Integer.numberOfLeadingZeros(3));

        System.out.println("===================================");
        int num=Integer.numberOfLeadingZeros(16) | (1 << (16 - 1));
        int num111=(num<<16);
        System.out.println(num111);
        System.out.println(Integer.toBinaryString(num111));
        System.out.println(Integer.toBinaryString(2145714176));
        System.out.println(Integer.toBinaryString(num));

        System.out.println("===================================");
        int num3=-123;
        int num4=num3>>>3;
        System.out.println(num3);
        System.out.println(num4);
        System.out.println(Integer.toBinaryString(num3));
        System.out.println(Integer.toBinaryString(num4));


    }
}
