package com.aismall.springActiveMQ;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.TextMessage;

@Service
public class SpringMQ_Produce {
    @Autowired
    private JmsTemplate jmsTemplate;

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-activemq.xml");
        SpringMQ_Produce springMQ_producer = (SpringMQ_Produce) applicationContext.getBean("springMQ_Produce");
        //此处使用了lambda表达式
        springMQ_producer.jmsTemplate.send((session) -> {
            TextMessage textMessage=session.createTextMessage("*****spring和ActiveMQ的整合case*******");
            return  textMessage;

        });
        System.out.println("********send task over");
    }
}


