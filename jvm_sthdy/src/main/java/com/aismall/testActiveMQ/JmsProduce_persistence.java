package com.aismall.testActiveMQ;

import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.*;

//持久化topic 的消息生产者
public class JmsProduce_persistence {
    public static final String ACTIVEMQ_URL = "tcp://127.0.0.1:61616";
    public static final String TOPIC_NAME = "topic_persistence";
    public static final String QUEUE_NAME= "queue_persistence";

    public static void main(String[] args) throws  Exception{
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        Connection connection = activeMQConnectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue queue=session.createQueue(QUEUE_NAME);
        MessageProducer messageProducer = session.createProducer(queue);
        //Topic topic = session.createTopic(TOPIC_NAME);
        //MessageProducer messageProducer = session.createProducer(topic);


        // 设置持久化topic
        messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);
        // 设置持久化topic之后再，启动连接
        connection.start();
        for (int i = 1; i < 4 ; i++) {
            TextMessage textMessage = session.createTextMessage("topic_name--" + i);
            messageProducer.send(textMessage);
            MapMessage mapMessage = session.createMapMessage();
        }
        messageProducer.close();
        session.close();
        connection.close();
        System.out.println("  **** TOPIC_NAME消息发送到MQ完成 ****");
        System.out.println("  **** QUEUE_NAME消息发送到MQ完成 ****");
    }
}

