package com.aismall.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
/**
 * 1、 取出一个字符串中字母出现的次数。如：字符串："abcdekka27qoq" ，
 * 输出格式为：a(2)b(1)k(2)...
 * 思路:
 * 1.需要知道每个字符出现的次数,就要对每个字符进行遍历.
 * 2.如何对遍历出来的字符和对应出现的次数进行存储呢?就需要map集合.
 * 3.这里一个问题就是,如何把每个字符出现的总次数装入map中呢?
 * 就需要通过一个判断,如果这个字符不存在,就将key以及对应value存入，如果已经存在,将value值加1。
 * 4.当这个map集合把元素都存入之后，再通过EntrySet()将这个关系对遍历出来.然后再分别获取key和value.最后打印.
 * 总结:
 * 1.对于出现了有映射或者对应关系的元素时,必须想到要用到map集合.
 * 2.对于要查询出现的次数,必须要能够想到需求遍历.
 * */
public class Demo01 {
    public static void main(String[] args) {
        String s = "fdskai2353425hfdsakhfdska";
        Map<Character, Integer> map = StringList(s); //接受一个字符串,返回一个map集合.
        String str=ListMap(map);
        System.out.print(str);
    }

    public static String ListMap(Map<Character, Integer> map) {//遍历map
        StringBuilder stringBuilder = new StringBuilder();
        //返回map里面的全部key的集合，然后去除重复的元素
        Iterator<Character> it = map.keySet().iterator();

        //it.hasNext()判断集合中是否存在值；如果存在就it.next()
        while (it.hasNext()) {
            //就是取得当前集合的元素 然后把指针往后度移一位指向下一个元素
            Character key = it.next();
            //返回map里面指定键映射的值
            Integer value = map.get(key);
            //打印结果
            stringBuilder.append(key + "-->" + value+" ");
        }
        return stringBuilder.toString();
    }

    public static Map<Character, Integer> StringList(String s) {  //将字符串遍历
        char[] ch = s.toCharArray();
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for (int x = 0; x < ch.length; x++) {
            int a = 0;  //定义一个变量,每次都是从x角标在y循环中获取次数.
            for (int y = 0; y < ch.length; y++) {
                if (ch[x] == ch[y]) {
                    a++;  //如果相同了,就让变量加一.
                }
            }
            if (!map.containsKey(ch[x])) //如果不存在才装进来.保证这个value是最大的次数值.
            {
                map.put(ch[x], a);
            }
        }
        return map;
    }
}
