package com.aismall.test;

import java.util.List;

public class Qri {
    public String IDNumber;
    public String searchType;

    public String getIDNumber() {
        return IDNumber;
    }

    public void setIDNumber(String IDNumber) {
        this.IDNumber = IDNumber;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public List<String> getSendMessageTime() {
        return sendMessageTime;
    }

    public void setSendMessageTime(List<String> sendMessageTime) {
        this.sendMessageTime = sendMessageTime;
    }

    public String receiptNumber;
    public List<String> sendMessageTime;

    @Override
    public String toString() {
        return "Qri{" +
                "IDNumber='" + IDNumber + '\'' +
                ", searchType='" + searchType + '\'' +
                ", receiptNumber='" + receiptNumber + '\'' +
                ", sendMessageTime=" + sendMessageTime +
                '}';
    }
}
