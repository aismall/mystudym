package com.aismall.test;

import java.util.ArrayList;
import java.util.Scanner;

public class Demo4 {
    public static void main(String[] args) {
        ArrayList<Integer>  arrayList=new ArrayList<>();
        Scanner in=new Scanner(System.in);
        System.out.println("请输入一个表示长度的数字");
        int num=in.nextInt();
        for(int i=0;i<num;i++){
            System.out.println("请输入一个数值");
            int temp=in.nextInt();
            arrayList.add(temp);
        }
        System.out.println(countNum(arrayList));

    }
    public static int countNum(ArrayList<Integer> arrayList){
        int max=arrayList.get(0);
        int count=0;
        for (int j=1;j<arrayList.size();j++){
            if(arrayList.get(j)>max){
                max=arrayList.get(j);
            }
        }
        for(int p=0;p<arrayList.size();p++){
            if(max>arrayList.get(p)){
                count++;
            }
        }
        return count;
    }
}
