package com.aismall.test;

import java.util.ArrayList;
import java.util.List;

public class Demo02 {
    public static void main(String[] args) {
        String data="123,132,12313,45646,78978,12313*2";
        Qri qri=method(data);
        qri.toString();
    }
    public static Qri method(String data) {
        Qri qri = new Qri();
        String[] split = data.split(",");
        qri.setIDNumber(split[1]);
        qri.setSearchType(split[2]);
        qri.setReceiptNumber(split[3]);
        List<String> list = new ArrayList<>();
        for (int i = 4; i < split.length; i++) {
            list.add(split[i]);
        }
        qri.setSendMessageTime(list);
        int index=split.length-1;
        String[] strs=split[index].split("[*]");
        String str=strs[1];
        System.out.println(str);
        return qri;
    }
}
