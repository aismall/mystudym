package com.aismall.helloworld.mq;

import com.aismall.helloworld.config.MQConfig;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import sun.plugin2.message.Message;

/**
 * @ClassName MQReceiver
 * @Description TODO
 * @Author @AISMALL
 * @Date 2022/8/27 21:44
 */
@Component
public class MQReceiver {
    @RabbitListener(queues = MQConfig.FANOUT_QUEUE1)
    public void receiver(String msg, Channel channel) {
        // 只包含发送的消息
        System.out.println("receiver消费成功:" + msg);
        // channel 通道信息
        // message 附加的参数信息
    }
}
