package com.aismall.helloworld.mq;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.plugin2.message.Message;

import java.util.UUID;

/**
 * @ClassName MQSend
 * @Description TODO
 * @Author @AISMALL
 * @Date 2022/8/27 21:42
 */
@Component
public class MQSender{
    // 交换机
    public static final String FANOUT_EXCHANGE = "fanout.exchange";
    // 队列
    public static final String FANOUT_QUEUE1 = "fanout.queue1";
    @Autowired
    private RabbitTemplate rabbitTemplate;
    //发送消息，不需要实现任何接口，供外部调用。
    public void send(String message) {
        CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend(FANOUT_EXCHANGE, FANOUT_QUEUE1, message, correlationId);
        System.out.println("消息发送成功 : " + message);
    }
}