package com.aismall.helloworld.utils.mq;


import com.rabbitmq.client.*;
import java.io.IOException;
/*
 * 消费者
 * */
public class MQRevc {
    //消费的队列
    private final static String QUEUE_NAME = "hello";
    public static void main(String[] arg) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setPort(5672);
        factory.setVirtualHost("Vhost01");
        factory.setUsername("admin");
        factory.setPassword("admin");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        //创建并声明一个队列，如果队列存在则使用这个队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 定义一个消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String mess = new String(body);
                System.out.println("接收到消息" + mess);
            }
        };
        // 创建一个消费者监听器
        // 确认我们是否收到消息，true是自动确认，false是手动确认
        String res = channel.basicConsume(QUEUE_NAME, true, consumer);
        System.out.println("消费成功：" + res);
    }
}
