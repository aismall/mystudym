package com.aismall.helloworld.utils.tools;

import com.aismall.helloworld.entity.EMailMessage;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Date;
import java.util.Properties;
// 需要导入的包：spring-boot-starter-mail
// 不是组件无法注入，不注入，测试就会报错，先这样写吧。
@Component
public class SendSimpleEMail {
    // 邮件发送器：JavaMailSender接口的实现类
    @Autowired
    private JavaMailSenderImpl javaMailSenderImpl;
    @Value("${spring.mail.port}")
    private int port;
    @Value("${spring.mail.host}")
    private String host;
    @Value("${spring.mail.username}")
    private String username;
    @Value("${spring.mail.password}")
    private String password;
    // 该方法为判断方法
    public void checkMail(EMailMessage emailMessage) {
        Assert.notNull(emailMessage,"邮件请求不能为空");
        Assert.notNull(emailMessage.getSendTo(), "邮件收件人不能为空");
        Assert.notNull(emailMessage.getSubject(), "邮件主题不能为空");
        Assert.notNull(emailMessage.getText(), "邮件内容不能为空");
    }
    /*
    * 发送不带格式的简单邮件
    * */
    public void sendSimpleMail(EMailMessage emailMessage) {
        javaMailSenderImpl.setHost(host);
        javaMailSenderImpl.setPort(port);
        javaMailSenderImpl.setUsername(username);
        javaMailSenderImpl.setPassword(password);
        // 简单邮件对象
        SimpleMailMessage message = new SimpleMailMessage();
        //检验传入参数是否符合条件
        checkMail(emailMessage);
        //邮件发件人
        message.setFrom(username);
        //邮件收件人 1或多个
        message.setTo(emailMessage.getSendTo().split(","));
        //邮件主题
        message.setSubject(emailMessage.getSubject());
        //邮件内容
        message.setText(emailMessage.getText());
        //邮件发送时间
        message.setSentDate(new Date());
        //使用邮件发送器发送邮件
        javaMailSenderImpl.send(message);
    }
    /*
    * 可以发送带HTML格式的复杂邮件，如果传入文件地址，还可以发送附件
    * */
    public void sendHtmlMail(EMailMessage emailMessage, String filePath) {
        javaMailSenderImpl.setHost(host);
        javaMailSenderImpl.setPort(port);
        //SMTP验证时，需要用户名和密码
        javaMailSenderImpl.setUsername(username);
        javaMailSenderImpl.setPassword(password);
        // 用于连接邮件服务器的参数配置：也可在配置文件中配置（此处配置会覆盖配置文件中的配置）
        Properties prop = new Properties();
        prop.setProperty("mail.smtp.starttls.enable", "true");
        // 需要验证用户名密码
        prop.setProperty("mail.smtp.auth", "true");
        prop.setProperty("smtp.starttls.required", "true");
        prop.setProperty("mail.smtp.connectiontimeout", "10000");
        prop.setProperty("mail.smtp.timeout", "10000");
        //如果要密码验证,这里必须加这些参数,不然会报错
        javaMailSenderImpl.setJavaMailProperties(prop);
        //复杂邮件对象：支持更加复杂的邮件格式和内容
        MimeMessage mimeMessage = javaMailSenderImpl.createMimeMessage();
        //检验传入参数是否符合条件
        checkMail(emailMessage);
        try {
            // 创建MimeMessageHelper对象，处理MimeMessage的辅助类
            // msg:发送的邮件信息，true:是否为HTML格式的文件，utf-8:设置编码格式(因为发送html格式时、内容可能出现乱码)
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            //邮件发件人
            messageHelper.setFrom(username);
            //使用辅助类MimeMessage设定参数
            messageHelper.setSubject(emailMessage.getSubject());
            //邮件收件人 1或多个
            messageHelper.setTo(emailMessage.getSendTo().split(","));
            //为true可以发送转义 HTML
            messageHelper.setText(emailMessage.getText(), true);
            // 添加日期
            messageHelper.setSentDate(new Date());
            //String filePath = emailMessage.getFilePath();
            //if (StringUtils.hasText(filePath)) {
            //    FileSystemResource file = new FileSystemResource(new File(filePath));
            //    String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
            //    messageHelper.addAttachment(fileName,file);
            //}
            javaMailSenderImpl.send(mimeMessage);
        } catch (MessagingException e) {
            System.out.println(e+"邮件发送异常");
        }
    }
}
