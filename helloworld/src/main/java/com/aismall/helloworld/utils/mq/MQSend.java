package com.aismall.helloworld.utils.mq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
/*
*  如果发送消息的时候，不给队列绑定交换机，则默认将消息发送到default交换机之中
* */
public class MQSend {
    // 定义队列的名字
    private final static String QUEUE_NAME = "hello";
    public static void main(String[] arg) throws Exception {
        // 连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // MQ服务器的IP地址
        factory.setHost("localhost");
        // 端口号
        factory.setPort(5672);
        // MQ服务器里面的虚拟主机
        factory.setVirtualHost("Vhost01");
        // 虚拟主机隶属用户的账号
        factory.setUsername("admin");
        // 虚拟主机隶属用户的密码
        factory.setPassword("admin");
        //通过工厂获取链接
        Connection connection = factory.newConnection();
        // 从连接中创建通道
        Channel channel = connection.createChannel();
        //创建并声明一个队列，如果队列存在则使用这个队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //消息内容
        String message = "Hello World!";
        //发送消息：第一个参数可以设置交换机的名字，不设定发送到default交换机之中
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        channel.close();
        connection.close();
        System.out.println(" Send '" + message + "'");
    }
}
