package com.aismall.helloworld.utils.tools;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/*
* 将json数组转换为字符创数组，然后存入map中
* */
public class JsonArrayToStringList {
    // 需要导入的包：com.alibaba:fastjson:1.2.72
    public static void main(String[] args) {
        String abc = "[{\"ID\":\"4\",\"Name\":\"张山\",\"JoinDate\":\"2019-05-16\",\"loginName\":\"81000004\",\"No\":\"81000004\"},{\"ID\":\"6\",\"Name\":\"李四\",\"JoinDate\":\"2019-05-16\",\"loginName\":\"81000007\",\"No\":\"81000007\"}]";
        //将集合转成JSONArray
        JSONArray jsonArray = JSONArray.parseArray((String) abc);
        //打印JSONArray数组
        System.out.println("jsonArray =" + jsonArray);
        List<String> tempStringList = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {  //遍历JSONArray数组
            tempStringList.add(jsonArray.get(i).toString());
        }
        System.out.println("===================================");
        Map<String, Object> tempMap1 = null;
        Map<String, Object> tempMap2 = null;

        tempMap1 = JSON.parseObject(tempStringList.get(0));
        tempMap2 = JSON.parseObject(tempStringList.get(1));
        System.out.println(tempMap1);
        System.out.println(tempMap2);
    }
}
