package com.aismall.helloworld.controller;

import com.aismall.helloworld.mq.MQSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName SendMqController
 * @Description TODO
 * @Author @AISMALL
 * @Date 2022/8/27 22:02
 */
@RestController
public class SendMqController {
    @Autowired
    private MQSender mqSender;
    //测试Mq
    @GetMapping(value = "helloworld/mqsend")
    public void  mqSend() {
        String message = "测试消息：helloworld";
        mqSender.send(message);
        System.out.println("SendMqController执行成功！！！！！");
    }
}
