package com.aismall.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = "com.aismall")
public class HelloWorldApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(HelloWorldApplication.class, args);
        int count = run.getBeanDefinitionCount();
        String [] strings = run.getBeanDefinitionNames();
        System.out.println("自动配置Bean的数量" + count);
    }
}
