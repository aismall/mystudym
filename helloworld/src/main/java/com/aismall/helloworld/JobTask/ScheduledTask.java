package com.aismall.helloworld.JobTask;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import com.xxl.job.core.handler.IJobHandler;
import jdk.internal.instrumentation.Logger;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @ClassName ScheduledTask
 * @Description TODO
 * @Author @AISMALL
 * @Date 2022/8/28 12:45
 */
@Slf4j
@Component
// <version>2.0.1</version>：使用下面的注解
//@JobHandler(value = "testJob")
public class ScheduledTask {

    /**
     * 每1分钟扫描一次，扫描到就执行一次
     */
    @XxlJob("testJob")
    public ReturnT<String> testJob() {
        log.info("测试Job执行成功");
        return ReturnT.SUCCESS;
    }
}
