import com.aismall.helloworld.HelloWorldApplication;
import com.aismall.helloworld.entity.EMailMessage;
import com.aismall.helloworld.utils.tools.SendSimpleEMail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = HelloWorldApplication.class)
@RunWith(SpringRunner.class)
public class ToolsTest {
    @Autowired
    private SendSimpleEMail sendSimpleEMail;
    // 测试 SendSimpleMail.sendSimpleMail
    @Test
    public void test1(){
        EMailMessage emailMessage = new EMailMessage();
        emailMessage.setSendTo("xxxxxxxx@qq.com");
        emailMessage.setSubject("测试邮件");
        emailMessage.setText("测试邮件Text");
        sendSimpleEMail.sendSimpleMail(emailMessage);
        System.out.println("邮件发送成功");
    }

    // 测试 SendSimpleMail.sendHtmlMail
    @Test
    public void test2(){
        EMailMessage emailMessage = new EMailMessage();
        emailMessage.setSendTo("xxxxxxxxx@qq.com");
        emailMessage.setSubject("测试邮件");
        emailMessage.setText("<body><h1>Hello HTML格式的邮件内容</h1><body>" + "HTML格式的内容区");
        sendSimpleEMail.sendHtmlMail(emailMessage,null);
        System.out.println("邮件发送成功");
    }
}
