# Spring
## 给容器中注册组件的注解
方式一：包扫描+组件注解（只添加组件注解，如果不扫描，是不会被添加到Spring容器中的）
- 包扫描：ComponentScan
- 组件注解：Controller、Service、Repository、Component

方式二：包扫描+Configuration+Bean（只能添加到方法上）
- 包扫描：ComponentScan
- 配置类注解：Configuration
- 配置类中配置bean组件：bean

方式三：包扫描+Import注解
- 包扫描：ComponentScan
- 可以直接通过该注解将bean组件注入到容器中：Import({User.class,Person.class})

方式四：使用FactoryBean工厂，不推荐使用，太麻烦

注意：
- 以上只是注册组件的方式，只要启动SpringBoot项目时，这些注解不被扫描到，一个都不起作用。 
- 另外，SpringBoot项目有一个默认的扫描规则

- 我们可以这样配置，让SpringBoot扫到我们的配置类，我们在配置类中添加扫描到其他注解

可以指定包扫描的规则，结合Filter注解

## Configuration
Configuration注解：表示这个类是Spring的一个配置类，可以通过配置类，向Spring容器注入一些bean。

可以用来替代之前使用xml配置bean的方式。

## Bean
该注解只能标注在方法上，不能标注在类上，标注在方法上使用这个方法将某个bean注入到容器，一般要和 Configuration注解连用

## Scope
可以和Component注解一起使用，用来指定一个组件的生命

可以和Bean注解一起使用，用来指定一个Bean的声明周期

## Lazy
可以和Bean注解一起使用，用于指定一个bean的加载时机

## Conditional
按照一定的条件来加载，可以和Bean注解一起使用，用于指定，按需加载一个Bean（当某个bean依赖另外一个bean的时候，可在这个bean上面添加条件注解，如果依赖的bean的存在就报错）

如果注解标注在某个配置类上，如果条件不成立，整个配置类就不生效。

也可以可其他注解一起使用，用于指定按需加载。

衍生注解：ConditionalOnBean，ConditionalOnMissingBean

## ImportResource
ImportResource("classpath:beans.xml")：放在配置类上面，将配置文件（xml）中的bean注入到该配置类中，通过该配置类将这些bean注入到容器中。

## Spring中bean的生命周期
bean的生命周期就是：bean的创建-初始化-销毁的过程

Spring中所有bean都是由Spring容器来管理，如果我们想要在bean的生命周期中添加一些操作是可以的，我们可以定义一些方法通过注解的方式让它在Bean的生命周期的各个阶段执行，类似于生命周期钩子函数。

具体方式就不演示了，可以百度，需要注意的是，如果我使用继承bean的前置或者后置处理器，然后重写里面的方法的形式，来介入bean的声明周期，需要把这些类注入到spring容器中，例如使用Component注解等。

## value
放在定义的属性上面给属性赋值

## PropertySource
用于指定加载配置文件

可以和value注解一起使用，一个加载配置文件，一个从配置文件中获取值，传递值给对应的属性变量：@value(${配置文件中的key})

## Autowire
自动装配：Spring利用依赖注入，来对IOC容器中的各个组件赋值，默认赋null值

##  Qualifier
和Autowire注解一起使用，指定需要装配的组件的id，@Qualifier("bookDao")，指定装配容器中id为bookDao的类

## Primary
和Bean注解一起使用，如果Spring容器中有相同类型的Bean，可以指定被优先加载。

## Profile
根据环境动态的切换一些列组件的功能，指定组件在那些环境下才可以被注册到容器中，不指定，任何环境都会被注册到容器中。

注意：加了环境标识的bean，只有激活该环境，这个bean才可以被注册到Spring容器中，默认是default，@Profile("default")。

如何切换环境，来加载不同环境的bean？
- 使用命令行参数：-Dspring.profiles.active=test
- 使用代码的方式，设置需要激活的环境，详细了解可以百度，在Springboot中可以使用配置来指定加载不同的配置文件。

例如springboot中切换不同的配置文件

## AOP
AOP：在程序运行期间可以动态的将某段代码切入到指定位置来运行的机制，底层实现机制为，动态代理

方式：定义切面类，然后将切面类动态的切入到切入点进行代码填充，切入点为一个类中的方法（将切入类中的方法何时切入到切入点）。

注意：
- 定义好的切面类要注入到Spring容器中（和其他类型的类的注入没有什么区别）
- 为了让Spring容器知道那个类是切面类，要给切面类添加一个Aspect注解
- 还要给注入切面类的配置类添加一个EnableAspectJAutoProxy注解（开启基于注解的AOP模式 ）
- 我们测试AOP的功能的时候，不要自己创建类的实例，那样不是Spring容器帮我管理的，我们要从Spring容器中获取这个bean

# SpringBoot

## SpringBootApplication注解
等价于：SpringBootConfiguration + EnableAutoConfiguration + ComponentScan

## 自动配置：按需加载 + 条件装配
- 引入场景启动后就会帮我们配置默认的有关该场景的配置——具体的可以查看该场景的爸爸项目
- 各种配置都拥有默认的配置，默认的配置会绑定的一个类上（配置绑定），我们可以通过该类来实现对默认配置的修改。
- 引入什么场景，才会默认配置什么场景，该场景的自动配置才会生效（Springboot是按需配置的），我们才可以给该场景进行自定义配置。 

### 配置绑定
方式一：Component + ConfigurationProperties(prefix="配置文件中的前缀")
- Component：该类（bean类）是一个组件
- ConfigurationProperties：该类（bean类）和配置文件绑定，使用配置文件中的指定的值，作为该类的属性值。

方式二：EnableConfigurationProperties + ConfigurationProperties(prefix="配置文件中的前缀")
- EnableConfigurationProperties(User.class)：标注在配置类上，表示开启自动配置属性
- ConfigurationProperties：标注在bean类上，该类（bean类）和配置文件绑定

### 自动配置原理：EnableAutoConfiguration注解
- SpringBootConfiguration注解：等价于Configuration注解，标注这个类是一个配置类，区别在于该类是一个主配置类。
- ComponentScan注解：配置包扫描规则
- EnableAutoConfiguration注解：开启自动配置

EnableAutoConfiguration注解
- 等价于：AutoConfigurationPackage + Import({AutoConfigurationImportSelector.class})
- AutoConfigurationPackage注解：自动配置包，给容器中导入一个类 Registrar.class（注册类：通过该类批量给容器注册组件 ）
	- 将指定包下面的所有组件导入到容器中（main程序所在的包）
- Import({AutoConfigurationImportSelector.class})：导人一个类 AutoConfigurationImportSelector.class（自动配置导入选择器）

###  Registrar.class
static class Registrar implements ImportBeanDefinitionRegistrar, DeterminableImports {
        Registrar() {
        }
		// AnnotationMetadata注解元信息，表示该注解标注在那个类上
        public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
			// 将扫描到包的下面的所有组件导入到容器中（main程序所在的包）
            AutoConfigurationPackages.register(registry, (String[])(new AutoConfigurationPackages.PackageImports(metadata)).getPackageNames().toArray(new String[0]));
        }

        public Set<Object> determineImports(AnnotationMetadata metadata) {
            return Collections.singleton(new AutoConfigurationPackages.PackageImports(metadata));
        }
    }
### 按需加载：AutoConfigurationImportSelector.class	
// 获取自动配置的键值对
protected AutoConfigurationImportSelector.AutoConfigurationEntry getAutoConfigurationEntry(AnnotationMetadata annotationMetadata) {
	// 元数据是否为空
	if (!this.isEnabled(annotationMetadata)) {
		return EMPTY_ENTRY;
	} else {
		// 获取元数据
		AnnotationAttributes attributes = this.getAttributes(annotationMetadata);
		// 根据元数据获取候选的自动配置（所有自动配置都会被扫描到：包括第三方启动场景的自动配置）：默认扫描配置中所有：META-INFO/spring.factorys
		List<String> configurations = this.getCandidateConfigurations(annotationMetadata, attributes);
		// 移除重复的配置
		configurations = this.removeDuplicates(configurations);
		// 需要排除的配置
		Set<String> exclusions = this.getExclusions(annotationMetadata, attributes);
		// check
		this.checkExcludedClasses(configurations, exclusions);
		// 移除排除的配置
		configurations.removeAll(exclusions);
		configurations = this.getConfigurationClassFilter().filter(configurations);
		this.fireAutoConfigurationImportEvents(configurations, exclusions);
		// 将条件筛选后的自动配置返回
		return new AutoConfigurationImportSelector.AutoConfigurationEntry(configurations, exclusions);
	}
}

总结：
- 默认扫描依赖包中所有：META-INFO/spring.factorys文件，只要那个依赖中存在该文件，就会被扫描出来
- 此方法只扫描rEnableAutoConfiguration后缀的配置，比如：org.springframework.boot.autoconfigure.EnableAutoConfiguration

###  条件装配：ConditionalOnProperty
虽然一次加载了那么多的配置，并不是全部都生效，那些生效，那些不生效，还要看配置，如果我们没有配置，那么是不会生效的。

配置配上会出现这个两个注解：ConditionalOnProperty，ConditionalOnBean，ConditionalOnMissingBean

### 总结
- SpringBoot先加载所有的自动配置类  xxxxxAutoConfiguration。
- 每个自动配置类按照条件进行生效，默认都会绑定配置文件指定的值。xxxxProperties里面拿。xxxProperties和配置文件（application.properties）进行了绑定
- 生效的配置类就会给容器中装配很多组件
- 只要容器中有这些组件，相当于这些功能就有了
- 定制化配置
  - 方式一：用户直接自己@Bean替换底层的组件
  - 方式二：用户去看这个组件是获取的配置文件什么值就去修改。

xxxxxAutoConfiguration ---> 组件  ---> xxxxProperties里面拿值  ----> application.properties  

// 配置类
@EnableConfigurationProperties({DataSourceProperties.class})
// 配置类绑定配置文件中的属性：可以在配置类中查看到
@ConfigurationProperties(prefix = "spring.datasource")

# xxl-job使用