## mybatis的动态SQL
### 动态 SQL 之 if 标签
OGNL：全称是Object-Graph Navigation Language

用途：是一个用来获取和设置 java对象属性的表达式语言

- 属性名称：如对象user的属性username,可以使用user.username来获取.

- 方法调用：可以使用user.hashCode()返回当前对象的哈希码.

- 数组元素：对于userList数组对象,可以使用userList[0]来引用其中的某一个元素
```xml
<select id="findUserByCondition" resultMap="userMap" parameterType="user">
	select * from user where 1=1
	 /*两个判断条件*/
	<if test="username!=null">
		username=#{userName}
	</if>
	<if test="usersex!=null">
		and sex=#{userSex}
	</if>
</select>
```
```
注意： 
<if>标签的 test 属性中写的是对象的属性名，如果是包装类的对象要使用 OGNL 表达式的写法。

where 1=1的作用就是在SQL动态查询中，无论出现什么因素，都能保证有一条语句能正确执行（也就是防止where子句中内容为空时出现报错）
```
### 动态 SQL 之where标签
刚才使用了if标签进行数据的查询，如果不使用where 1=1，当出现所有设定的判断语句都不成立时，就会因为where子句为空而出错

我们可以把上面的查询语句进行改写，使用where标签，这样就不会出现上面的问题了，改写之后的映射配置如下：

```xml
 <!--根据条件查询-->
<select id="findUserByCondition" resultMap="userMap" parameterType="user">
    select * from user
    /*两个判断条件都放在where标签中*/
    <where>
        <if test="userName!=null">
            and username=#{userName}
        </if>
        <if test="userSex!=null">
            and sex=#{userSex}
        </if> 
    </where>
</select>
```
### 动态标签之foreach标签
传入多个 id 查询用户信息，进行范围查询时，就要将一个集合中的值，作为参数动态添加进来，比如一个List

对应的sql 语句可以是以下两种：
```sql
SELECT * FROM USERS WHERE (id =10 OR id =89 OR id=16);

SELECT * FROM USERS WHERE  id IN (10,89,16);
```

```xml
<!--根据QueryVo中提供的id集合，查询用户信息-->
<!--ids为包装类List类型，需要使用OGNL表达式，来获取-->
<select id="findUserByIds" parameterType="QueryVo" resultMap="userMap">
    select * from user
    <where>
        <if test="ids!=null and ids.size>0">
            <foreach collection="ids" open="id in (" close=")"  item="id" separator=",">
                #{id}
            </foreach>
        </if>
    </where>
</select>
```
- collection:代表要遍历的集合元素，注意编写时不要写#{}
- open:代表语句的开始部分
- close:代表结束部分
- item:代表要遍历集合中的元素，遍历出的值放在open参数和close参数的中间。
- sperator:代表分隔符

结果如下：
```sql
select * from user where id in (id1,id2,di3,,,,);
```
### 相同SQL代码的抽取
sql中可将重复的 sql 提取出来，使用时用 include 引用即可，最终达到 sql 重用的目的
- sql标签：抽取重复代码
- include标签：引入抽取的代码

```
<!--抽取重复Sql代码-->
<sql id="defaultSql">
    select * from user
</sql>
<!--根据QueryVo中提供的id集合，查询用户信息-->
<select id="findUserByIds" parameterType="QueryVo" resultMap="userMap">
    /*引入抽取的代码*/
    <include refid="defaultSql"></include>
    <where>
        <if test="ids!=null and ids.size>0">
            <foreach collection="ids" open="id in (" close=")"  item="id" separator=",">
                #{id}
            </foreach>
        </if>
    </where>
</select>
```