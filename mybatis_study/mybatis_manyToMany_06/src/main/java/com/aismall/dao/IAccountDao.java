package com.aismall.dao;

import com.aismall.domain.Account;
import com.aismall.domain.AccountUser;

import java.util.List;

public interface IAccountDao {
    /*查询所有账户*/
    List<Account> findAll();

    /*查询用户*/
    List<Account> findAccountUserAll();
    /*查询所有账户，同时还要获取当前账户所对应的用户信息（用户的名称和地址）*/
    List<AccountUser> findByAccount();
}
