package com.aismall.dao;

import com.aismall.domain.Role;

import java.util.List;

public interface IRoleDao {
    /*查询所有方法*/
    List<Role> findAll();
}
