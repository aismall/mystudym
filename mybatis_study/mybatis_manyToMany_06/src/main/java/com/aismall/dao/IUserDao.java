package com.aismall.dao;
import com.aismall.domain.User;

import java.util.List;

/**
 * 用户的持久层接口
 */
public interface IUserDao {
    /*查询所有用户*/
    List<User> findAll();

    /*一对多查询所有用户*/
    List<User> findUserAccountAll();

    /*查询所有*/
     List<User> findUserRoleAll();
}