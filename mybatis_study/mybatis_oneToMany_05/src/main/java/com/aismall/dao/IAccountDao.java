package com.aismall.dao;

import com.aismall.beans.Account;
import com.aismall.beans.AccountUser;

import java.util.List;

public interface IAccountDao {
    /*查询所有账户*/
    List<Account> findAccountAll();
    /*查询账户和用户信息：Account实体类中包含User*/
    List<Account> findAccountUserAll();
}
