package com.aismall.dao;

import com.aismall.beans.AccountUser;

import java.util.List;

public interface IAccountUserDao {
    /*
     * 查询所有账户，同时还要获取当前账户所对应的用户信息（用户的名称和地址）
     *  封账的实体类为：AccountUser
     * */
    List<AccountUser> findAccountUser();
}
