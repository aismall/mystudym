package com.aismall.dao;
import com.aismall.beans.User;

import java.util.List;

/**
 * 用户的持久层接口
 */
public interface IUserDao {
    /*查询所有用户*/
    List<User> findUserAll();

    /*一对多查询所有用户*/
    List<User> findUserAccountAll();
}