package com.aismall.beans;
/*
* 账户和用户是多对一的关系，但是一个账户只能对应一个用户（本质和一对一一样）：
*   AccountUser继承Account类，目的是为了封账Account类的信息（id，uid，money），
*   还可以封装User类中的一部信息（即本类中的属性）（属性：username，address），
*   这种方式不常用，但要知道，
*   一般都是分开写，一个账户类，一个用户类，然后做关联
* */
public class AccountUser extends Account{
    private String username;
    private String address;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return super.toString()+"      AccountUser{" +
                "username='" + username + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
