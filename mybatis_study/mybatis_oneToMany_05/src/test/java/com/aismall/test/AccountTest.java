package com.aismall.test;

import com.aismall.dao.IAccountDao;
import com.aismall.beans.Account;
import com.aismall.beans.AccountUser;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class AccountTest {
    private InputStream in;
    private SqlSession sqlSession;
    private IAccountDao accountDao;
    @Before//用于在测试方法执行之前执行
    public void init()throws Exception{
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.获取SqlSessionFactory
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        //3.获取SqlSession对象
        sqlSession = factory.openSession(true);
        //4.获取dao的代理对象
        accountDao = sqlSession.getMapper(IAccountDao.class);
    }

    @After//用于在测试方法执行之后执行
    public void destroy()throws Exception{
        //提交事务
        // sqlSession.commit();
        //6.释放资源
        sqlSession.close();
        in.close();
    }
    /*
    * 查询所有账户
    * */
    @Test
    public void testFindAccountAll(){
        List<Account> accounts=accountDao.findAccountAll();
        for(Account account:accounts){
            System.out.println(account);
        }
    }

    @Test
    public void testFindAccountUserAll(){
        List<Account> accounts=accountDao.findAccountUserAll();
        System.out.println("-----------------");
        for(Account account:accounts){
            System.out.println(account);
            System.out.println(account.getUser());
        }
    }
}