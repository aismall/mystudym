package com.aismall.test;

import com.aismall.beans.AccountUser;
import com.aismall.dao.IAccountDao;
import com.aismall.dao.IAccountUserDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class AccountUserTest {
    private InputStream in;
    private SqlSession sqlSession;
    private IAccountUserDao accountUserDao;
    @Before//用于在测试方法执行之前执行
    public void init()throws Exception{
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.获取SqlSessionFactory
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        //3.获取SqlSession对象
        sqlSession = factory.openSession(true);
        //4.获取dao的代理对象
        accountUserDao = sqlSession.getMapper(IAccountUserDao.class);
    }

    @After//用于在测试方法执行之后执行
    public void destroy()throws Exception{
        //提交事务
        // sqlSession.commit();
        //6.释放资源
        sqlSession.close();
        in.close();
    }

    /*
     * 查询所有账户，同时还要获取当前账户所对应的用户信息（用户的名称和地址）
     *  封账的实体类为：AccountUser
     * */
    @Test
    public void testFindByAccount(){
        List<AccountUser> accountUsers=accountUserDao.findAccountUser();
        for (AccountUser accountUser:accountUsers){
            System.out.println(accountUser);
        }
    }
}
