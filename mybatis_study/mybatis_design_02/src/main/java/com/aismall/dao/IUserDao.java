package com.aismall.dao;

import com.aismall.domain.User;

import java.util.List;

/*用户持久层接口*/
public interface IUserDao {
    /*定义一个查询操作*/
    List<User> findAll();//抽象方法
}
