package com.aismall.mybatis.SqlSession;

import com.aismall.mybatis.SqlSession.defaults.DefaultsSqlSessionFactory;
import com.aismall.mybatis.cfg.Configuration;
import com.aismall.mybatis.utils.XMLConfigBuilder;

import java.io.InputStream;
/*用于创建一个SqlSessionFactory对象*/
public class SqlSessionFactoryBuilder {
    /*根据参数的字节输入流来构建一个SqlSessionFactory工厂*/
    public SqlSessionFactory build(InputStream config) {
        Configuration cfg=XMLConfigBuilder.loadConfiguration(config);
        return  new DefaultsSqlSessionFactory(cfg);
    }
}
