package com.aismall.mybatis.SqlSession;

public interface SqlSessionFactory {
    SqlSession openSession();
}
