package com.aismall.mybatis.SqlSession;

import com.aismall.dao.IUserDao;

/*自定义mybatis中和数据库交互的核心类
* 他可以创建dao接口的代理对象*/
public interface SqlSession {
    /*根据参数创建一个代理对象*/
    //注意泛型
    <T> T getMapper(Class<T> daoInterfaceClass);

    /*释放资源*/
    void close();
}
