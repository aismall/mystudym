package com.aismall.mybatis.SqlSession.defaults;

import com.aismall.mybatis.SqlSession.SqlSession;
import com.aismall.mybatis.SqlSession.SqlSessionFactory;
import com.aismall.mybatis.cfg.Configuration;

/*用于创建一个SqlSession对象*/
public class DefaultsSqlSessionFactory implements SqlSessionFactory {
    private Configuration cfg;
    public DefaultsSqlSessionFactory(Configuration cfg) {
        this.cfg=cfg;
    }


    /*用于创建新的操作数据库的对象*/
    public SqlSession openSession() {
        return new DefaultSqlSession(cfg);
    }
}
