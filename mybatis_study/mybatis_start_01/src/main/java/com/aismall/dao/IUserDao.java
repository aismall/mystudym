package com.aismall.dao;

import com.aismall.beans.User;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*用户持久层接口*/
public interface IUserDao {
    //定义一个查询操作：不使用注解，使用映射配置文件（常用）
    List<User> findByUsername(String username);

    //使用注解：无需和映射配置文件绑定（不常用）
    @Select("select * from t_user")
    List<User> findAll();
}
