package com.aismall.test;
import com.aismall.dao.IUserDao;
import com.aismall.beans.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/*入门案例*/
public class MybatisTest {
    /*
     * 使用的是映射配置文件
     * */
    @Test
    public void findByUsernameTest() throws IOException {
        //1.读取配置文件
        InputStream io = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.创建SqlSessionFactory工厂，因为接口不能直接new，所以借助SqlSessionFactoryBuilder
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(io);
        //3.使用工厂生产SqlSession对象
        SqlSession session = factory.openSession();
        //4.使用SqlSession创建Dao接口的代理对象,因为此接口没有实现类，所以使用代理的方式对接口进行增强
        IUserDao userDao = session.getMapper(IUserDao.class);
        //5.使用代理对象执行方法
        List<User> users = userDao.findByUsername("aismall01");
        for (User user : users) {
            System.out.println(user);
        }
        //6.释放资源
        session.close();
        io.close();
    }

    /*
     *  使用的是注解
     * */
    @Test
    public void findAllTest() throws IOException {
        //1.读取配置文件
        InputStream io = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.创建SqlSessionFactory工厂，因为接口不能直接new，所以借助SqlSessionFactoryBuilder
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(io);
        //3.使用工厂生产SqlSession对象
        SqlSession session = factory.openSession();
        //4.使用SqlSession创建Dao接口的代理对象,因为此接口没有实现类，所以使用代理的方式对接口进行增强
        IUserDao userDao = session.getMapper(IUserDao.class);
        //5.使用代理对象执行方法
        List<User> users = userDao.findAll();
        for (User user : users) {
            System.out.println(user);
        }
        //6.释放资源
        session.close();
        io.close();
    }
}
