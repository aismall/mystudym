DROP DATABASE IF EXISTS `mybatis_study`;

/*创建mybatis_study数据库*/
CREATE DATABASE `mybatis_study`;

/*使用这个数据库 */
USE `mybatis_study`;

/*创建t_user表*/
DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(100) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL,
  `sex` varchar(30) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  CHARSET=utf8;

/*给t_user表填充数据*/
insert  into `t_user`(`username`,`birthday`,`sex`,`address`) values ('aismall01','2022-08-13','male','shanghai'),('aismall02','2022-08-13','male','shanghai'),('aismall03','2022-08-13','male','shanghai'),('aismall04','2022-08-13','male','shanghai'),('aismall05','2022-08-13','male','shanghai'),('aismall06','2022-08-13','male','shanghai'),('aismall07','2022-08-13','male','shanghai'),('aismall08','2022-08-13','male','shanghai');

/*创建t_account表*/
DROP TABLE IF EXISTS `t_account`;

CREATE TABLE `t_account` (
  `aid` int(11) NOT NULL  AUTO_INCREMENT COMMENT '账户id',
  `uid` int(11) default NULL COMMENT '用户编号',
  `money` double default NULL COMMENT '账户余额',
  PRIMARY KEY  (`aid`),
  KEY `uid_fk_1` (`uid`),
  CONSTRAINT `uid_fk_1` FOREIGN KEY (`uid`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
 
/*给t_account表填充数据*/
insert  into `t_account`(`uid`,`money`) values (1,1000),(2,1000),(1,2000);

/*创建角色表t_role*/
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `rid` int(11) NOT NULL  AUTO_INCREMENT COMMENT '编号',
  `role_name` varchar(200) default NULL COMMENT '角色名称',
  `role_desc` varchar(200) default NULL COMMENT '角色描述',
  PRIMARY KEY  (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `t_role`(`role_name`,`role_desc`) values ('院长','管理整个学院'),('总裁','管理整个公司'),('校长','管理整个学校');

/* 创建角色和用户的中间表*/
DROP TABLE IF EXISTS `t_user_role`;

CREATE TABLE `t_user_role` (
  `rid` int(11) NOT NULL COMMENT '角色编号',
  `uid` int(11) NOT NULL COMMENT '用户编号',
  PRIMARY KEY  (`uid`,`rid`),
  KEY `rid_fk_2` (`rid`),
  CONSTRAINT `rid_fk_2` FOREIGN KEY (`rid`) REFERENCES `t_role` (`rid`),
  CONSTRAINT `uid_fk_3` FOREIGN KEY (`uid`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `t_user_role`(`rid`,`uid`) values (1,1),(2,1),(1,2);
