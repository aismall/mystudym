package com.aismall.dao;

import com.aismall.domain.Account;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface IAccountDao {

    /**
     * 查询所有账户，采用延迟加载的方式查询账户的所属用户
     * @return
     */
    @Select("select * from account")
    @Results(id="accountMap",
            value= {
                    @Result(id=true,column="id",property="id"),
                    @Result(column="uid",property="uid"),
                    @Result(column="money",property="money"),
                    @Result(column="uid", property="user",
                            one=@One(select="com.aismall.dao.IUserDao.findById",
                                    fetchType=FetchType.LAZY
                            )
                    )
            })

    /*@One:相当于<association>的配置
select 属性：代表将要执行的 sql 语句
fetchType 属性：代表加载方式，一般如果要延迟加载都设置为 LAZY 的值*/
    List<Account> findAll();

    @Select("select * from account where uid = #{uid} ")
    List<Account> findByUid(Integer userId);
}
