package com.aismall.dao;

import com.aismall.domain.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface IUserDao {
 @Results(id="userMap",
         value= {
                 @Result(id=true,column="id",property="userId"),
                 @Result(column="username",property="userName"),
                 @Result(column="sex",property="userSex"),
                 @Result(column="address",property="userAddress"),
                 @Result(column="birthday",property="userBirthday"),
                 @Result(column="id",property="accounts",
                 many=@Many(
                         select="com.aismall.dao.IAccountDao.findByUid",
                         fetchType=FetchType.LAZY
                 )
         )
         })
/*@Many:相当于<collection>的配置
select 属性：代表将要执行的 sql 语句
fetchType 属性：代表加载方式，一般如果要延迟加载都设置为 LAZY 的值*/

 /*根据 id 查询一个用户*/
 @Select("select * from user where id = #{uid} ")
 User findById(Integer userId);

 @Select("select * from user")
 @ResultMap("userMap")
 List<User> findAll();
}

