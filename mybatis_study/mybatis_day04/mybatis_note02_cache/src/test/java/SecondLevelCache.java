import com.aismall.dao.IUserDao;
import com.aismall.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

public class SecondLevelCache {

    private InputStream in;
    private SqlSessionFactory factory;

    @Before//用 于在测试方法执行之前执行
    public void init()throws Exception{
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.获取SqlSessionFactory
        factory = new SqlSessionFactoryBuilder().build(in);
    }

    @After//用于在测试方法执行之后执行
    public void destroy()throws Exception{
        //提交事务
        // sqlSession.commit();
        in.close();
    }

    /*测试二级缓存*/
    @Test
    public void testSecondLevelCache1(){
        //3.获取SqlSession对象
        SqlSession sqlSession1=factory.openSession();
        //4.获取dao的代理对象
        IUserDao userDao1=sqlSession1.getMapper(IUserDao.class);

        User user1=userDao1.findUserById(41);
        System.out.println(user1);

        //5.释放资源
        sqlSession1.close();

        SqlSession sqlSession2=factory.openSession();
        IUserDao userDao2=sqlSession2.getMapper(IUserDao.class);
        User user2=userDao2.findUserById(41);

        System.out.println(userDao2);
        sqlSession2.close();


        System.out.println(user1==user2);
    }


}
