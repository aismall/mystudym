import com.aismall.dao.IUserDao;
import com.aismall.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

public class UserTest {
     private InputStream in;
    private SqlSession sqlSession;
    private IUserDao userDao;
    private  SqlSessionFactory factory;
    @Before//用 于在测试方法执行之前执行
    public void init()throws Exception{
        //1.读取配置文件，生成字节输入流
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.获取SqlSessionFactory
        factory = new SqlSessionFactoryBuilder().build(in);
        //3.获取SqlSession对象
        sqlSession = factory.openSession(true);
        //4.获取dao的代理对象
        userDao = sqlSession.getMapper(IUserDao.class);
    }

    @After//用于在测试方法执行之后执行
    public void destroy()throws Exception{
        //提交事务
        // sqlSession.commit();
        //6.释放资源
        sqlSession.close();
        in.close();
    }

   /*测试一级缓存：不关闭session情况下*/
    @Test
    public void testFirstLevelCache1(){
        User user1=userDao.findUserById(41);
        System.out.println(user1);

        User user2=userDao.findUserById(41);
        System.out.println(user2);

        //返回true就证明是同一个对象
        System.out.println(user1==user2);
    }

    /*测试一级缓存：关闭session情况下*/
    @Test
    public void testFirstLevelCache2(){
        User user1=userDao.findUserById(41);
        System.out.println(user1);

        /*
        //第一种方法
        //关闭session
        sqlSession.close();
        //重新获取session
        sqlSession= factory.openSession();
        userDao=sqlSession.getMapper(IUserDao.class);
        */
        //第二种方法
        sqlSession.clearCache();
        User user2=userDao.findUserById(41);
        System.out.println(user2);

        //返回false就证明是一级缓存被释放
        System.out.println(user1==user2);
    }

    /*测试缓存同步*/
    @Test
    public void testUpdateUser(){
        //1.根据id查询用户
        User user1=userDao.findUserById(41);
        System.out.println(user1);
        //2.更新用户信息
        user1.setUsername("updateUser");
        user1.setAddress("北京");
        //3.更新用户
        userDao.updateUser(user1);
        //4.再次查询
        User user2=userDao.findUserById(41);
        System.out.println(user2);

        //返回true就证明是同一个对象
        System.out.println(user1==user2);

        /*一级缓存是 SqlSession 范围的缓存，当调用 SqlSession 的修改，添加，删除，
        commit()， close()等，方法时，就会清空一级缓存。*/
    }
}
