package com.aismall.dao;

import com.aismall.domain.User;

import java.util.List;

public interface IUserDao {
    /*查询所有*/
    List<User> findAll();

    /*根据 id 查询*/
    User findUserById(Integer userId);

    /*更新用户*/
    void updateUser(User user);
}
