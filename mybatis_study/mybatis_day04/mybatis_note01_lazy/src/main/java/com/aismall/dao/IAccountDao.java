package com.aismall.dao;

import com.aismall.domain.Account;

import java.util.List;

public interface IAccountDao {
    /*查询所有账户*/
    List<Account> findAll();

    /*根据Id查询用户*/
    /*用户对应的账户可能为多个，一对多，使用集合接收，配置的时候使用collection*/
    List<Account> findAccountById(Integer accountId);
}
