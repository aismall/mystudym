package com.aismall.dao;

import com.aismall.domain.Account;
import com.aismall.domain.User;

import java.util.List;

public interface IUserDao {
    /*查询所有*/
    List<User> findAll();

    /*根据 id 查询*/
    /*账户只能对应一个用户，多对一，不需要集合接收，配置的时候使用association*/
    User findUserById(Integer userId);
}
