package com.aismall.test;
import com.aismall.dao.IUserdao;
import com.aismall.beans.QueryVo;
import com.aismall.beans.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/*编写测试类*/
public class mybatis_Test{
    //成员变量
    private  InputStream in;
    private SqlSession sqlSession;
    private IUserdao userdao;
    @Before//用于在测试方法执行之前操作
    /*初始化方法*/
    public void init() throws IOException {
        //1.读取配置文件，生成字节输入流
        //静态方法可以使用类名直接调用
        in= Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.获取sqlSessionFactoryBuilder对象
        SqlSessionFactoryBuilder builder=new SqlSessionFactoryBuilder();
        //通过构建者创建工厂
        SqlSessionFactory factory=builder.build(in);
        //3.通过SqlSessionFactory工厂生成sqlSession对象
        sqlSession=factory.openSession();
        //4.获取dao的代理对象
        userdao=sqlSession.getMapper(IUserdao.class);
    }

    @After//用于在测试方法执行之后操作
    /*释放资源方法*/
    public void destory() throws IOException {
        //事务提交
        sqlSession.commit();
        sqlSession.close();
        in.close();
    }

    @Test
    /*
    * 查询所有用户
    * */
    public void testFindAll() {
        List<User> users=userdao.findAll();
        for (User user:users) {
            System.out.println(user);
        }
    }

    @Test
    /*
    * 插入用户
    * */
    public void testInsertUser() {
        User user=new User();
        user.setUsername("AISMALL");
        user.setAddress("beijing");
        user.setSex("female");
        user.setBirthday(new Date());
        userdao.insertUser(user);
        System.out.println("插入数据结束");

    }
    /*
    * 根据用户名查询用户
    * */
    @Test
    public void testFindByUsername(){
        List<User> users = userdao.findByUsername("AISMALL");
        for (User user:users) {
            System.out.println(user);
        }
    }

    @Test
    /*
    * 更新用户信息
    * */
    public void testUpdateUser(){
        User user=new User();
        user.setUsername("AISMALL");
        user.setAddress("shanghai");
        user.setSex("female");
        user.setBirthday(new Date());
        userdao.updateUser(user);
        System.out.println("更新数据结束");
    }
    /*
    * 删除用户
    * */
    @Test
    public void testDeleteUser(){
        userdao.deleteUser("AISMALL");
        System.out.println("删除数据结束");
    }

    @Test
    /*根据id查询用户*/
    public void testFindById(){
        User user=userdao.findById(43);
        System.out.println(user);
    }

    /*根据用户名进行模糊查询*/
    @Test
    public void testFindByName(){
        //因为进行模糊查询，包含AISMALL的都被查询出来，所以下面要加上% %，SQL语法内容
        List<User> users=userdao.findByName("%AISMALL%");
        for (User user:users) {
            System.out.println(user);
        }
    }

    /*根据queryVo中的 username 条键查询用户*/
    @Test
    public void testFindByVo(){
        QueryVo vo=new QueryVo();
        User user=new User();
        user.setUsername("%AISMALL%");
        //让两者建立关系
        vo.setUser(user);
        //使用代理类来调用方法
        List<User> users=userdao.findUserByVo(vo);
        for(User u:users){
            System.out.println(u);
        }
    }

    /*测试查询总记录条数方法*/
    @Test
    public void testFindTotal(){
        int count=userdao.findTotal();
        System.out.println(count);
    }
}

