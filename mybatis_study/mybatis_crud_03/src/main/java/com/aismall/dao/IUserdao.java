package com.aismall.dao;

import com.aismall.beans.QueryVo;
import com.aismall.beans.User;

import java.util.List;
/*用户持久层接口*/
public interface IUserdao {
    /*查询所有用户*/
    List<User> findAll();

    /*插入用户*/
    void insertUser(User user);

    /*根据用户名查询用户*/

    List<User> findByUsername(String username);

    /*更新用户信息*/
    void updateUser(User user);

    /*根据 username 删除操作*/
    void deleteUser(String username);

    /*根据ID查询操作*/
    User findById(Integer id);

    /*根据用户名模糊查询*/
    List<User> findByName(String username);

    /*根据queryVo中的 username 条键查询用户*/
    List<User>  findUserByVo(QueryVo vo);

    /*统计数据库中的数据条数*/
    int findTotal();
}
