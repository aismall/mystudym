package com.aismall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.aismall.news")
public class SpringbootGrpcClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootGrpcClientApplication.class, args);
    }
}
