package com.aismall.news.controller;

import com.aismall.news.proto.NewsProto;
import com.aismall.news.proto.NewsServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//restful风格的控制器
@RestController
public class NewsController {
    //指向配置文件的 grpc-server 配置
    @GrpcClient("grpc-server")
    private NewsServiceGrpc.NewsServiceBlockingStub newsServiceBlockingStub;

    //访问链接地址：localhost:8726/news?date=20220725
    @GetMapping("news")
    public String news(String date) {
        System.out.println("==================");
        NewsProto.NewsResponse newsResponse = newsServiceBlockingStub.list(NewsProto.NewsRequest.newBuilder().setDate(date).build());
        return newsResponse.getNewsList().toString();

    }
}
