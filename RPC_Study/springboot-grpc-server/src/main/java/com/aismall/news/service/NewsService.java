package com.aismall.news.service;

import com.aismall.news.proto.NewsProto;
import com.aismall.news.proto.NewsServiceGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.Date;

// 使用注解标注这是一个Grpc的服务:标识接口
//springboot扫描的时候，会认为这个类是grpc的实现类
@GrpcService
public class NewsService extends NewsServiceGrpc.NewsServiceImplBase {
    //注意：入参为方法里面第一个参数，出参为方法里面的第二个参数
    @Override
    public void list(NewsProto.NewsRequest request, StreamObserver<NewsProto.NewsResponse> responseObserver) {
        // 获取入参的data属性
        String date = request.getDate();
        // 反参对象
        NewsProto.NewsResponse newsList = null;
        try {
            // 反参的构建器
            NewsProto.NewsResponse.Builder newsListBuilder = NewsProto.NewsResponse.newBuilder();
            for ( int i = 1;i<=25;i++ ){
                // 构建反参中的存储的实体类
                NewsProto.News news = NewsProto.News.newBuilder().setId(i)
                        .setContent(date + "新闻内容" + i)
                        .setTitle("新闻标题" + i)
                        .setCreateTime(new Date().getTime())
                        .build();
                // 将构建好的实体类存入反参的构建器中
                newsListBuilder.addNews(news);
            }
            // 调用build()方法完成反参的构建
            newsList = newsListBuilder.build();
        } catch (Exception e) {
            responseObserver.onError(e);
        }finally {
            // 实用观察者模式，将参数返回
            responseObserver.onNext(newsList);
        }
        // 关闭
        responseObserver.onCompleted();
    }
}
