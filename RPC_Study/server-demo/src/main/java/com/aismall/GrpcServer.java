package com.aismall;

import com.aismall.news.service.NewsService;
import io.grpc.ServerBuilder;

import java.io.IOException;
/*所有服务一起启动*/
public class GrpcServer {
    public static final int port = 7520;
    public static void main(String[] args) throws InterruptedException, IOException {
        io.grpc.Server server = ServerBuilder.forPort(port).addService(new NewsService())
                .build().start();
        System.out.println(String.format("GRPC 服务启动，端口号：%d", port));
        server.awaitTermination();
    }
}
