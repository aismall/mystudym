package com.aismall;


import com.aismall.news.proto.NewsProto;
import com.aismall.news.proto.NewsServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.List;
/*
* 测试 NewsService接口中的list服务
* */
public class GrpcNewsClient {
    // 访问server的url和端口号
    private static final String host="localhost";
    private static final int serverPort=7520;
    public static void main(String[] args) {
        //建立一个传输文本的通道
        ManagedChannel channel= ManagedChannelBuilder.forAddress(host,serverPort).usePlaintext()
                .build();
        // 创建一个传输通道，使用阻塞模式
        NewsServiceGrpc.NewsServiceBlockingStub blockingStub=NewsServiceGrpc.newBlockingStub(channel);
        //创建一个入参类
        NewsProto.NewsRequest request= NewsProto.NewsRequest.newBuilder().setData("20220709").build();
        // 调用NewService接口中的list服务：调用方式类似于调用方法一样。
        // 调用之后，会给我们返回一个出参类型的值：NewsResponse实例，该实例在服务端创建，通过上面定义的通信方式传输过来
        NewsProto.NewsResponse response=blockingStub.list(request);
        //获取出参实例中的参数
        List<NewsProto.News> newsList=response.getNewsList();
        for (NewsProto.News news:newsList) {
            System.out.println(news);
        }
        //关闭传输通道
        channel.shutdown();
    }
}
