DROP DATABASE IF EXISTS `ssm_study`;

/*创建mybatis_study数据库*/
CREATE DATABASE `ssm_study`;

/*使用这个数据库 */
USE `spring_study`;

/*创建t_user表*/
DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL  AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(100) DEFAULT NULL COMMENT '用户名称',
  `sex` varchar(100) DEFAULT NULL COMMENT '用户性别',
  `age` int(20) default NULL COMMENT '用户年龄',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
 
/*给t_user表填充数据*/
insert  into `t_user` (`username`,`sex`, `age`) values ('测试用户01', '男', 18),('测试用户02', '女', 19);
