package com.example.ssmstudy.service;

import com.example.ssmstudy.beans.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户业务层的接口
 */
@Service
public interface IUserService {
    /**
     * 查询所有
     */
    List<User> findAll();
    /**
     * 保存
     */
    void saveUser(User user);
    /**
     * 更新
     */
    void updateUser(User user);
    /**
     * 删除
     */
    void deleteUserByUserName(String username);
    /**
     * 根据 id 查询
     * @param userId
     */
    User findUserById(Integer userId);

    /**
     * 根据 用户名 查询
     * @param username
     */
    User findUserByUsername(String username);
}
