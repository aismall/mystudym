package com.example.ssmstudy.mapper;


import com.example.ssmstudy.beans.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface IUserMapper {
    /**
     * 查询所有
     */
    List<User> findAll();
    /**
     * 保存
     */
    void saveUser(User user);
    /**
     * 更新
     */
    void updateUser(User user);
    /**
     * 删除
     */
    void deleteUserByUserName(String username);
    /**
     * 根据 id 查询
     * @param userId
     */
    User findUserById(Integer userId);

    /**
     * 根据 用户名 查询
     * @param username
     */
    User findUserByUsername(String username);
}

