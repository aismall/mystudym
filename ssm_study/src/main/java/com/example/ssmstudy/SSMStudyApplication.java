package com.example.ssmstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*
* 启动之后访问链接：
*   注册用户：http://localhost:8080/
*   其他：http://localhost:8080/query
*
* */
@SpringBootApplication
public class SSMStudyApplication {
    public static void main(String[] args) {
        SpringApplication.run(SSMStudyApplication.class, args);
    }

}
