package com.example.ssmstudy.controller;

import com.example.ssmstudy.beans.User;
import com.example.ssmstudy.service.IUserService;
import com.example.ssmstudy.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @ClassName helloworld
 * @Description TODO
 * @Author @AISMALL
 * @Date 2022/8/29 22:55
 */
@Controller
public class HelloWorldController {
    @Autowired
    private IUserService userServiceImpl;
    @GetMapping("/")
    public String index(){
        // 返回index.html
        return "index";
    }

    /*
    * 插入一个用户到数据库
    * */
    @PostMapping("/saveUser")
    public String saveUser(String username, String sex, Integer age, Model model){
        User user = new User();
        user.setUsername(username);
        user.setSex(sex);
        user.setAge(age);
        try {
            userServiceImpl.saveUser(user);
        } catch (Exception e) {
            System.out.println(e);
            return "error";
        }
        model.addAttribute("form", user);
        // 返回pay.html
        return "success";
    }

    @GetMapping("/query")
    public String query(){
        return "query";
    }
    @GetMapping("/findAll")
    public String findAll(Model model){
        List<User> userList = null;
        try {
            userList = userServiceImpl.findAll();
        } catch (Exception e) {
            System.out.println(e + "查询全部用户");
            return "error";
        }
        model.addAttribute("form", userList);
        return "success";
    }
    @PostMapping("/findUserById")
    public String findUserById(Integer id, Model model){
        User user = null;
        try {
            user = userServiceImpl.findUserById(id);
        } catch (Exception e) {
            System.out.println(e + "根据ID查询用户");
            return "error";
        }
        if (user == null){
            String retMsg = "用户不存在";
            model.addAttribute("form", retMsg);
            return "success";
        }
        model.addAttribute("form", user);
        return "success";
    }

    @PostMapping("/deleteUserByName")
    public String deleteUserByName(String username, Model model){
        String res = "删除用户成功";
        if (userServiceImpl.findUserByUsername(username)==null){
            res = "该用户不存在";
            model.addAttribute("form", res);
            return "success";
        }
        try {
            userServiceImpl.deleteUserByUserName(username);
        } catch (Exception e) {
            System.out.println(e + "根据用户名字删除用户");
            return "error";
        }
        model.addAttribute("form", res);
        return "success";
    }

    @PostMapping("/updateUser")
    public String updateUser(String username, String sex, Integer age, Model model){
        String res = "更新用户成功";
        if (userServiceImpl.findUserByUsername(username)==null){
            res = "该用户不存在";
            model.addAttribute("form", res);
            return "success";
        }
        User user = new User();
        user.setUsername(username);
        user.setSex(sex);
        user.setAge(age);
        try {
            userServiceImpl.updateUser(user);
        } catch (Exception e) {
            return "error";
        }
        model.addAttribute("form", res);
        return "success";
    }
}
