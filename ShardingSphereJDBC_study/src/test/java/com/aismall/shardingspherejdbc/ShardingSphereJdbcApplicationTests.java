package com.aismall.shardingspherejdbc;

import com.aismall.shardingspherejdbc.entity.Common;
import com.aismall.shardingspherejdbc.entity.Course;
import com.aismall.shardingspherejdbc.entity.Demo;
import com.aismall.shardingspherejdbc.entity.User;
import com.aismall.shardingspherejdbc.mapper.CommonMapper;
import com.aismall.shardingspherejdbc.mapper.CourseMapper;
import com.aismall.shardingspherejdbc.mapper.DemoMapper;
import com.aismall.shardingspherejdbc.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardingSphereJdbcApplicationTests {
    @Autowired
    private DemoMapper testMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private CommonMapper CommonMapper;

    @Test
    public void addTestTable() {
        Demo demo = new Demo();
        demo.setTname("aismall");
        demo.setTage(18);
        testMapper.insert(demo);
    }

    @Test
    public void addTestDB() {
        Demo demo = new Demo();
        //demo.setTid(10L);
        demo.setTname("aismall");
        demo.setTage(10);
        testMapper.insert(demo);
    }

    @Test
    public void addUser() {
        User user = new User();
        user.setName("001");
        user.setSex("male");
        userMapper.insert(user);
    }

    @Test
    public void addCourse() {
        Course course = new Course();
        course.setCname("English");
        course.setCinfo("The First Book");
        courseMapper.insert(course);
    }


    @Test
    public void testAddCommon() {
        Common common = new Common();
        common.setCoinfo("common info");
        CommonMapper.insert(common);
    }

    @Test
    public void testCommonById() {
        QueryWrapper<Common> commonQueryWrapper = new QueryWrapper<>();
        // coid的值根据你自己的表来确定
        commonQueryWrapper.eq("coid", 760527149667975169L);
        Common data = CommonMapper.selectOne(commonQueryWrapper);
        System.out.println(data);
    }

    @Test
    public void tesCommonDel() {
        QueryWrapper<Common> commonQueryWrapper = new QueryWrapper<>();
        commonQueryWrapper.eq("coid", 760527149667975169L);
        CommonMapper.delete(commonQueryWrapper);
    }
}
