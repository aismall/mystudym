package com.aismall.shardingspherejdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value="t_course")
public class Course {
    @TableId(type = IdType.AUTO)
    private Long cid;
    private String cname;
    private String cinfo;
}
