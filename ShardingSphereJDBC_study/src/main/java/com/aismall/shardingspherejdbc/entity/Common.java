package com.aismall.shardingspherejdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value="t_common")
public class Common {
    @TableId(type = IdType.AUTO)
    private Long coid;
    private String coinfo;
}
