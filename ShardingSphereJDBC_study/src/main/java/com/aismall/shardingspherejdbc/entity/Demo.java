package com.aismall.shardingspherejdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Demo {
    @TableId(type = IdType.AUTO)
    private  Long tid ;
    private String tname ;
    private  int tage ;
}
