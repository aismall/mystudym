package com.aismall.shardingspherejdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value="t_user")
public class User {
    @TableId(type = IdType.AUTO)
    private Long uid;
    private String name;
    private String sex;
}
