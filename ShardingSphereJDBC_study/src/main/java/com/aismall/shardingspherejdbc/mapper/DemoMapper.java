package com.aismall.shardingspherejdbc.mapper;

import com.aismall.shardingspherejdbc.entity.Demo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/*
* 使用 mybatis-plus
* */
@Repository
public interface DemoMapper extends BaseMapper<Demo> {

}
