package com.aismall.shardingspherejdbc.mapper;

import com.aismall.shardingspherejdbc.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseMapper extends BaseMapper<Course> {
}
