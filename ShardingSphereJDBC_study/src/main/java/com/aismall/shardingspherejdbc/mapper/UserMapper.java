package com.aismall.shardingspherejdbc.mapper;

import com.aismall.shardingspherejdbc.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
}
