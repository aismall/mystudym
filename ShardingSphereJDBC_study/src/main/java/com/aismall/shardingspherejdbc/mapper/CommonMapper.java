package com.aismall.shardingspherejdbc.mapper;

import com.aismall.shardingspherejdbc.entity.Common;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonMapper extends BaseMapper<Common> {
}
