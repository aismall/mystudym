# web-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# bug解决
## 关掉ESlint语法检查
 vue.config.js 里面有配置如下:lintOnSave: false //关闭语法检查