// 导入全局引入都在main.js中导入
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios' //全局导入，跨域操作
import Element, { Message } from 'element-ui' //全局引入elementUI框架，通过我们编写的element.js文件引入
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(Element)
Vue.prototype.$message = Message




Vue.config.productionTip = false
Vue.prototype.$http=axios   // 挂载axios到Vue的原型prototype的$http
axios.defaults.baseURL="http://localhost:8725"  // 设置请求的根路径  

//调用链路：main.js——》App.vue——》路由占位符——》router.js——》对应单文件组件
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
