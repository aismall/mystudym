package com.aismall.dao;

import com.aismall.domain.Account;

import java.util.List;

public interface IAccountDao {
    /**
     * 查询所有
     */
    List<Account> findAll();
    /**
     * 保存
     */
    void saveAccount(Account account);
    /**
     * 更新
     */
    void updateAccount(Account account);
    /**
     * 删除
     */
    void deleteAccount(Integer accountId);
    /**
     * 根据 id 查询
     * @param accountId
     */
    Account findAccountById(Integer accountId);
}

