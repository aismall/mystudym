package com.aismall.dao.impl;

import com.aismall.dao.IAccountDao;
import com.aismall.domain.Account;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class AccountDaoImpl implements IAccountDao {
    private QueryRunner queryRunner;

    public void setQueryRunner(QueryRunner queryRunner) {
        this.queryRunner = queryRunner;
    }

    @Override
    public List<Account> findAll() {
        try {
            return queryRunner.query("select * from t_account",new BeanListHandler<Account>(Account.class));
        } catch (SQLException e) {
            throw  new RuntimeException(e);
        }
    }

    @Override
    public void saveAccount(Account account) {

        try {
            queryRunner.update("insert into t_account(name,money) values(?,?)",account.getName(),account.getMoney());
        } catch (SQLException e) {
            throw  new RuntimeException(e);
        }
    }

    @Override
    public void updateAccount(Account account) {
        try {
            queryRunner.update("update t_account set name=?.money=? where id=?",account.getName(),account.getMoney(),account.getId());
        } catch (SQLException e) {
            throw  new RuntimeException(e);
        }
    }

    @Override
    public void deleteAccount(Integer accountId) {
        try {
            queryRunner.update("delete from t_account where id=?",accountId);
        } catch (SQLException e) {
            throw  new RuntimeException(e);
        }
    }

    @Override
    public Account findAccountById(Integer accountId) {
        try {
            return queryRunner.query("select * from t_account where id=?",new BeanHandler<Account>(Account.class),accountId);
        } catch (SQLException e) {
            throw  new RuntimeException(e);
        }
    }
}