package com.aismall.service;

import com.aismall.dao.IAccountDao;
import com.aismall.domain.Account;

import java.util.List;

/**
 * 账户业务层的接口
 */
public interface IAccountService {
    /**
     * 查询所有
     */
    List<Account> findAll();

    /**
     * 查询一个
     */
    Account findAccountById(Integer accountId);

    /**
     * 保存
     */
    void saveAccount(Account account);

    /**
     * 更新
     */
    void updateAccount(Account account);

    /**
     * 删除
     */
    void deleteAccount(Integer acccountId);
}
