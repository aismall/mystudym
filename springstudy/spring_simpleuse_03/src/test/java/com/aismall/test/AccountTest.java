package com.aismall.test;

import com.aismall.domain.Account;
import com.aismall.service.IAccountService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
public class AccountTest {
    @Test
    public void testFindAll(){
        //1.获取核心容器对象
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean.xml");
        IAccountService as=ac.getBean("accountServiceImpl",IAccountService.class);
        List<Account> accounts= as.findAll();
        for (Account account:accounts) {
            System.out.println(account);
        }
    }
}


