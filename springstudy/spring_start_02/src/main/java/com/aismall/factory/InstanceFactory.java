package com.aismall.factory;

import com.aismall.service.IAccountService;
import com.aismall.service.impl.AccountServiceImpl;

/**
 * 模拟一个工厂类（该类可能是存在于jar包中的，我们无法通过修改源码的方式来提供默认构造函数）
 */
public class InstanceFactory {
    //无空参构造函数（默认构造）
    //有参构造函数
    //第二种：普通方法
    public IAccountService getAccountServiceImpl() {
        System.out.println("InstanceFactory.getAccountServiceImpl========");
        return new AccountServiceImpl();
    }
}
