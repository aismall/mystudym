package com.aismall.factory;

import com.aismall.service.IAccountService;
import com.aismall.service.impl.AccountServiceImpl;

/**
 * 模拟一个工厂类（该类可能是存在于jar包中的，我们无法通过修改源码的方式来提供默认构造函数）
 */
public class StaticFactory {
    //无空参构造函数（默认构造）
    //有参构造函数
    //第三种：静态方法
    public static IAccountService getAccountService(){
        System.out.println("StaticFactory.getAccountService==============");
        return new AccountServiceImpl();
    }
}
