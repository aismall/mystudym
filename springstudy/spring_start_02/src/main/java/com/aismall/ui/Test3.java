package com.aismall.ui;

import com.aismall.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 模拟一个表现层，用于调用业务层
 */

/**
 * AccountServiceImpl2.3.4为了演示依赖注入
 */
public class Test3 {
    public static void main(String[] args) {
        //1.获取核心容器对象
        ApplicationContext ac = new ClassPathXmlApplicationContext("beans3.xml");
        //2.根据id获取Bean对象
        IAccountService as2  = (IAccountService)ac.getBean("accountServiceImpl2");
        as2.saveAccount();
        IAccountService as3  = (IAccountService)ac.getBean("accountServiceImpl3");
        as3.saveAccount();
        IAccountService as4 = (IAccountService) ac.getBean("accountServiceImpl4");
        as4.saveAccount();
    }
}
