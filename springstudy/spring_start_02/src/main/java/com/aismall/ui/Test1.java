package com.aismall.ui;

import com.aismall.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 模拟一个表现层，用于调用业务层：测试Spring帮我们管理Bean
 */
public class Test1 {

    /*
    * 获取Spring的ioc核心容器，并根据id获取对象
    * ApplicationContext的三个常用实现类
    * 在ApplicationContext上鼠标右键 Diagrams—>ShowDiagrams，然后
    * 在ApplicationContext上鼠标右键 Show Implementations，就可以看到所有的实现类
    * 我们在这里介绍三个：
    *   ClassPathXmlApplicationContext：加载类路径下的配置文件
    *   FileSystemXmlApplicationContext：加载任意路径下的配置文件
    *   AnnotationConfigApplicationContext：读取注解创建容器
    *
    *
    *   核心容器两个接口引发的问题：
    *   ApplicationContext：（单例模式比较好）
    *       他在构建容器时，创建对象采取的策略是采用立即加载的方式，也就是说，只要一读取完配置文件，
    *       马上就创建配置文件中配置的对象（可以在无参构造函数中输出语句来查看对象创建与否）
    *   BeanFactory：（多例对象比较好）
    *       他在构建容器时，创建对象的策略是采用延时加载的方式，也就是说，什么时候根据id获取对象，
    *       什么时候真正创建对象
    *
    * */
    public static void main(String[] args) {
        //1.获取spring核心容器中的对象
        ApplicationContext ac=new ClassPathXmlApplicationContext("beans1.xml");
        //2.根据id获取bean对象
        IAccountService as=(IAccountService)ac.getBean("accountServiceImpl");
        as.saveAccount();
    }
}
