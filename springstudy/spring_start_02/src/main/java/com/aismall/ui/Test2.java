package com.aismall.ui;

import com.aismall.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * 模拟一个表现层，用于调用业务层：测试Spring管理Bean的特性
 */
public class Test2 {
    public static void main(String[] args) {
        //1.获取核心容器对象
        // ApplicationContext ac = new ClassPathXmlApplicationContext("beans3.xml");
        // ApplicationContext的子类： ClassPathXmlApplicationContext
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("beans2.xml");
        //2.根据id获取Bean对象
        IAccountService as1  = (IAccountService)ac.getBean("accountServiceImpl");
        //3.在演示多例模式的时候打开，如果返回false则为多例模式
        IAccountService as2  = (IAccountService)ac.getBean("accountServiceImpl");
        System.out.println(as1==as2);
        as1.saveAccount();
        //手动关闭容器：ClassPathXmlApplicationContext中的方法，父类无法调用
        ac.close();
    }
}
