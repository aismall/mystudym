package com.aismall.service.impl;

import com.aismall.dao.IAccountDao;
import com.aismall.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/*
* init方法和destroy方法是为了演示SpringBean的生命周期，配置在beans2.xml中
* */

public class AccountServiceImpl implements IAccountService {
    /*默认构造函数*/
    public AccountServiceImpl(){
        System.out.println("accountServiceImpl无参构造方法执行了，对象创建了");
    }
    /*保存操作方法*/
    public void saveAccount() {
        System.out.println("AccountServiceImpl.saveAccount===============");
        //1.获取spring核心容器中的对象
        ApplicationContext ac=new ClassPathXmlApplicationContext("beans1.xml");
        //2.根据id获取bean对象
        IAccountDao ao=(IAccountDao)ac.getBean("accountDaoImpl");
        ao.saveAccount();
    }
    /*初始化操作方法*/
    public void  init(){
        System.out.println("AccountServiceImpl.init 对象初始化了======");
    }
    /*销毁操作方法*/
    public void  destroy(){
        System.out.println("AccountServiceImpl.destroy 对象销毁了=====");
    }
}
