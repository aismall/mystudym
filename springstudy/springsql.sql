DROP DATABASE IF EXISTS `spring_study`;

/*创建mybatis_study数据库*/
CREATE DATABASE `spring_study`;

/*使用这个数据库 */
USE `spring_study`;

/*创建t_account表*/
DROP TABLE IF EXISTS `t_account`;

CREATE TABLE `t_account` (
  `id` int(11) NOT NULL  AUTO_INCREMENT COMMENT '账户id',
  `name` varchar(100) DEFAULT NULL COMMENT '账户名称',
  `money` int(20) default NULL COMMENT '账户余额',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
 
/*给t_account表填充数据*/
insert  into `t_account` (`name`,`money`) values ('支付宝',1000),('微信',1000),('银行卡',2000);
