package com.aismall.test;

import com.aismall.dao.IAccountDao;
import com.aismall.domain.Account;
import com.aismall.service.IAccountService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * 使用Junit单元测试：测试我们的配置
 */

public class AccountServiceTest {

    @Test
    public void testSaveAccountTest(){
        //1.获取核心容器对象
        //ApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
        //2.根据id获取Bean对象
        IAccountService as  = (IAccountService)ac.getBean("accountService");
        //IAccountService as2  = (IAccountService)ac.getBean("accountServiceImpl");
        //System.out.println(as);
        IAccountDao adao = ac.getBean("accountDao",IAccountDao.class);
        //System.out.println(adao);
        //System.out.println(as == as2);
        as.saveAccountTest();
        ac.close();
    }

    @Test
    public void testFindAll() {
        //1.创建容器
        ApplicationContext ac=new ClassPathXmlApplicationContext("beans.xml");
        //2.获取对象
        IAccountService as=ac.getBean("accountService",IAccountService.class);
        //3.执行方法
        List<Account> accounts = as.findAllAccount();
        for(Account account : accounts){
            System.out.println(account);
        }
    }

    @Test
    public void testFindOne() {
        //1.创建容器
        ApplicationContext ac=new ClassPathXmlApplicationContext("beans.xml");
        //2.获取对象
        IAccountService as=ac.getBean("accountService",IAccountService.class);
        //3.执行方法
        Account account = as.findAccountById(1);
        System.out.println(account);
    }

    @Test
    public void testSave() {
        //1.创建容器
        ApplicationContext ac=new ClassPathXmlApplicationContext("beans.xml");
        //2.获取对象
        IAccountService as=ac.getBean("accountService",IAccountService.class);
        Account account = new Account();
        account.setName("test");
        account.setMoney(12345f);
        //3.执行方法
        as.saveAccount(account);
        System.out.println(ac);
    }

    @Test
    public void testUpdate() {
        //1.创建容器
        ApplicationContext ac=new ClassPathXmlApplicationContext("beans.xml");
        //2.获取对象
        IAccountService as=ac.getBean("accountService",IAccountService.class);
        //3.执行方法
        Account account = as.findAccountById(4);
        account.setMoney(23456f);
        as.updateAccount(account);
    }

    @Test
    public void testDelete() {
        //1.创建容器
        ApplicationContext ac=new ClassPathXmlApplicationContext("beans.xml");
        //2.获取对象
        IAccountService as=ac.getBean("accountService",IAccountService.class);
        //3.执行方法
        as.deleteAccount(4);
    }
}
