AccountTest测试的是，使用纯注解的方式来演示操作数据库
    使用config包下面的两个类+jdbc.properties来替代beans.xml文件

AccountServiceTest使用半注解的方式演示Spring的IOC，因为数据源是在beans.xml文件中配置的

注意测试的时候：
    执行AccountTest测试类的时候需要把beans.xml文件中的内容注释掉，否则会报错。
    执行AccountServiceTest需要把beans.xml文件中的配置打开。