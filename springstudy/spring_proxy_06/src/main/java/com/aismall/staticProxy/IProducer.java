package com.aismall.staticProxy;

/*模拟生产厂家的接口*/
public interface IProducer {
    /*销售*/
    public void saleProduct(float money);
}
