package com.aismall.staticProxy;

/*模拟代理类*/
public class ProxyShop implements IProducer {
    private IProducer producer;
    public ProxyShop(){
        super();
    }

    public  ProxyShop(IProducer producer) {
        super();
        this.producer=producer;
    }

    /*销售*/
    public void saleProduct(float money){
        enhanceMethod();
        producer.saleProduct(1000*0.8f);
    }

    public void enhanceMethod(){
        System.out.println("产品被代理商售出，代理商扣除了20%的费用");
    }
}
