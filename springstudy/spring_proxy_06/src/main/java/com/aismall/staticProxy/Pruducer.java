package com.aismall.staticProxy;

/*模拟生产厂家类*/
public class Pruducer implements IProducer {
    /*销售*/
    public void saleProduct(float money){
        System.out.println("销售产品，并拿到钱"+money);
    }
}
