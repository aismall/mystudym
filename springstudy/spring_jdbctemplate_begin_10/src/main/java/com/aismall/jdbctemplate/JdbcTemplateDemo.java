package com.aismall.jdbctemplate;

import com.aismall.dao.IAccountDao;
import com.aismall.domain.Account;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * JdbcTemplate的最基本用法
 */
public class JdbcTemplateDemo {

    public static void main(String[] args) {
        //1.获取容器
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean.xml");
        //2.获取对象
        IAccountDao accountDao = ac.getBean("accountDao",IAccountDao.class);
         List<Account> accounts=accountDao.findAll();
        for (Account account:accounts) {
            System.out.println(account);
        }
    }
}
