package com.aismall.jdbctemplate;

import com.aismall.domain.Account;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.List;

/**
 * JdbcTemplate的最基本用法
 */
public class JdbcTemplateDemo1 {
    public static void main(String[] args) {
        //准备数据源：spring的内置数据源
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://localhost:3306/spring_study");
        ds.setUsername("root");
        ds.setPassword("12345678");

        //1.创建JdbcTemplate对象
        JdbcTemplate jt = new JdbcTemplate();
        //给jt设置数据源
        jt.setDataSource(ds);
        //2.执行操作
        jt.execute("insert into t_account(name,money)values('支付宝',1000)");
        System.out.println("插入成功");
        String name = "支付宝";
        List<Account> accounts = jt.query("select * from t_account where name = ?",new BeanPropertyRowMapper<Account>(Account.class),name);
        if(accounts.isEmpty()){
            System.out.println("查询为空");;
        }
        if(accounts.size()>1){
            throw new RuntimeException("结果集不唯一");
        }
        Account account = accounts.get(0);
        System.out.println(account);
    }
}
