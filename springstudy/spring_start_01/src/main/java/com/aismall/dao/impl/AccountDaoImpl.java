package com.aismall.dao.impl;

import com.aismall.dao.IAccountDao;

/**
 * 账户的持久层实现类：主要为了模拟使用，因为没有链接数据库，也没有使用mybatis框架
 */
public class AccountDaoImpl implements IAccountDao {
    /*模拟保存*/
    public  void saveAccount(){
        System.out.println("保存了账户");
    }
}
