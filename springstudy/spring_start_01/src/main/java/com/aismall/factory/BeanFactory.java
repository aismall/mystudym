package com.aismall.factory;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 一个创建Bean对象的工厂
 *
 * Bean：在计算机英语中，有可重用组件的含义。
 * JavaBean：用java语言编写的可重用组件。
 *      javabean > 实体类
 *
 *   它就是创建我们的service和dao对象的。
 *
 *   第一个：需要一个配置文件来配置我们的service和dao
 *           配置的内容：唯一标识=全限定类名（key=value)
 *   第二个：通过读取配置文件中配置的内容，反射创建对象
 *
 *   我的配置文件可以是xml也可以是properties
 */

/*
*
* 用于反射创建对象
*   key=accountDao
*   value=com.aismall.dao.impl.AccountDaoImpl
*   key=accountService
*   value=com.aismall.service.impl.AccountServiceImpl
*
* */
public class BeanFactory {
    //1.定义一个Properties对象（用来读取配置文件）
    private static Properties props;
    //2.定义一个Map,用于存放我们要创建的对象。我们把它称之为容器，类似于spring的容器
    private static Map<String,Object> beans;

    //使用静态代码块为Properties对象赋值
    static {
        try {
            //3.实例化对象
            props = new Properties();
            //4.获取properties文件的流对象，使用类加载器来获取，避免路径问题
            InputStream in = BeanFactory.class.getClassLoader().getResourceAsStream("bean.properties");
            //5.将配置文件内容加载到Properties对象中
            props.load(in);
            //6.实例化容器
            beans = new HashMap<String,Object>();
            //7.取出配置文件中所有的Key
            Enumeration keys = props.keys();
            //遍历枚举
            while (keys.hasMoreElements()){
                //8.取出每个Key
                String key = keys.nextElement().toString();
               // System.out.println(key);
                //9.根据key获取value
                String beanPath = props.getProperty(key);
                //System.out.println(beanPath);
                //10.反射创建对象：使用类加载器，最为 map 中的 value 属性
                Object value = Class.forName(beanPath).newInstance();
                //把key和value存入容器中，（key）accountService=（value）com.aismall.service.impl.AccountServiceImpl
                beans.put(key,value);
            }
        }catch(Exception e){
            throw new ExceptionInInitializerError("初始化properties失败！");
        }
    }

    /*根据bean的名称获取对象*/
    //注意此处的返回值类型应该是一个Object类型（不能为具体的类型，反射创建的类型不是一个）
    public static Object getBean(String beanName){
        System.out.println(beans.get("accountServiceImpl"));
        return beans.get(beanName);
    }
}
