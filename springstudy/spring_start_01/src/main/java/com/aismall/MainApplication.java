package com.aismall;

import com.aismall.factory.BeanFactory;
import com.aismall.service.IAccountService;
/**
 * 模拟一个表现层，用于调用业务层
 */
public class MainApplication {
    public static void main(String[] args) {
        //使用new关键字创建对象（我们要解决的就是不用new关键字，使用反射创建对象）
        //IAccountService as = new AccountServiceImpl();
        //使用反射创建对象
        IAccountService as = (IAccountService) BeanFactory.getBean("accountServiceImpl");
        System.out.println(as);
        as.saveAccount();

    }
}
