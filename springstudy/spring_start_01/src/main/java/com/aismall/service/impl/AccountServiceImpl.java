package com.aismall.service.impl;
import com.aismall.dao.IAccountDao;
import com.aismall.factory.BeanFactory;
import com.aismall.service.IAccountService;

/**
 * 账户的业务层实现类
 */
public class AccountServiceImpl implements IAccountService {
    //使用new关键字创建对象
    //private IAccountDao accountDao = new AccountDaoImpl();
    public void  saveAccount(){
        //使用工厂模式创建对象
        IAccountDao ad = (IAccountDao)BeanFactory.getBean("accountDaoImpl");
        ad.saveAccount();
    }
}
