package com.aismall.forkjoin10;
/**
 * java.util.concurrent包下的实现类：ForkJoinTask，ForkJoinPool
 *
 * ForkJoin特点：工作窃取
 *      ForkJoin中维护的是双端队列，这样才可以工作窃取，当一个执行快的线程把本队列中的任务做完，去帮助别的
 *      线程执行任务，这时两个线程一个从队尾执行，一个从对头执行（类似于执行快的线程偷偷执行了执行慢的线程的任务）
 *
 * */

/**
 * 用一个计算任务为例（难度依次增加）：
 *  1.普通方法计算
 *  2.使用ForkJoin
 *  3.使用Stream并行流
 *  */

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

/**ForkJoin使用方法：
 *      1、创建一个自己计算任务类，继承ForkJoinTask，里面是一个任务ForkJoinTask
 *      2、创建ForkJoinPool的实现类，通过它里面的execute(ForkJoinTask task)方法来执行ForkJoinTask
 *      3，通过ForkJoinPool中的submit(ForkJoinTask<T> task)来提交ForkJoinTask任务并执行
 *
 * 注意：ForkJoinPool是一个普通类，ForkJoinTask是抽象类
 *       我们一般继承ForkJoinTask的子类（也是抽象类）：
 *              需要返回值继承：RecursiveTask
 *              不需要返回值继承：RecursiveAction
 * */
public class ForkJoinTest{
    public static void main(String[] args)throws Exception {
        //test01();
       test02();
        //test3();
    }

    //普通方法计算
    public static void test01(){
        Long sum=0L;
        //开始时间
        long start=System.currentTimeMillis();
        for (Long i = 1L; i <= 10_0000_0000; i++) {
            sum += i;
        }
        //结束时间
        long end = System.currentTimeMillis();
        //计算使用时间
        System.out.println("sum="+sum+" 时间："+(end-start));
    }

    // 使用ForkJoin
    public static void test02() throws ExecutionException, InterruptedException {
        //开始时间
        long start = System.currentTimeMillis();
        //创建ForkJoinPool的实例，是用来执行ForkJoinTask
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        //创建ForkJoinTask
        ForkJoinTask<Long> task = new ForkJoinDemo(0L, 10_0000_0000L);
        //提交任务
        ForkJoinTask<Long> submit = forkJoinPool.submit(task);
        //获得结果
        Long sum = submit.get();
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+" 时间："+(end-start));
    }
    public static void test3(){
        long start = System.currentTimeMillis();
        // Stream并行流
        long sum = LongStream.rangeClosed(0L,10_0000_0000L).parallel().reduce(0, Long::sum);
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+"时间："+(end-start));
    }
}

class ForkJoinDemo extends RecursiveTask<Long> {
    private Long start;
    private Long end;
    private Long temp = 10000L;// 临界值

    public ForkJoinDemo(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    //计算方法
    @Override
    protected Long compute() {
        if ((end - start) < temp) {//小于临界值，使用普通方法
            Long sum = 0L;
            for (Long i = start; i <= end; i++) {
                sum += i;
            }
            return sum;
        } else { // 大于临界值：ForkJoin 递归
            long middle = (start + end) / 2;  // 中间值
            ForkJoinDemo task1 = new ForkJoinDemo(start, middle);
            task1.fork(); // 拆分任务，把任务压入线程队列
            ForkJoinDemo task2 = new ForkJoinDemo(middle + 1, end);
            task2.fork(); // 拆分任务，把任务压入线程队列
            return task1.join() + task2.join();//合并任务并返回
        }
    }
}