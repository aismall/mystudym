package com.aismall.produceAndconsumer02;
//synchronized版本的生产消费问题：synchronized ，wait，notify

/*线程之间的通信，生产者和消费者的问题，等待唤醒机制
* 线程交替执行，A   B  同时操作一个变量，num=0
* A:num+1
* B:num-1
* */

/*如果存在四个线程：连个两个生产者线程（A和C），两个消费者线程（B和D）
* 就会出现问题：当生产者线程A生产完之后，可能会唤醒生产者C，这时候num的值就有可能为2
*               当消费者线程B消费完之后，可能会唤醒消费者D，这时候num的值就有可能为-1*/

/*解决办法：如果使用【if】的话，只会判断一次，才会出现上面的情况（虚假唤醒）
*           如果使用【循环】，把等待放在循环中，每次其他线程执行完，唤醒一个线程的时候
*           都会进行判断*/
public class test01 {
    public static void main(String[] args) throws InterruptedException{
        //并发：多个线程共享一个资源资源
        //创建多个线程，把资源直接丢入线程
        Data data=new Data();

        new Thread(()->{
            for(int i=0;i<10;i++){
            try {
                data.increment();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }},"A：producer").start();

        new Thread(()->{
            for(int i=0;i<10;i++){
            try {
                data.decrement();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }},"B：consumer").start();

        new Thread(()->{
            for(int i=0;i<10;i++){
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }},"C：producer").start();

        new Thread(()->{
            for(int i=0;i<10;i++){
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }},"D：consumer").start();


    }
}

/*等待，业务，通知*/
class Data{//数据类,资源类
    private int num=0;
    //+1
    public synchronized  void increment() throws InterruptedException{
        while(num!=0){
            this.wait();//等待
        }
        num++;//业务
        System.out.println(Thread.currentThread().getName()+"==>"+num);
        this.notifyAll();//通知
    }
    //-1
    public synchronized  void decrement()throws InterruptedException{
        while(num==0){
            this.wait();//等待
        }
        num--;//业务
        System.out.println(Thread.currentThread().getName()+"==>"+num);
        this.notifyAll();//通知
    }
}