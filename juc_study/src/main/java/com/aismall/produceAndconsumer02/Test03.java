package com.aismall.produceAndconsumer02;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


//Condition：实现精准的通知和唤醒作用

/*A执行完调用B
 * B执行完调用C
 * C执行完调用A
 * */
public class Test03 {
    public static void main(String[] args) throws InterruptedException{
        //并发：多个线程共享一个资源资源
        //创建多个线程，把资源直接丢入线程
        Data3 data=new Data3();
        new Thread(()->{
            for(int i=0;i<12;i++){
                try {
                    data.printA();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }},"Thread_A").start();

        new Thread(()->{
            for(int i=0;i<12;i++){
                try {
                    data.printB();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }},"Thread_B").start();

        new Thread(()->{
            for(int i=0;i<12;i++){
                try {
                    data.printC();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }},"Thread_C").start();

    }
}

/*等待，业务，通知*/
class Data3{ //数据类,资源类
    private int flag=1;//flag为1 A线程执行，2 B 线程执行，3 C 线程执行
    Lock lock=new ReentrantLock();
    Condition conditionA=lock.newCondition();
    Condition conditionB=lock.newCondition();
    Condition conditionC=lock.newCondition();

    public void printA(){
        try {
            lock.lock();
            while (flag!=1){
                conditionA.await();//等待
            }
            System.out.println(Thread.currentThread().getName()+".....AAAAA....");
            flag=2;//唤醒B线程
            conditionB.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public void printB(){
        try{
            lock.lock();
            while (flag!=2){
                conditionB.await();//等待
            }
            System.out.println(Thread.currentThread().getName()+".....BBBBB....");
            flag=3;//唤醒B线程
            conditionC.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public void printC(){
        try {
            lock.lock();
            while (flag!=3){
                conditionC.await();//等待
            }
            System.out.println(Thread.currentThread().getName()+".....CCCCC....");
            flag=1;//唤醒B线程
            conditionA.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
