package com.aismall.produceAndconsumer02;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
//JUC 版本的生产消费问题：Lock，await，single

/*Lock：代替synchronized关键字的作用。 通过lock.newCondition创建一个条件
* condition.await：代替wait
* condition.single：代替notify*/

/*通过刚才的演示可以发现，这种实现的效果和使用synchronized，wait，notify的效果是一样的
* 任何一种新的技术绝不会只是对原来技术的一种覆盖，肯定有它的优势，和对原来技术的补充，
*
* Condition：实现精准的通知和唤醒作用
*
* */
public class Test02 {
    public static void main(String[] args) throws InterruptedException{
        //并发：多个线程共享一个资源资源
        //创建多个线程，把资源直接丢入线程
        Data2 data=new Data2();
        new Thread(()->{
            for(int i=0;i<15;i++){
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }},"A：producer").start();

        new Thread(()->{
            for(int i=0;i<15;i++){
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }},"B：consumer").start();

        new Thread(()->{
            for(int i=0;i<15;i++){
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }},"C：producer").start();

        new Thread(()->{
            for(int i=0;i<15;i++){
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }},"D：consumer").start();


    }
}

/*等待，业务，通知*/
class Data2{ //数据类,资源类
    private int num=0;
    Lock lock=new ReentrantLock();
    Condition condition=lock.newCondition();

    //+1
    public void increment() throws InterruptedException{
        try {
            lock.lock();
            while(num!=0) {
                condition.await();//等待
            }
            num++;//业务
            System.out.println(Thread.currentThread().getName()+"==>"+num);
            condition.signalAll();//通知
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    //-1
    public synchronized  void decrement()throws InterruptedException{
        try {
            lock.lock();
            while(num==0){
               condition.await();//等待
            }
            num--;//业务
            System.out.println(Thread.currentThread().getName()+"==>"+num);
            condition.signalAll();//通知
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
