package com.aismall.somelocks.reentrantlock;

public class SynchronizedReentrantLock {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    System.out.println("第1次获取锁，这个锁是："+this);
                    int index=1;
                    while (true){
                        synchronized (this){
                            System.out.println("第"+(++index)+"次获取锁，这个锁是："+this);
                        }
                        //重复获取十次锁
                        if(index==10){
                            break;
                        }
                    }
                }
            }
        }).start();

        System.out.println("运行结束，无需解锁");
    }

}
