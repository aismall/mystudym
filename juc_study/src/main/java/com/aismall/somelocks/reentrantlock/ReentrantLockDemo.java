package com.aismall.somelocks.reentrantlock;

import java.util.concurrent.locks.ReentrantLock;
//加了多少次，解了多少次
public class ReentrantLockDemo {
    public static void main(String[] args) {
        //创建一个可重入锁
        ReentrantLock lock=new ReentrantLock();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //加锁
                    lock.lock();
                    System.out.println("第1次获取锁，这个锁是：" + lock);
                    int index = 1;
                    while (true) {
                        try {
                            //加锁
                            lock.lock();
                            System.out.println("第" + (++index) + "次获取锁，这个锁是：" + lock);

                            if (index == 10) {
                                break;
                            }
                        } finally {
                            //解锁
                            System.out.println("第" + (index) + "次解锁，这个锁是：" + lock);
                            lock.unlock();
                        }
                    }
                } finally {
                    //解锁
                    lock.unlock();
                    System.out.println("第1"  + "次解锁，这个锁是：" + lock);
                }
            }
        }).start();
    }
}
