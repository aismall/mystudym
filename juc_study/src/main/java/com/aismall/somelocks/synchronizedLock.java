package com.aismall.somelocks;
/**
 * 可重入锁：synchronized版本
 *      对于线程Thread_A:调用sendMsg方法的时候拿到一把锁，然后会自动拿到里面的call()方法的锁
 *      等两个方法都执行完就释放锁，*/
public class synchronizedLock {
    public static void main(String[] args) {
        Phone phone=new Phone();
        new Thread(()->{
            phone.sendMSG();
        },"Thread_A").start();

        new Thread(()->{
            phone.sendMSG();
        },"Thread_B").start();
    }
}

class Phone{
    public  synchronized void sendMSG(){//这里有锁
        System.out.println(Thread.currentThread().getName()+" :sendMSG.....");
        call();//这里也有锁
    }
    public  synchronized void call(){
        System.out.println(Thread.currentThread().getName()+" :call.....");
    }
}