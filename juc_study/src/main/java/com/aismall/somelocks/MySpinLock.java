package com.aismall.somelocks;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 自旋锁要使用CAS：比较并且替换
 *      */
public class MySpinLock {
    //创建一个原子引用的对象，这个是java自带的带有CAS的类： AtomicReference
    AtomicReference<Thread> atomicReference = new AtomicReference<>();

    // 加锁
    public void myLock(){
        Thread thread = Thread.currentThread();
        // 自旋锁：
        /**
         * 解析：因为创建atomicReference实例使用的是空参构造
         *      当创建对象的时候，默认为Thread类型的null
         *      第一个线程进来，利用空参构造创建一个对象，得到默认值null
         *      此时判断成立，将其替换为thread对象的地址，返回true，所以第一个
         *      进来的线程不会进入while循环
         *      第二个线程进来以后，由于默认值被替换为thread的地址不是null，所以进入while循环
         *      并且一直等待，直到第一个线程调用解锁方法，把这个thread的值替换为null，第二个线程才能执行
         *        */
        while (!atomicReference.compareAndSet(null,thread)){
            System.out.println(Thread.currentThread().getName() + "==> 拿不到锁");
        }
    }
    // 加锁
    public void myUnLock(){
        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName() + "==> 解锁");
        atomicReference.compareAndSet(thread,null);
    }

    public static void main(String[] args) {
        //创建一个锁
        MySpinLock lock=new MySpinLock();
        Thread thread01=new Thread(new Runnable() {
            @Override
            public void run() {
                //加锁
                lock.myLock();
                System.out.println("thread01==> 拿到锁");
                try {
                    //让线程睡1毫秒
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //解锁
                lock.myUnLock();
            }
        },"thread01");
        Thread thread02=new Thread(new Runnable() {
            @Override
            public void run() {
                //加锁
                lock.myLock();
                System.out.println("thread01==> 拿到锁");
                //解锁
                lock.myUnLock();
            }
        },"thread02");
        thread01.start();
        thread02.start();
    }
}
