package com.aismall.somelocks;

import java.util.concurrent.TimeUnit;

public class DeadLock {
    public static void main(String[] args) {
        String lockA="lockA";
        String lockB="lockB";
        MyDeadLockThread A=new MyDeadLockThread(lockA,lockB);
        MyDeadLockThread B=new MyDeadLockThread(lockB,lockA);
        new Thread(A).start();
        new Thread(B).start();
    }
}

class MyDeadLockThread implements Runnable{

    private String lockA;
    private String lockB;

    public MyDeadLockThread(String lockA, String lockB) {
        this.lockA = lockA;
        this.lockB = lockB;
    }

    @Override
    public void run() {
        synchronized (lockA){
            System.out.println(Thread.currentThread().getName() + "：拿到"+lockA+"=>想得到"+lockB);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lockB){
                System.out.println(Thread.currentThread().getName() + "：拿到"+lockB+"=>想得到"+lockA);
            }
        }
    }
}