package com.aismall.somelocks;

/**
 * 可重入锁：lock版本
 * 注意：拿锁和解锁的顺序相反，最后哪的最先解锁*/
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockDemo  {
    public static void main(String[] args) {
        Phone02 phone02=new Phone02();
        new Thread(()->{
            phone02.sendMSG();
        },"Thread_A").start();

        new Thread(()->{
            phone02.sendMSG();
        },"Thread_B").start();
    }
}
class Phone02{
    Lock lock= new ReentrantLock();
    public void sendMSG(){
        // 细节问题：lock.lock(); lock.unlock()必须配对，否则就会死在里面
        lock.lock(); //锁A
        lock.lock();//锁B
        try {
            System.out.println(Thread.currentThread().getName() + " sendMSG");
            call(); // 调用：锁C
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();//解锁B
            lock.unlock();//解锁A
        }
    }
    public void call(){
        lock.lock();//锁C
        try {
            System.out.println(Thread.currentThread().getName() + "  call");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();//解锁C
        }
    }
}