package com.aismall.demo01;

public class SaleTicketDemo01 {
    public static void main(String[] args) {
        //并发：多线程操作同一个资源，把资源直接丢入线程
        Ticket ticket=new Ticket();
        new Thread(()->{//每个线程都买60次
            for (int i=0;i<60;i++){
                ticket.sale();
            }
        },"A").start();

        new Thread(()->{
            for (int i=0;i<60;i++){
                ticket.sale();
            }
        },"B").start();

        new Thread(()->{
            for (int i=0;i<60;i++){
                ticket.sale();
            }
        },"C").start();

    }
}
//资源类
class Ticket{
    //定义票数
    private int ticket=50;
    public  void sale(){
        if(ticket>0){
            System.out.println(Thread.currentThread().getName()+":sale："+(ticket--)+" ticket，剩余："+ticket);
        }
    }
}