package com.aismall.demo01;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SaleTiketDemo02 {
    public static void main(String[] args) {
        //并发：多线程操作同一个资源，把资源直接丢入线程
        Ticket2 ticket=new Ticket2();
        new Thread(()->{//每个线程都买60次
            for (int i=0;i<60;i++){
                ticket.sale();
            }
        },"A").start();

        new Thread(()->{
            for (int i=0;i<60;i++){
                ticket.sale();
            }
        },"B").start();

        new Thread(()->{
            for (int i=0;i<60;i++){
                ticket.sale();
            }
        },"C").start();
    }
}
//资源类
class Ticket2{
    //定义票数
    private int ticket=50;

    //从底层源码可以看出，不加参数为非公平锁，加参数True为公平锁
    Lock lock=new ReentrantLock();//第一步

    public void sale(){
        lock.lock();//第二步：
        try {//业务代码
            if(ticket>0){
                System.out.println(Thread.currentThread().getName()+":sale："+(ticket--)+" ticket，剩余："+ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
