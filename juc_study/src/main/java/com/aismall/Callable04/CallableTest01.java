package com.aismall.Callable04;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/*Callable接口在：java.util.concurrent.callable接口
*       功能：类似于Runnable接口，可以创建一个可以被其他线程执行的实例
*       差别：Callable接口
*               可以有返回值
*               可以抛出异常
*               方法不同，一个是run(),一个是call()*/
public class CallableTest01 {
    public static void main(String[] args) {
       /* FutureTask:Runnable接口的实现类
        Callable04-->FutureTask-->Runnable-->Thread
        1.MyThread2 myThread2=new MyThread2();
        2.FutureTask futureTask=new FutureTask(myThread2);
        3.new Thread(futureTask).start();
*/
        new Thread(new FutureTask<String>(new MyThread2()),"Thread_A").start();
        MyThread2 myThread2=new MyThread2();
        FutureTask futureTask=new FutureTask(myThread2);
        new Thread(futureTask,"Thread_B").start();

        try {
            String returnValue=(String) futureTask.get();//此方法会产生阻塞，一般往后放
            System.out.println(returnValue);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
}

//注意：泛型的参数等于方法的返回值
/*
* Thread-->Runnable
* Runnable-->FutureTask-->Callable04
* Callable04-->FutureTask-->Runnable-->Thread
* */
class MyThread2 implements Callable<String>{
    @Override
    public String call() throws  InterruptedException{
        System.out.println("Callable04.call.....");
        return "aismall";
    }
}