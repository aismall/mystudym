package com.aismall.jmm;

import java.util.concurrent.TimeUnit;

/**
 * JMM：Java内存模型
 *
 * 请你谈谈你对 Volatile 的理解？
 *     Volatile 是 Java 虚拟机提供轻量级的同步机制
 *         1、保证可见性
 *         2、【不保证原子性】
 *         3、禁止指令重排
 *
 * 什么是JMM？
 *     JMM ： Java内存模型，不存在的东西，一种约定
 *
 * 关于JMM的一些同步的约定：
 *     1、线程解锁前，必须把共享变量立刻刷回【主存】。
 *     2、线程加锁前，必须读取主存中的最新值到工作内存中。
 *     3、加锁和解锁是同一把锁
 *
 * 线程之间的变量是相互不可见的，如果两个线程同时修改一个变量的值，要使用Volatile关键字保证变量的【可见性】
 **/

/**
 * Volatile 保证可见性举例如下：*/
public class JMMDemo {
    //private  static int num=0;
    private volatile static int num=0;//【保证线程之间变量的可见性】
    public static void main(String[] args) {
        //在主线程中开启一个测试线程
        //测试线程先拿到num值，此时陷入死循环
      new Thread(()->{
          while(num==0){
              //死循环
          }
      },"Thread_A").start();

      //主线程睡眠
      try {
          //System.out.println(Thread.currentThread().getName());
          TimeUnit.SECONDS.sleep(2);
          //System.out.println(Thread.currentThread().getName());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
        //主线程睡眠结束，在回去CPU执行时间的时，修改了num的值，但是这个值对线程Thread_A不可见，
        //主线程执行结束后，Thread_A 线程阻塞，陷入死循环
      num=1;
      System.out.println(num);
    }
}
