package com.aismall.jmm;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 为了保证原子性操作：我们引入java.util.concurrent.atomic包，包中对于不同类型的变量对象不同的类
 * 对于整数：AtomicInteger类*/
public class JMMDemo03 {
    //保证可见性volatile，保证原子性AtomicInteger
    private volatile static AtomicInteger num=new AtomicInteger();

    //给这个静态方法加锁可以保证原子性：synchronized或者lock，
    //我们不使用锁，如何保证原子性？
    //保证原子性
    public static void add(){
        num.getAndIncrement();
    }

    public static void main(String[] args) {
        atomic();
    }

    public static void atomic(){
        //理论上num的值为2万
        for(int i=1;i<=20;i++){
            new Thread(()->{
                for(int j=0;j<1000;j++){
                    add();//调用静态方法
                }
            }).start();
        }
        while (Thread.activeCount()>2){
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName()+" "+num);
    }
}
