package com.aismall.jmm;
/**
 * 原子性： 不可分割
 *          线程A在执行任务的时候，不能被打扰的，也不能被分割。要么同时成功，要么同时失败。
 *
 * Volatile 不保证原子性举例如下：
 * */

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 为了保证原子性操作：我们引入java.util.concurrent.atomic包，包中对于不同类型的变量对象不同的类
 *          对于整数：AtomicInteger类*/
public class JMMDemo2 {
    //Volatile 不保证原子性
    private volatile static int num = 0;

    public static void add() {
        num++;//不保证原子性
    }

    public static void main(String[] args) {
       noAtomic();
    }


    public static void noAtomic(){
        //理论上num结果应该为 2 万：20*1000
        for (int i = 1; i <= 20; i++) {
            new Thread(()->{
                for (int j = 0; j < 1000 ; j++) {
                    add();//调用静态方法
                }
            }).start();
        }
        while (Thread.activeCount()>2){ //查看存活线程个数，默认两个main gc在执行
            Thread.yield();//线程礼让
        }
        System.out.println(Thread.currentThread().getName() + " " + num);
    }
}
