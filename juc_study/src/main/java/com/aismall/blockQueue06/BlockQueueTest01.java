package com.aismall.blockQueue06;

/*
 * 【BlockQueue】（继承了Collection接口）:阻塞队列，java.util.concurrent.BlockQueue接口
 *      队列：FIFO，先进先出
 *      阻塞：当队列满了就阻塞了，前面没出去，后面进不来
 *      取：当队里里空的时候，阻塞，等待生产
 *      存：当队列里满的时候。阻塞，等待消费
 *
 * 常用实现类：
 *      ArrayBlockQueue：数组阻塞队列
 *      LinkedBlockQueue：链表阻塞对列
 *      SynchronousQueue：同步阻塞对列
 *
 * 注意：Collection接口-->Queue接口-->BlockQueue接口-->常用实现类：ArrayBlockQueue
 *       Collection接口-->List接口-->ArrayList实现类，LinkedList实现类
 *      */

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/*
* 四组API（添加，删除）：
* 1.抛出异常：add，remove，element（检查队首元素）
* 2.不会抛出异常，有返回值：offer，poll，peek（检查队首元素）
* 3.阻塞等待：put，take
* 4.超时等待：offer，poll
* */
public class BlockQueueTest01 {
    public static void main(String[] args) {
        test01();
    }

    /**
     * 抛出异常
     */
    public static void test01(){
        BlockingQueue blockingQueue=new ArrayBlockingQueue(3);//队列大小为3
        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));
        //System.out.println(blockingQueue.add("d"));//报错


        System.out.println("===========================");
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        //System.out.println(blockingQueue.remove());//报错
    }

    /**
     * 有返回值，没有异常
    */
    public static void test2(){
        // 队列的大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
        System.out.println(blockingQueue.offer("a"));
        System.out.println(blockingQueue.offer("b"));
        System.out.println(blockingQueue.offer("c"));
        // System.out.println(blockingQueue.offer("d")); // false 不抛出异常！

        System.out.println("============================");
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll()); // null 不抛出异常！
    }

    /**
     * 等待，阻塞（一直阻塞）
     * */
    public static void test3() throws InterruptedException {
        // 队列的大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
        // 一直阻塞
        blockingQueue.put("a");
        blockingQueue.put("b");
        blockingQueue.put("c");
        // blockingQueue.put("d"); // 队列没有位置了，一直阻塞

        System.out.println("======================");
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        //System.out.println(blockingQueue.take()); // 没有这个元素，一直阻塞
    }


    /**
     * 等待，阻塞（等待超时)
     * */
    public static void test4() throws InterruptedException {
        // 队列的大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
        blockingQueue.offer("a");
        blockingQueue.offer("b");
        blockingQueue.offer("c");
        // blockingQueue.offer("d",2,TimeUnit.SECONDS); // 等待超过2秒就退出


        System.out.println("===============");
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        //blockingQueue.poll(2,TimeUnit.SECONDS); // 等待超过2秒就退出
    }
}

