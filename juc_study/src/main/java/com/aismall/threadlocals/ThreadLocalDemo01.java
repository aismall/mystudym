package com.aismall.threadlocals;

public class ThreadLocalDemo01 {
    //ThreadLocal变量t1是String类型
    public static final ThreadLocal<String> t1 = new ThreadLocal();
    //ThreadLocal变量t2是String类型
    public static final ThreadLocal<String> t2 = new ThreadLocal();

    public static void main(String[] args) {
        //线程1
        Thread thread_01=new Thread(new Runnable() {
            @Override
            public void run() {
                t1.set("Thread_01 String01");
                t2.set("Thread_01 String02");
                String str01 = t1.get();
                String str02 = t2.get();
                System.out.println(str01);
                System.out.println(str02);
            }
        },"Thread_01");

        //线程2
        Thread thread_02=new Thread(new Runnable() {
            @Override
            public void run() {
                t1.set("Thread_02 String01");
                t2.set("Thread_02 String02");
                String str01 = t1.get();
                String str02 = t2.get();
                System.out.println(str01);
                System.out.println(str02);
            }
        },"Thread_02");

        thread_01.start();
        thread_02.start();
        //主线程
        String str01=t1.get();
        String str02=t1.get();
        System.out.println(Thread.currentThread().getName()+"==>"+str01);
        System.out.println(Thread.currentThread().getName()+"==>"+str02);
    }
}
