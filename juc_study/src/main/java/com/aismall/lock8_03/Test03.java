package com.aismall.lock8_03;

import java.util.concurrent.TimeUnit;

/*8锁问题：关于锁的8个问题
 *  5.由于两个都是【静态方法】，并且使用synchronized关键字修饰，所以锁的是同一个对象【Class】
 *  和之前一样，还需要抢锁,打印sendMsg
 *  6.由于锁的是【Class】，两个对象也需要抢锁，打印sendMsg
 *   */
public class Test03 {
    public static void main(String[] args) {
        Phone3 phone =new Phone3();
        Phone3 phone2=new Phone3();
        new Thread(()->{
            phone.sendMsg();
        },"thread_A").start();

        //使用java.util.concurrent.TimeUtil来进行睡眠
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

       new Thread(()->{
           phone2.call();
       },"Thread_C").start();
    }
}

class Phone3{
    /*静态方法：类以加载就有了，不属于任何对象，属于类的实例共有
    * 此时锁的是Class，也就是模板*/
    public static synchronized void sendMsg(){
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("sendMsg.....");
    }
    public static synchronized void call(){
        System.out.println("call....");
    }
}
