package com.aismall.lock8_03;

import java.util.concurrent.TimeUnit;

/*8锁问题：关于锁的8个问题
 *  7.由于被synchronized关键字修饰的静态方法的锁对象是【Class】
 *  被synchronized关键字修饰的普通方法的锁对象是【调用者】
 *  不用抢锁，虽然先执行sendMsg，但是它睡眠了，所以CPU会调度其他线程执行（由于不是同一把锁）
 *  打印call
 *  8.锁的对象不同，静态方法的锁对象是【Class】，普通方法的锁对象是【调用者】
 *   */

/*
*总结：锁的对象就是两个东西，一个new出来的【调用者】，一个static修饰的【Class】 */
public class Test04 {
    public static void main(String[] args) {
        Phone4 phone =new Phone4();
        new Thread(()->{
            phone.sendMsg();
        },"thread_A").start();

        //使用java.util.concurrent.TimeUtil来进行睡眠
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()->{
            phone.call();
        },"Thread_B").start();

    }
}

class Phone4{
    /*静态方法：类以加载就有了，不属于任何对象，属于类的实例共有
     * 此时锁的是Class，也就是模板*/
    public static synchronized void sendMsg(){
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("sendMsg.....");
    }
    public synchronized void call(){
        System.out.println("call....");
    }

}
