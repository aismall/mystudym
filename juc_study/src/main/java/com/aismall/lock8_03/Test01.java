package com.aismall.lock8_03;

import java.util.concurrent.TimeUnit;

/*8锁问题：关于锁的8个问题
*   1.synchronized 锁只有一个锁，线程A先拿到锁，所以先执行线程A中的打印操作，在执行线程B
*   2.因为有锁，如果不设置行线程等待【wait】，使用synchronized修饰的方法会【抱着锁睡觉】
*   */
public class Test01 {
    public static void main(String[] args) {
        Phone phone =new Phone();
        new Thread(()->{
            phone.sendMsg();
        },"thread_A").start();

        //使用java.util.concurrent.TimeUtil来进行睡眠
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()->{
            phone.call();
        },"Thread_B").start();
    }
}

class Phone{
    public synchronized void sendMsg(){
        System.out.println("sendMsg.....");
    }

    public synchronized void call(){
        System.out.println("call....");
    }
}
