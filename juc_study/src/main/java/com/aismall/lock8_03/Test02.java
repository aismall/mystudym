package com.aismall.lock8_03;

import java.util.concurrent.TimeUnit;

/*8锁问题：关于锁的8个问题
 *   3.synchronized锁，同被synchronized关键字修饰的方法才会抢这个锁
 *   由于hello没有被synchronized关键字修饰，所以不用抢这个锁
 *   虽然sendMsg先拿到了锁，但是它睡眠了，所以CPU会调度执行下一个线程（线程中的方法不带锁），
 *   打印hello
 *   4.synchronized 锁，锁的是【方法的调用者（phone）】，由于有两个调用者，所以有两个锁，不用抢
 *   虽然先执行了sensMsg方法，但是它睡眠了，所以CPU会调度执行下一个线程（线程中的方法带锁，但是不是同一把锁）
 *   打印call
 *   */
public class Test02 {
    public static void main(String[] args) {
        Phone2 phone =new Phone2();
        Phone2 phone2=new Phone2();
        new Thread(()->{
            phone.sendMsg();
        },"thread_A").start();

        //使用java.util.concurrent.TimeUtil来进行睡眠
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()->{
           phone2.call();
       },"Thread_B").start();

    }
}

class Phone2{
    //因为有锁，如果不设置行线程等待，使用synchronized修饰的方法会【抱着锁睡觉】
    public synchronized void sendMsg(){
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("sendMsg.....");
    }

    public synchronized void call(){
        System.out.println("call....");
    }
}
