package com.aismall.unsafe03;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

//Set：也是不安全的
//并发下会报错，java.util.ConcurrentModificationException：并发修改异常
public class SetTest {
    public static void main(String[] args) {
        //单个线程十分安全，但是并发下安全吗？
        //注意：Set和List同属于java.util.Collection接口
        Set<String> setString=new HashSet<>();
        setString.add("aismall");
        /*HashSet底层：使用map给put一个值
               public boolean add(E e) {
                    return map.put(e, PRESENT)==null;
                    }
         */

        /*
        //并发下会报错：并发下Set不安全
        Set<String> set=new HashSet<>();
        for(int i=0;i<20;i++){
            new Thread(()->{
                set.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(set);
            },String.valueOf(i)).start();
        }
        */

         /*解决方案1：使用vector(默认安全)
                    List<String> list=new Vector();
                    这个方法过于老旧
          解决方法2：使用Collections中的静态方法，synchronizedList
                    Set<String> set= Collections.synchronizedSet(new HashSet<>());
          解决方法3：使用JUC包下的copyOnWrite（写入时复制）
                    List<String> list=new CopyOnWriteArraySet<>();

          问题1：CopyOnWrite比Vector优越在哪里？
                  Vector的add方法添加了synchronized锁，效率比较慢
                  CopyOnWrite里面使用的是lock锁，效率快一点
        */
         Set<String> set= Collections.synchronizedSet(new HashSet<>());
         for(int i=0;i<20;i++){
            new Thread(()->{
                set.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(set);
            },String.valueOf(i)).start();
        }

        Set<String> set2=new CopyOnWriteArraySet<>();
        for(int i=0;i<20;i++){
            new Thread(()->{
                set2.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(set);
            },String.valueOf(i)).start();
        }
    }
}
