package com.aismall.unsafe03;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

//map：【不安全】
public class MapTest {
    public static void main(String[] args) {
        //map 是这样用的吗？ 不是，工作中不用HashMap
        //默认构造函数等价于什么？  new HashMap(16,0.75);
        //HashMap默认容量为16，超过12个就会进行扩容

        Map<String,String> map=new HashMap<>();
        for(int i=0;i<20;i++){
            new Thread(()->{
                map.put(Thread.currentThread().getName(),UUID.randomUUID().toString().substring(0,5));
                System.out.println(map);
            },String.valueOf(i)).start();
        }

        /*解决办法：使用java.util.concurrent.ConcurrentHashMap
        * */

      /*  Map<String,String> map=new ConcurrentHashMap<>();
        for(int i=0;i<20;i++){
            new Thread(()->{
                map.put(Thread.currentThread().getName(),UUID.randomUUID().toString().substring(0,5));
                System.out.println(map);
            },String.valueOf(i)).start();
        }*/
    }
}
