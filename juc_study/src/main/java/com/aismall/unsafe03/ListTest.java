package com.aismall.unsafe03;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
//面试时候说的异常：内存溢出，栈溢出，并发修改异常
//List：【不安全】
//并发下会报错，java.util.ConcurrentModificationException：并发修改异常
public class ListTest {
    public static void main(String[] args) {
        //单个线程十分安全，但是并发下安全吗？
       /* List<String> lists= Arrays.asList("aismall","is","very","beautiful");
        lists.forEach(System.out::println);
*/
        //并发下会报错：并发下ArrayList不安全
       /* List<String> list=new ArrayList<>();
        for (int i=0;i<20;i++){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            },String.valueOf(i)).start();
        }*/

        /*解决方案1：使用vector(默认安全)
                    List<String> list=new Vector();
                    这个方法过于老旧
          解决方法2：使用Collections中的静态方法，synchronizedList
                    List<String> list=Collections.synchronizedList(new ArrayList<>());
          解决方法3：使用JUC包下的copyOnWrite（写入时复制）
                    List<String> list=new CopyOnWriteArrayList<>();

          问题1：CopyOnWrite比Vector优越在哪里？
                  Vector的add方法添加了synchronized锁，效率比较慢
                  CopyOnWrite里面使用的是lock锁，效率快一点

        */
        List<String> list=Collections.synchronizedList(new ArrayList<>());
        for(int i=0;i<10;i++){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
        List<String> list2=new CopyOnWriteArrayList<>();
        for(int i=0;i<10;i++){
            new Thread(()->{
                list2.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }
}
