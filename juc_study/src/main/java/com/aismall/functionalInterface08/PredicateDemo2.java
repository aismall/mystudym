package com.aismall.functionalInterface08;


import java.util.function.Predicate;

/**
 * 四大函数时式接口之一：
 *@FunctionalInterface
 * public interface Predicate<T> {.....}
 *
 * 断定型接口：有一个输入参数，返回值只能是布尔值
 * */
public class PredicateDemo2 {
    public static void main(String[] args) {
        //工具类：判断字符串是否为空，为空返回true
       Predicate predicate01= new Predicate<String>(){
           @Override
           public boolean test(String s) {
               return s.isEmpty();
           }
       };
       System.out.println(predicate01.test(""));

       Predicate<String> predicate02=(str)->{return  str.isEmpty();};
       System.out.println(predicate02.test("aismall"));
    }
}
