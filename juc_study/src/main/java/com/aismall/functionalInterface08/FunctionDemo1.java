package com.aismall.functionalInterface08;


import java.util.function.Function;
/**
 * 新时代的程序员要掌握的技能：
 *      lambda表达式
 *      链式编程
 *      函数式接口
 *      Stream流式计算*/


/**
 * 函数式接口： 只有一个方法的接口
 * 四大函数时式接口：java.util.function包下的四个【原生的接口】
 *      Consumer接口
 *      Function接口
 *      Predicate接口
 *      Supplier接口*/

/**
 * 四大函数时式接口之一：
 * @FunctionalInterface
 * public interface Function<T, R> { .....}
 *    传入的参数的泛型T，返回的参数泛型R, 不写默认是Object类型
 *
 * Function 函数型接口, 有一个输入参数，有一个输出
 * 只要是 函数型接口都可以用lambda表达式简化
 *    */

/**
 * */
public class FunctionDemo1 {
    public static void main(String[] args) {
        //工具类：输出输入的值
        //普通使用方法
       Function function01= new Function<String,String>() {
            @Override
            public String apply(String string) {
                return string;
            }
        };
        System.out.println(function01.apply("aismall"));
        //使用Lambda表达式
        Function<String,String> function02=(str)->{return str;};
        System.out.println(function02.apply("aismall"));
    }
}
