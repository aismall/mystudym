package com.aismall.functionalInterface08;

import java.util.function.Consumer;

/**
 * 四大函数时式接口之一：
 * @FunctionalInterface
 * public interface Consumer<T> {......}
 *
 * Consumer 消费型接口:只有输入，没有返回值
 * */
public class ConsumerDemo3 {
    public static void main(String[] args) {
        Consumer<Integer> consumer01=new Consumer<Integer>() {
            @Override
            public void accept(Integer num) {
                int a=num+666;
                System.out.println(a);
            }
        };
        consumer01.accept(100);

        Consumer<Integer> consumer02=(num)->{
            int a=num+666;
            System.out.println(a);
        };
        consumer02.accept(100);//没有输出结果
    }
}
