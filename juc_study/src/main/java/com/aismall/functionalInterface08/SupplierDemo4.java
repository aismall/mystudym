package com.aismall.functionalInterface08;


import java.util.function.Supplier;

/**四大函数时式接口之一：
 * @FunctionalInterface
 * public interface Supplier<T> {.....}
 *
 * Supplier 供给型接口 没有参数，只有返回值
 * */
public class SupplierDemo4 {
    public static void main(String[] args) {
       Supplier<Test> supplier01=new Supplier<Test>() {
            @Override
            public Test get() {
            Test test01 =new Test();
            return test01;
            }
        };

       Test test01=supplier01.get();
       test01.show();


        Supplier<Test> supplier02=()->{
        Test test02 =new Test();
        return test02;
        };
        Test test02=supplier02.get();
        test02.show();
     }
}
class Test{
    public void show(){
        System.out.println("Supplier接口测试。。。");
    }
}
