package com.aismall.stream09;


import java.util.Arrays;
import java.util.List;

/**
 * java.util.stream包：
 *
 * 题目要求：一分钟内完成此题，只能用一行代码实现！
 * 现在有5个用户，筛选：
 * 1、ID 必须是偶数
 * 2、年龄必须大于23岁
 * 3、用户名转为大写字母
 * 4、用户名字母倒着排序
 * 5、只输出一个用户
 * */
public class Test {
    public static void main(String[] args) {
        User u1 = new User(1,"a",21);
        User u2 = new User(2,"b",22);
        User u3 = new User(3,"c",23);
        User u4 = new User(4,"d",24);
        User u5 = new User(6,"e",25);
        //创建一个集合存储这些User
        List<User> list = Arrays.asList(u1, u2, u3, u4, u5);
        // 计算交给Stream流
        // lambda表达式、链式编程、函数式接口、Stream流式计算
        list.stream()
                //找出id是偶数的
                .filter(u->{return u.getId()%2==0;})
                //年龄大于23
                .filter(u->{return u.getAge()>23;})
                //将名字转换为大写
                .map(u->{return u.getName().toUpperCase();})
                //只要第一个
                .limit(1)
                //打印输出
                .forEach(System.out::println);
    }
}
