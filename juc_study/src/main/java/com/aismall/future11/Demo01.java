package com.aismall.future11;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**java.util.concurrent.Future接口：
 * Future接口实现类：
 *      CompletableFuture：
 *      FutureJoinTask:
 *      FutureTask:
 * */

/**
 * 异步调用： CompletableFuture
 * // 异步执行
 * // 成功回调
 * // 失败回调
 *
 * completableFuture是JDK1.8版本新引入的类，这个类实现了俩接口Future和CompletionStage
 *      使用completionStage接口，去支持完成时触发的函数和操作
 *      使用用Future接口，能做一些executorService配合futures做不了的工作
 *          executorService配合future需要等待isDone为true才能知道任务跑完了或者就是用get方法调用的时候会出现阻塞
 *          而completableFuture就可以用then，when等等操作来防止以上的阻塞和轮询isDone的现象出现
 *          【也可以使用get方法调用的时候会出现阻塞】
 *
 *  一个completableFuture对象代表着一个任务：
 *      创建CompletableFuture直接new对象也成
 *      complete意思就是这个任务完成了需要返回的结果
 *      然后用get();方法可以获取到
 */

public class Demo01 {
    public static void main(String[] args) throws InterruptedException,ExecutionException {
        runAsyncVoid();
        supplyAsyncReturn();
    }

    public static void runAsyncVoid()throws InterruptedException,ExecutionException{
        // 没有返回值的 runAsync 异步回调，【Async：异步】
        CompletableFuture<Void> completableFuture=CompletableFuture.runAsync(()->{
            try {
                //当线程沉睡值不阻塞，其他线程可执行
                TimeUnit.SECONDS.sleep(6);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"runAsync=>Void");
        });

        System.out.println("666666");

        completableFuture.get();//获取阻塞返回结果

        System.out.println("88888888");
    }

    public static void supplyAsyncReturn() throws InterruptedException,ExecutionException{
        //又返回值的supplyAsync，【async：异步】
        CompletableFuture<Integer> completableFuture=CompletableFuture.supplyAsync(()->{
            System.out.println(Thread.currentThread().getName()+"supplyAsync=>Integer");
            int i = 10/0;
            return 1024;
        });
        System.out.println("22222222");

        completableFuture.get();//获取返回结果，不会阻塞,但是不处理错误
        System.out.println("33333333");

        //whenComplete中参数是 BiConsumer，有两个返回值，第一个是结果，第二个是错误信息
        //因为BiConsumer是消费型接口，无法返回错误信息，要想返回错误信，需要使用exceptionally
        //链式编程：返回值都一样，可以像链子一样，根据返回值再次调用
       /* System.out.println(completableFuture.whenComplete((t, u) -> {
            System.out.println("t=>" + t); // 正常的返回结果
            System.out.println("u=>" + u); // 错误信息：
            //java.util.concurrent.CompletionException: java.lang.ArithmeticException:by zero
        }).exceptionally((e) -> {
            System.out.println(e.getMessage());
            return 233; // 可以获取到错误的返回结果
        }).get());*/
    }
}
