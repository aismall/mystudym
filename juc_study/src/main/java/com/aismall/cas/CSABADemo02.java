package com.aismall.cas;
import java.util.concurrent.atomic.AtomicStampedReference;

/*注意：版本号只增不减*/
public class CSABADemo02 {
    //使用AtomicStampedReference
    public static AtomicStampedReference<Integer> a=new AtomicStampedReference<>(new Integer(1),1);

    public static void main(String[] args) {
        Thread main=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("操作线程"+Thread.currentThread().getName()+"，初始值："+a.getReference());
                try {
                    //获得期望的引用值
                    Integer exceptReference=a.getReference();
                    Integer newReference=exceptReference+1;
                    //获得期望的版本号
                    Integer expectStamp=a.getStamp();
                    Integer newStamp=a.getStamp()+1;

                    //主线程休眠，让出CPU
                    Thread.sleep(1000);
                    //CAS操作
                    boolean isCasSuccess=a.compareAndSet(exceptReference,newReference,expectStamp,newStamp);
                    System.out.println("操作线程"+Thread.currentThread().getName()+"，CAS操作："+isCasSuccess);
                    System.out.println("操作线程"+Thread.currentThread().getName()+"，CAS操作之后的值="+a.getReference());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"主线程");

        Thread other=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //先睡眠，保证main线程优先执行
                    Thread.sleep(20);
                    //执行加一操作
                    a.compareAndSet(a.getReference(),a.getReference()+1,a.getStamp(),a.getStamp()+1);
                    System.out.println("操作线程"+Thread.currentThread().getName()+"，increment，值="+a.getReference());
                    //执行减一操作
                    a.compareAndSet(a.getReference(),a.getReference()-1,a.getStamp(),a.getStamp()+1);
                    System.out.println("操作线程"+Thread.currentThread().getName()+"，increment，值="+a.getReference());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"干扰线程");

        //启动线程
        main.start();
        other.start();
    }
}
