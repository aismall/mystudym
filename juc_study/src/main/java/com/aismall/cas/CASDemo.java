package com.aismall.cas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * CAS compareAndSet : 比较并交换
 *      CAS ： 比较当前工作内存中的值和主内存中的值，如果这个值是期望的，那么则执行操作，如果不是就一直循环
 *      缺点：
 *          1、 循环会耗时
 *          2、一次性只能保证一个共享变量的原子性
 *          3、ABA问题
 *
 * ABA问题：
 * */

/**
 * 1.UnSafe 是CAS的核心类 由于Java 方法无法直接访问底层 ,需要通过本地(native)方法来访问,
 * UnSafe相当于一个后面,基于该类可以直接操作特额定的内存数据.
 * UnSafe类在于sun.misc包中,其内部方法操作可以向【C】的指针一样直接操作内存,因为Java中CAS操作的助兴依赖于UNSafe类的方法.
 * 注意UnSafe类中所有的方法都是native修饰的,也就是说UnSafe类中的方法都是【直接调用操作底层资源执行响应的任务】 */

/**
 * getAndIncrement()：AtomicInteger类的一个方法，功能是加一操作
 * 点进去看源码：底层是得到一个返回值，这个返回值由unsafe类的getAndAddINT()方法提供
 *      分析一下三个参数：
 *                  this：当前对象地址
 *                  valueOffset：变量在内存中的偏移地址
 *                  1 ：整数1
 *          public final int getAndIncrement() {
 *              return unsafe03.getAndAddInt(this, valueOffset, 1);
 *               }
 *
 * 点进去看一下unsafe类的getAndAddINT()方法：
 *         public final int getAndAddInt(Object var1, long var2, int var4) {
 *  *               int var5;
 *              do {
 *                   var5 = this.getIntVolatile(var1, var2);//通过对象地址和偏移地址获取对象的值并赋值给var5
 *              }
 *              //再次通过对象地址和偏移地址获取对象的值，和var5比较，如果相同（没被修改），就让这个变量执行加一
 *              while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));
 *             return var5;
 *             }
 *
 *
 * */
public class CASDemo {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(2020);
        System.out.println(atomicInteger.get());//获取构造的时候传入的值
        // 期望、更新
        // public final boolean compareAndSet(int expect, int update)
        // 如果我期望的值达到了，那么就更新，否则，就不更新, CAS 是CPU的并发原语
        System.out.println(atomicInteger.compareAndSet(2020, 2021));//true
        System.out.println(atomicInteger.get());//看看是否更改
        atomicInteger.getAndIncrement();//加一操作
        System.out.println(atomicInteger.compareAndSet(2020, 2021));//false
        System.out.println(atomicInteger.get());//看看是否更改
    }
}
