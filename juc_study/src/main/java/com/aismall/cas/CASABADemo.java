package com.aismall.cas;

import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;

import java.util.concurrent.atomic.AtomicInteger;

public class CASABADemo {
    //原子整形
    public static AtomicInteger a=new AtomicInteger(1);

    public static void main(String[] args) {
        Thread main=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("操作线程"+Thread.currentThread().getName()+"，初始值："+a.get());
                try {
                    int exceptNum=a.get();
                    int newNum=exceptNum+1;
                    //主线程休眠，让出CPU
                    Thread.sleep(1000);
                    //CAS操作
                    boolean isCasSuccess=a.compareAndSet(exceptNum,newNum);
                    System.out.println("操作线程"+Thread.currentThread().getName()+"，CAS操作："+isCasSuccess);
                    System.out.println("操作线程"+Thread.currentThread().getName()+"，CAS操作之后的值="+a.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"主线程");

        Thread other=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //先睡眠，保证main线程优先执行
                    Thread.sleep(20);
                    //执行加一操作
                    a.incrementAndGet();
                    System.out.println("操作线程"+Thread.currentThread().getName()+"，increment，值="+a.get());
                    //执行减一操作
                    a.decrementAndGet();
                    System.out.println("操作线程"+Thread.currentThread().getName()+"，increment，值="+a.get());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"干扰线程");

        //启动线程
        main.start();
        other.start();
    }
}
