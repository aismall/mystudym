package com.aismall.cas;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
/*
按道理来说，100个线程，每个线程对这个num修改10次，最后num的值应该是1000，但是结果却不是这样
Q：分析一下问题出在哪里？
A：num++操作实际上有三步来完成，所以numPlus这个方法没办法保证原子性
    1、获取num的值，记为A：A=num
    2、将A的值加一，得到B：B=A+1
    3、将B的值赋值给num

    如果有A和B两个线程，如果A执行到第一步被线程B抢占，线程B也执行了第一步，这时候等这两个线程都执行完
    这三步，num其实就加了一次，少加了一次

Q：怎么解决这个问题？
A：多线程的时候，只能让一个线程修改，一个线程不修改结束，其他线程不能进去修改，使用synchronized关键字
   对资源加锁



*/
/*CAS的引出：通过使用多线程修改一个变量来引出CAS*/
public class CASDemo02 {
    //定义一个变量
    public static int num=0;
    //一个执行加一操作的方法
    public static void numPlus() throws InterruptedException {
        //睡眠时间：每个线程过来调用这个方法就让它睡一会在执行加一操作，睡五毫秒
        TimeUnit.MILLISECONDS.sleep(5);
        num++;
    }

    public static void main(String[] args) throws InterruptedException {
        //记录开始时间
        long startTime=System.currentTimeMillis();
        //我们要创建的线程数
        int threadSize=100;
        //使用栏栅类来控制
        CountDownLatch countDownLatch=new CountDownLatch(threadSize);
        for(int i=0;i<threadSize;i++){
            Thread thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    //业务代码编写处：多线程改写一个变量的值
                    try {
                        //一个线程加十次，为的就是演示没有原子性
                        for(int j=0;j<10;j++){
                            //调用加一操作
                            numPlus();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        countDownLatch.countDown();
                    }
                }
            });
            thread.start();
        }
        //通过栏栅类来控制，线程不创建完成，不能执行后面的代码
        //也就是，threadSize不减为0，不能越过栏栅
        countDownLatch.await();
        //结束时间
        long endTime=System.currentTimeMillis();
        //打印使用的时间，和线程的名字
        System.out.println(Thread.currentThread().getName()+"：耗时"+(endTime-startTime)+"  num="+num);
    }
}
