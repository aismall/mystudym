package com.aismall.cas;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/*
Q：如何解决耗时的问题
A：num++操作实际上有三步来完成，所以numPlus这个方法没办法保证原子性
    1、获取num的值，记为A：A=num
    2、将A的值加一，得到B：B=A+1
    3、将B的值赋值给num
    我们使用另外一种方式实现
    	1、获取锁
    	2、获取count的预期值，记为exp
    	3、判断执行的结果是否为exp，如果是，返回true，不是返回false
    	4、释放锁*/

//问题出在加一操作的原子性，我们给加一操作这个代码进行改写，对加一操作进行加锁
public class CASDemo04 {
    //定义一个变量
    public volatile static int num=0;
    //一个执行加一操作的方法
    public static void numPlus() throws InterruptedException {
        //睡眠时间：每个线程过来调用这个方法就让它睡一会在执行加一操作，睡五毫秒
        TimeUnit.MILLISECONDS.sleep(5);

        int originalNum;//原始值
        //给加一操作这句代码加锁，保证加一操作的原子性，就不会有问题了
        while (!compareAndSwap(originalNum=getNum(),originalNum+1)){
            //不需要循环体
        }
    }

    //添加一个方法：用于比较新值与期望值是否相等
    public synchronized static boolean compareAndSwap(int originalNum,int expectNum){
        //第二次获取num的值
        if(getNum()==originalNum){
            num=expectNum;
            return true;
        }else {
            return false;
        }
    }

    public static int getNum() {
        return num;
    }

    public static void setNum(int num) {
        CASDemo04.num = num;
    }

    public static void main(String[] args) throws InterruptedException {
        //记录开始时间
        long startTime=System.currentTimeMillis();
        //我们要创建的线程数
        int threadSize=100;
        //使用栏栅类来控制
        CountDownLatch countDownLatch=new CountDownLatch(threadSize);
        for(int i=0;i<threadSize;i++){
            Thread thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    //业务代码编写处：多线程改写一个变量的值
                    try {
                        //一个线程加十次，为的就是演示没有原子性
                        for(int j=0;j<10;j++){
                            //调用加一操作
                            numPlus();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        countDownLatch.countDown();
                    }
                }
            });
            thread.start();
        }
        //通过栏栅类来控制，线程不创建完成，不能执行后面的代码
        //也就是，threadSize不减为0，不能越过栏栅
        countDownLatch.await();
        //结束时间
        long endTime=System.currentTimeMillis();
        //打印使用的时间，和线程的名字
        System.out.println(Thread.currentThread().getName()+"：耗时"+(endTime-startTime)+"  num="+num);
    }
}
