package com.aismall.cas;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
/*使用synchronized关键字对资源加锁*/
public class CASDemo03 {
    //定义一个变量
    public static int num=0;
    //一个执行加一操作的方法
    public synchronized static void numPlus() throws InterruptedException {
        //睡眠时间：每个线程过来调用这个方法就让它睡一会在执行加一操作，睡五毫秒
        TimeUnit.MILLISECONDS.sleep(5);
        num++;
    }

    public static void main(String[] args) throws InterruptedException {
        //记录开始时间
        long startTime=System.currentTimeMillis();
        //我们要创建的线程数
        int threadSize=100;
        //使用栏栅类来控制
        CountDownLatch countDownLatch=new CountDownLatch(threadSize);
        for(int i=0;i<threadSize;i++){
            Thread thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    //业务代码编写处：多线程改写一个变量的值
                    try {
                        //一个线程加十次，为的就是演示没有原子性
                        for(int j=0;j<10;j++){
                            //调用加一操作
                            numPlus();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        countDownLatch.countDown();
                    }
                }
            });
            thread.start();
        }
        //通过栏栅类来控制，线程不创建完成，不能执行后面的代码
        //也就是，threadSize不减为0，不能越过栏栅
        countDownLatch.await();
        //结束时间
        long endTime=System.currentTimeMillis();
        //打印使用的时间，和线程的名字
        System.out.println(Thread.currentThread().getName()+"：耗时"+(endTime-startTime)+"  num="+num);
    }
}
