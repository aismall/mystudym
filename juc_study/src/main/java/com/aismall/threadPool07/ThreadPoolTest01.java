package com.aismall.threadPool07;
import java.util.concurrent.*;

/*
* 池化技术：程序的运行本质，占用系统的资源， 优化资源的使用=>池化技术
  线程池、连接池、内存池、对象池。。。 创建、销毁，十分浪费资源
  池化技术：事先准备好一些资源，有人要用，就来我这里拿，用完之后还给我。

 线程池的好处:
       1、降低资源的消耗
       2、提高响应的速度
       3、方便管理。
线程复用、可以控制最大并发数、管理线程
*/

/**
 * 线程池不允许使用Executors去创建，而是通过ThreadPoolExecutor的方式去创建
 * 这样写让其他人更加明确线程的运行规则，规避资源耗尽的风险
 *
 * Executors【返回线程池】的弊端如下：
 *    FixedThreadPool和SingleThreadPool允许的请求对列长度为Integer.MAX_VALUE，可能会堆积大量的请求造成OOM
 *    CachedThreadPool和cheduledThreadPool允许创建的线程数量为Integer.MAX_VALUE，可能会创建大量的线程造成OOM
 * */

//线程池：三大方法、7大参数、4 种拒绝策略

/**
 * Executors：工具类：三大方法（创建线程池的三大方法，本质也是使用ThreadPoolExecutor来创建的，但是【不建议使用】）
 *       Executors.newSingleThreadExecutor();//单个线程
 *       Executors.newFixedThreadPool(5);//创建一个固定的线程池大小
 *       Executors.newCachedThreadPool();//可伸缩的*/

/**
 * ThreadPoolExecutor（【使用这个来创建线程池】）：创建线程池的类：里面有七个参数
 *
 *      public ThreadPoolExecutor(int corePoolSize,//核心线程池大小
 *                               int maximumPoolSize,//最大核心线程池大小
 *                               long keepAliveTime,//没人调用时的存活时间，过期自动释放
 *                               TimeUnit unit,//超时单位
 *                               BlockingQueue<Runnable> workQueue,//阻塞对列
 *                               ThreadFactory threadFactory, //创建工厂，用来创建线程，一般不动
 *                               RejectedExecutionHandler defaultHandler)//【拒绝策略】
 *                               {
 *         this(【corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,threadFactory, defaultHandler】);
 *     }
 *
 *  4 种拒绝策略：RejectedExecutionHandler接口的【四个实现类，ThreadPoolExecutor类中的子类】（使用默认的即可）
 *  默认拒绝策略：AbortPolicy()
 * */

/**
 *  4 种拒绝策略:
 * new ThreadPoolExecutor.AbortPolicy()       // 银行满了，还有人进来，不处理这个人的，抛出异常
 * new ThreadPoolExecutor.CallerRunsPolicy()  // 哪来的去哪里！
 * new ThreadPoolExecutor.DiscardPolicy()     // 队列满了，丢掉任务，不会抛出异常
 * * new ThreadPoolExecutor.DiscardOldestPolicy() //队列满了，尝试去和最早的竞争，也不会抛出异常！
 * */
public class ThreadPoolTest01 {
    public static void main(String[] args) {
        ExecutorService threadPool01=Executors.newSingleThreadExecutor();//单个线程
        ExecutorService threadPool02=Executors.newFixedThreadPool(5);//创建一个固定的线程池大小
        ExecutorService threadPool03=Executors.newScheduledThreadPool(2);
        ExecutorService threadPool04=Executors.newCachedThreadPool();//可伸缩的
        ExecutorService threadPool=new ThreadPoolExecutor(2,
                5,
                3,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());

        try {
            //最大承载：Deque+max-->3+5,超出会报异常
            //触发max的条件：corePoolSize+Deque>max
            for(int i=0;i<10;i++){
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"   OK!!!");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();//线程池使用完需要关闭
        }
    }
}
