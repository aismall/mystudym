package com.aismall.threadPool07;

import java.util.concurrent.*;

/**
 * 自定义线程池使用：——> ThreadPoolExecutor
 *
 * 最大线程到底该如何定义?
 * 1、CPU 密集型(可以【并行】执行多少条线程)：几核，就是几，可以保持CPU的效率最高
 * 2、IO 密集型：判断你程序中十分耗IO的线程有多少个，大于这个数就行
*/
public class Demo02 {
    public static void main(String[] args) {
        // 获取CPU的核数
        System.out.println(Runtime.getRuntime().availableProcessors());
        //创建线程池
        ExecutorService threadPool=new ThreadPoolExecutor(2,
                3,
                3,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());//AbortPolicy是一个内部类

        try {
            for(int i=0;i<6;i++){
               threadPool.execute(()->{
                   System.out.println(Thread.currentThread().getName()+" OK!!!");
               });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }
}
