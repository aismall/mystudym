package com.aismall.readWriteLock05;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/*ReadWriteLock是一个接口：java.util.concurrent.locks.ReadWriteLick
* ReadWriteLock接口只有一个实现类：ReentrantReadWriteLock（可重入读写锁）
*
* 读写锁的【读锁】可以有多个线程同时读，【写锁】写的时候只能有一个线程来写，类似共享锁和独占锁
 *    读-读 可以共存！
 *    读-写 不能共存！
 *    写-写 不能共存！
* */
public class ReadWriteLockTest01 {
    public static void main(String[] args) {
       // MyCache myCache=new MyCache();
        MyCacheLock myCache=new MyCacheLock();
        //五个线程写
        /*问题一：如果那个线程写，必须执行完写操作，其他线程才可以进来

        * */
        for(int i=1;i<5;i++) {
            final int temp=i;
            new Thread(()->{
                myCache.put(temp+"",temp+"");
            },String.valueOf(i)).start();
        }

        //五个线程读
        for(int i=1;i<5;i++) {
            final int temp=i;
            new Thread(()->{
                myCache.get(temp+"");
            },String.valueOf(i)).start();
        }
    }
}
/*自定义缓存：不带锁*/
class MyCache{
    private volatile Map<String,Object> map=new HashMap<>();
    public void put(String key,Object value){
        //存（写）
        System.out.println(Thread.currentThread().getName()+"写"+key);
        map.put(key,value);
        System.out.println(Thread.currentThread().getName()+"写结束");
    }
    public void get(String key){
        //取（读）
        System.out.println(Thread.currentThread().getName()+"读"+key);
        map.get(key);
        System.out.println(Thread.currentThread().getName()+"读结束");
    }
}


/*自定义缓存：带锁*/
class MyCacheLock{
    private volatile Map<String,Object> map=new HashMap<>();
    private ReadWriteLock readWriteLock=new ReentrantReadWriteLock();
    public void put(String key,Object value){
        readWriteLock.writeLock().lock();
        //存（写）
        try {
            System.out.println(Thread.currentThread().getName()+"写"+key);
            map.put(key,value);
            System.out.println(Thread.currentThread().getName()+"写结束");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }
    public void get(String key){
        readWriteLock.readLock().lock();
        //取（读）
        try {
            System.out.println(Thread.currentThread().getName()+"读"+key);
            map.get(key);
            System.out.println(Thread.currentThread().getName()+"读结束");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.readLock().unlock();
        }
    }
}