package com.aismall.auxiliaryToolClass;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/*CyclicBarrier:栅栏类，类似于屏障
* */
public class CyclicBarrierTest {
    public static int num=0;
    public static void main(String[] args) {
        /*如果达不到给定的线程数量，将会等待，如果达到给定的线程数量将会跳越屏障
        * 跳跃屏障后可以设定要进行的操作，也可以不设定，这个由重载的构造器决定
        * 注意：如果线程数达不到要求，就会一直等待，陷入阻塞 */
        CyclicBarrier cyclicBarrier=new CyclicBarrier(7,()->{
            System.out.println("七个线程创建完毕。。。。");
        });
        for(int i=1;i<8;i++){
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"条线程。。。");
                //需要调用await()进行等待
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }finally {
                    System.out.println(++num);
                }
            },String.valueOf(i)).start();
        }
    }
}
