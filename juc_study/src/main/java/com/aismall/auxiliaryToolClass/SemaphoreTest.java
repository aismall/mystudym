package com.aismall.auxiliaryToolClass;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/*Semaphore：信号量
* 用来限制流量，每次只能有这么多线程，这些线程不释放，其他线程就进不来*/
public class SemaphoreTest {
    public static void main(String[] args) {
        //线程数量
        Semaphore semaphore=new Semaphore(3);

        for(int i=0;i<6;i++){
            new Thread(()->{
                //acquire()，【得到】
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"线程抢到执行权。。。");
                    TimeUnit.SECONDS.sleep(4);//睡四秒
                    System.out.println(Thread.currentThread().getName()+"线程释放执行权。。。");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    //release()，【释放】
                    semaphore.release();
                }
            },String.valueOf(i)).start();
        }
    }
}
