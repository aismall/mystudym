package com.aismall.auxiliaryToolClass;

import javax.xml.transform.Source;
import java.util.concurrent.CountDownLatch;

/*CountDownLatch：计数器（下计数器）:允许一个或多个线程等待直到在其他线程中执行的一组操作完成的同步辅助

在具体实现的时候，线程执行的速度不同，有的快有的慢，部分业务需要等待其他业务执行完毕才能结束

本次演示：创建六个线程完毕之后，再结束等待
* */
public class CountDownLatchTest01 {
    public static void main(String[] args) throws Exception {
        CountDownLatch count=new CountDownLatch(6);//减法计数器，减完执行操作
        for(int i=0;i<6;i++){
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+":OK!!!");
                count.countDown();//执行减一操作
            },String.valueOf(i)).start();
        }
        count.await();//等待

        System.out.println("End");
    }
}
