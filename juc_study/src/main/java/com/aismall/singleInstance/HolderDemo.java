package com.aismall.singleInstance;

/**
 * */
//静态内部类
public class HolderDemo {
    //构造方法
    private HolderDemo(){
    }

    public static class InnerClass{
        private static final HolderDemo HOLDERDEMO=new HolderDemo();
    }

    public static HolderDemo getInstance(){
        return InnerClass.HOLDERDEMO;
    }

    //反射
    public static void main(String[] args) {

    }

}
