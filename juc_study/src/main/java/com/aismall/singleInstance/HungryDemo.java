package com.aismall.singleInstance;
/**
 * 饿汉式特点：
 *      1.饿汉式在类加载的时候就实例化，并且创建单例对象
 *      2.饿汉式线程安全 (在线程还没出现之前就已经实例化了，因此饿汉式线程一定是安全的)
 *      3.饿汉式没有加任何的锁，因此执行效率比较高
 *
 *      4.饿汉式在类加载的时候就初始化，不管你是否使用，它都实例化了，所以会占据空间，浪费内存
 * */

//饿汉式单例
public class HungryDemo {
    //无参构造
    private HungryDemo(){};
    /*
    private 为了对外无法访问
    * static 为了在类加载的时候加载
    * final  为了只创建一次*/
    private static final HungryDemo HUNGRY_DEMO = new HungryDemo();

    //通过这个静态方法获取实例
    public static HungryDemo getInstance(){
        return HUNGRY_DEMO;
    }
}
