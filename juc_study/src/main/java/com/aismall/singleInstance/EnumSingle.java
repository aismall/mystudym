package com.aismall.singleInstance;

import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public enum EnumSingle {
    INSTANCE;
    public EnumSingle getInstance(){
        return INSTANCE;
    }

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //通过方法进行创建
        EnumSingle instance1=EnumSingle.INSTANCE;
        EnumSingle instance2=EnumSingle.INSTANCE;
        System.out.println(instance1);
        System.out.println(instance2);

        //通过反射进行创建：获取空参构造器
        //获取空参构造的时候报错：java.lang.NoSuchMethodException: com.aismall.singleInstance.EnumSingle.<init>()
        //Constructor<EnumSingle> declaredConstructor=EnumSingle.class.getDeclaredConstructor(null);
        Constructor<EnumSingle> declaredConstructor=EnumSingle.class.getDeclaredConstructor(String.class,int.class);
        //能获取该类中的所有字段
        declaredConstructor.setAccessible(true);
        EnumSingle enumSingle1=declaredConstructor.newInstance();
        EnumSingle enumSingle2=declaredConstructor.newInstance();
        System.out.println(enumSingle1);
        System.out.println(enumSingle2);
    }
}
