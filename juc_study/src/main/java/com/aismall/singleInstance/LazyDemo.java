package com.aismall.singleInstance;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/**
 * 懒汉式特点：
 *      1.懒汉式默认不会实例化，外部什么时候调用什么时候new
 *      2.懒汉式【线程不安全】( 因为懒汉式加载是在使用时才会去new 实例的，那么你去new的时候是一个动态的过程，
 *      是放到方法中实现的，如果这个时候有多个线程访问这个实例，这个时候实例还不存在，还在new，就会进入到方法中，
 *      有多少线程就会new出多少个实例。一个方法只能return一个实例，那最终return出哪个呢？是不是会覆盖很多new的实例？
 *      【这种情况当然也可以解决】，那就是【加同步锁，避免这种情况发生】）
 *      3.懒汉式一般使用都会加同步锁，效率比饿汉式差
 *
 *      4.懒汉式什么时候需要什么时候实例化，相对来说不浪费内存
 * */

//懒汉式单例
public class LazyDemo {
    //使用一个标志位，来防止暴力反射
    private static boolean flag=true;
    //构造方法
    private LazyDemo(){
        //高级版本的防止暴力反射
        synchronized (LazyDemo.class) {
            //我们不判断lazyDemo，判断flag
            //标志位为true
            if (flag == true) {
                //设为false
                flag = false;
            } else {
                throw new RuntimeException("不要试图使用反射来破坏抛出异常");
            }
        }
    };

    //第二重加锁：默认不会被实例化，保证原子性，避免指令重排，加上volatile
    private volatile static LazyDemo lazyDemo=null;

    //什么时候调用什么时候实例化，这种方式比直接在静态方法上加锁效率高一点
    public static LazyDemo getInstance_safe(){
        //第一重锁：锁住这个实例
        if (lazyDemo==null){
            synchronized (LazyDemo.class) {
                if(lazyDemo==null) {
                    lazyDemo = new LazyDemo();
                }
            }
        }
        return lazyDemo;
    }

    public static void main(String[] args) throws Exception {
        //【假设】我们知道这关键字是谁（现实基本不可能）
        //获取这个LazyDemo的flag字段
        Field flag = LazyDemo.class.getDeclaredField("flag");
        // 在反射的时候能够访问这个私有变量：flag
        flag.setAccessible(true);

        //使用暴力反射创建一个实例：破坏单例模式
        Constructor<LazyDemo> declaredConstructor=LazyDemo.class.getDeclaredConstructor(null);

        //创建一个实例
        LazyDemo instance1=declaredConstructor.newInstance();

        // 我们在手动把这个值改为：flag=true
        flag.set(instance1,true);//这句代码注意位置
        //在创建一个实例
        LazyDemo instance2=declaredConstructor.newInstance();

        System.out.println(instance1);
        System.out.println(instance2);
    }
}
