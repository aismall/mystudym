package com.aismall.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class HelloWorld {
    @RequestMapping("/hello")
    public String hello(){
        return "Hello Boot...";
    }
    /*
    * main方法：
    *   这是遵循应用程序入口点的Java约定的标准方法。
    *   我们的main方法通过调用run委托给Spring Boot的SpringApplication类。
    *   SpringApplication引导我们的应用程序，启动Spring，然后Spring启动自动配置的Tomcat web服务器。
    *   我们需要将HelloWorld.class作为参数传递给run方法，以告知SpringApplication哪个是主要的Spring组件。
    *   还会传递args数组以公开任何命令行参数。
    * */
    public static void main(String[] args) {
        SpringApplication.run(HelloWorld.class,args);
    }
}

