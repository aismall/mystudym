package com.aismall.admin.service.impl;

import com.aismall.admin.bean.Account;
import com.aismall.admin.mapper.AccountMapper;
import com.aismall.admin.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    public Account getAcctByid(Long id){
        return accountMapper.getAcct(id);
    }
}
