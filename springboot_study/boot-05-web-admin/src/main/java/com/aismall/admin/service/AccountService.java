package com.aismall.admin.service;

import com.aismall.admin.bean.Account;

public interface AccountService {

    Account getAcctByid(Long id);
}
