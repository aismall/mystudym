package com.aismall.admin.config;


import com.aismall.admin.interceptor.LoginInterceptor;
import com.aismall.admin.interceptor.RedisUrlCountInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AdminWebConfig implements WebMvcConfigurer{


    /**
     * Filter、Interceptor 几乎拥有相同的功能？
     * 1、Filter是Servlet定义的原生组件。好处，脱离Spring应用也能使用
     * 2、Interceptor是Spring定义的接口。可以使用Spring的自动装配等功能
     */
    //@Autowired
    RedisUrlCountInterceptor redisUrlCountInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")  //所有请求都被拦截包括静态资源
                .excludePathPatterns("/","/login","/css/**","/fonts/**","/images/**",
                        "/js/**","/sql"); //放行的请求

        //registry.addInterceptor(redisUrlCountInterceptor)
        //        .addPathPatterns("/**")
        //        .excludePathPatterns("/","/login","/css/**","/fonts/**","/images/**",
        //                "/js/**","/aa/**");
    }

    //@Bean
    //public WebMvcRegistrations webMvcRegistrations(){
    //    return new WebMvcRegistrations(){
    //        @Override
    //        public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
    //            return null;
    //        }
    //    };
    //}
//

}
