package com.aismall.admin.service.impl;

import com.aismall.admin.bean.User;
import com.aismall.admin.mapper.UserMapper;
import com.aismall.admin.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {


}
