package com.aismall.admin.mapper;

import com.aismall.admin.bean.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 *
 */
public interface UserMapper extends BaseMapper<User> {


}
