package com.aismall.admin.service;

import com.aismall.admin.bean.City;


public interface CityService {

     City getById(Long id);

     void saveCity(City city);

}
