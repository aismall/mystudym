package com.aismall.boot.controller;


import com.aismall.boot.bean.Person;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据绑定：页面提交的请求数据（GET、POST）都可以和对象属性进行绑定
 * RequestAttribute获取数据，和页面跳转
 */

@RestController
public class ParameterTestController {
    //测试POJO封装
    @PostMapping("/saveuser")
    public Person saveuser(Person person){
        return person;
    }

    //方法一：单个获取
    @GetMapping(value = "/carSingle/{id}/owner/{username}",produces = "application/json; charset=utf-8")
    public Map<String,Object> getCarSingle(@PathVariable("id") Integer id,
                                     @PathVariable("username") String name,
                                     @RequestHeader("User-Agent") String userAgent,
                                     @RequestParam("age") Integer age,
                                     @RequestParam("inters") List<String> inters){
        //创建一个Map存放获取到的值
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        map.put("userAgent",userAgent);
        map.put("age",age);
        map.put("inters",inters);
        return map;
    }
    //方法二：使用map获取
    @GetMapping(value = "/carMap/{id}/owner/{username}",produces = "application/json;charset=utf-8")
    public Map<String,Object> getCarMap(@PathVariable Map<String,String> pv,
                                     @RequestHeader Map<String,String> header,
                                     @RequestParam Map<String,String> params,
                                     @CookieValue("NMTID") String _ga,
                                     @CookieValue("NMTID") Cookie cookie){
        //创建一个Map存放获取到的值
        Map<String,Object> map = new HashMap<>();
        map.put("pv",pv);
        map.put("headers",header);
        map.put("params",params);
        map.put("_ga",_ga);
        System.out.println(cookie.getName()+"===>"+cookie.getValue());
        return map;
    }

    //测试获取请求体
    @PostMapping("/save")
    public Map postMethod(@RequestBody String content){
        Map<String,Object> map = new HashMap<>();
        map.put("content",content);
        return map;
    }


    //测试矩阵变量的功能
    //1、语法： 请求路径：/cars/sell;low=34;brand=byd,audi,yd
    //2、SpringBoot默认是禁用了矩阵变量的功能
    //      手动开启：原理。对于路径的处理。UrlPathHelper进行解析。
    //              removeSemicolonContent（移除分号内容）支持矩阵变量的
    //3、矩阵变量必须有url路径变量才能被解析
    @GetMapping(value = "/cars/{path}",produces = "application/json;charset=utf-8")
    public Map carsSell(@MatrixVariable("low") Integer low,
                        @MatrixVariable("brand") List<String> brand,
                        @PathVariable("path") String path){
        Map<String,Object> map = new HashMap<>();
        map.put("low",low);
        map.put("brand",brand);
        map.put("path",path);
        return map;
    }

    // /boss/sell;age=20/2;age=10
    @GetMapping(value = "/boss/{bossId}/{empId}",produces = "application/json;charset=utf-8")
    public Map boss(@MatrixVariable(value = "age",pathVar = "bossId") Integer bossAge,
                    @PathVariable("bossId") String pathBoosId,
                    @MatrixVariable(value = "age",pathVar = "empId") Integer empAge,
                    @PathVariable("empId") String pathEmpId){
        Map<String,Object> map = new HashMap<>();

        map.put("bossAge",bossAge);
        map.put("pathBoosId",pathBoosId);
        map.put("empAge",empAge);
        map.put("pathEmpId",pathEmpId);
        return map;
    }


}
