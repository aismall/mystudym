package com.aismall.boot.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
/**
 * 测试@RequestAttribute获取数据，和页面跳转
 * */
@Controller
public class RequestController {
    //页面跳转
    @GetMapping("/goto")
    public String goToPage(HttpServletRequest request){
        //设置属性：在向success页面跳转的时候获取
        request.setAttribute("msg","成功了...");
        request.setAttribute("code",200);
        //转发到：success请求
        return "forward:/success";
    }

    //success
    @ResponseBody
    @GetMapping(value = "/success",produces = "application/json; charset=utf-8")
    public Map success(@RequestAttribute(value = "msg",required = false) String msg,
                         @RequestAttribute(value = "code",required = false)Integer code,
                       HttpServletRequest request){
        //使用method获取属性值
        Object msg1 = request.getAttribute("msg");

        //创建一个map
        Map<String,Object> map = new HashMap<>();

        map.put("reqMethod_msg",msg1);
        map.put("annotation_msg",msg);
        return map;
    }

    //验证复杂参数：
    @GetMapping("/params")
    public String testParam(Map<String,Object> map,
                            Model model,
                            HttpServletRequest request,
                            HttpServletResponse response){
        map.put("hello","hello666");
        model.addAttribute("world","world666");
        request.setAttribute("message","message666");

        Cookie cookie = new Cookie("c1","v1");
        response.addCookie(cookie);
        return "forward:/returnMSG";
    }

    @ResponseBody
    @GetMapping("/returnMSG")
    public Map returnMSG(HttpServletRequest request){
        //创建一个map
        Map<String,Object> map = new HashMap<>();
        //获取下面这个属性的值由于没有设置，所以结果为null
        Object msg1 = request.getAttribute("msg");

        Object hello = request.getAttribute("hello");
        Object world = request.getAttribute("world");
        Object message=request.getAttribute("message");

        map.put("reqMethod_msg",msg1);
        map.put("hello",hello);
        map.put("world",world);
        map.put("message",message);
        return map;
    }
}
