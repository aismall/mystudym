package com.aismall.boot.config;


import com.aismall.hello.service.HelloService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//通过自动配置包中获取的这个类添加到容器中
@Configuration
public class MyConfig {
    @Bean
    public HelloService helloService(){
        HelloService helloService = new HelloService();
        return helloService;
    }
}
