package com.aismall.boot;

import com.aismall.boot.bean.Car;
import com.aismall.boot.bean.Cat;
import com.aismall.boot.bean.Person;
import com.aismall.boot.bean.User;
import com.aismall.boot.config.MainConfig01;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.interceptor.CacheAspectSupport;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
//@SpringBootApplication= @SpringBootConfiguration+@EnableAutoConfiguration+@ComponentScan("com.aismall.boot")
public class Boot01Helloworld2Application {
    public static void main(String[] args) {
        //1.会返回我们的IOC容器
        ConfigurableApplicationContext run=SpringApplication.run(Boot01Helloworld2Application.class, args);

        //2.查看容器中的组件
        String[] names=run.getBeanDefinitionNames();
        /*for (String name:names) {
            System.out.println(name);
        }*/

        //3.从容器中获取组件
        Cat cat01=run.getBean("tomcat",Cat.class);
        Cat cat02=run.getBean("tomcat",Cat.class);
        System.out.println("Cat组件："+(cat01==cat02));

        //4.从容器中获取我们自己编写的配置类：配置类也是组件
        MainConfig01 config01=run.getBean(MainConfig01.class);
        System.out.println(config01);

        //@Configuration(proxyBeanMethods = true)代理对象调用方法，SpringBoot总会检查这个组件是否在容器中
        //保持组件的单实例
        User user01=config01.user();
        User user02=config01.user();
        System.out.println("User组件："+(user01==user02));

        Car car=run.getBean(Car.class);
        System.out.println(car.toString());

        //判断容器中是否有某个类
        String[] beanTypes=run.getBeanNamesForType(CacheAspectSupport.class);
        System.out.println("========"+beanTypes.length+"=============");

        Person person=run.getBean(Person.class);
        System.out.println(person.toString());
    }

}
