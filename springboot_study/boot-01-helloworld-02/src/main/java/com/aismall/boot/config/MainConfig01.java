package com.aismall.boot.config;

import com.aismall.boot.bean.Car;
import com.aismall.boot.bean.Cat;
import com.aismall.boot.bean.User;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/*
 *1、配置类里面使用@Bean标注在方法上给容器注册组件，默认也是单实例的
 *
 *2、配置类本身也是组件
 *
 *3、默认@Configuration中的proxyBeanMethods属性为true：
 *    proxyBeanMethods：代理bean的方法
 *      Full(proxyBeanMethods = true)、保证每个@Bean方法被调用多少次返回的组件都是单实例的
 *      Lite(proxyBeanMethods = false)、每个@Bean方法被调用多少次返回的组件都是新创建的
 *      组件依赖必须使用Full模式默认。其他默认是否Lite模式
 *
 *4、@Import({User.class,Cat.class})
 *      给容器中自动创建出这两个类型的组件、默认组件的名字就是全类名
 *
 *5、@ImportResource("classpath:beans.xml")导入Spring的配置文件，
 *
 *6、按条件添加的注解，当容器中有这个Bean的时候才进行注入
 *  @ConditionalOnBean(value = {Cat.class})
 *
 * */

/*
 * @EnableConfigurationProperties(Car.class)
 * 1、开启Car配置绑定功能
 * 2、把这个Car这个组件自动注册到容器中*/

//@EnableConfigurationProperties(Car.class)
@Configuration //表明这是一个配置类
public class MainConfig01 {
    @Bean //给容器中添加组件。以方法名作为组件的id。返回类型就是组件类型。返回的值，就是组件在容器中的实例
    public User user(){
        return new User("AISMALL",18);
    }

    @ConditionalOnBean(value = {User.class})
    @Bean(value = "tomcat")
    public Cat cat(){
        return new Cat("tomcat");
    }
}