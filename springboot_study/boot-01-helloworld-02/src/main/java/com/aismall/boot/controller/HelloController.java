package com.aismall.boot.controller;


import com.aismall.boot.bean.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//RestController=@Controller+@ResponseBody
public class HelloController {
    @RequestMapping("/hello")
    public String hello(){
        return "Hello SpringBoot2!"+"   你好 SpringBoot2！";
    }

    @Autowired
    Person person;
    @RequestMapping("/person")
    public Person person(){

        String userName = person.getUserName();
        System.out.println(userName);
        return person;
    }
}
