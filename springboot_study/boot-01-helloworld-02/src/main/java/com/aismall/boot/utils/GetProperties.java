package com.aismall.boot.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

/*获取配置类中的内容*/
public class GetProperties {
    public static void main(String[] args) throws FileNotFoundException,IOException{
        Properties pps = new Properties();
        String path="E:\\File\\Idea_File\\springboot2\\boot-01-helloworld-02\\src\\main\\resources\\application.properties";
        pps.load(new FileInputStream(path));
        Enumeration enum1 = pps.propertyNames();//得到配置文件的名字
        while(enum1.hasMoreElements()) {
            String strKey = (String) enum1.nextElement();
            String strValue = pps.getProperty(strKey);
            System.out.println(strKey + "=" + strValue);
            //封装到JavaBean。
        }
    }
}
