package com.aismall.boot.bean;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
//在配置文件中激活prod环境这个类才会生效
@Profile("prod")
@Component
@ConfigurationProperties("person")
@Data
public class PersonProd implements Person {

    private String name;
    private Integer age;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Integer getAge() {
        return this.age;
    }
}
