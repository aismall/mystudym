package com.aismall.boot.bean;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

//在配置文件中激活test环境这个类才会生效
@Profile("test")
@Component
@ConfigurationProperties("person")
@Data
public class PersonTest implements Person {

    private String name;
    private Integer age;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getAge() {
        return age;
    }
}
