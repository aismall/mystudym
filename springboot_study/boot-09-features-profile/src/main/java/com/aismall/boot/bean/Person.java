package com.aismall.boot.bean;

public interface Person {
   String getName();
   Integer getAge();
}
