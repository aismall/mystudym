package com.aismall.admin;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import redis.clients.jedis.Jedis;






/*@RunWith*/

/**
 * @BootstrapWith(SpringBootTestContextBootstrapper.class)
 * @ExtendWith(SpringExtension.class)
 */
//@SpringBootTest
@DisplayName("junit5功能测试类")
public class Junit5Test {

    @Autowired
    StringRedisTemplate redisTemplate;


    @Autowired
    RedisConnectionFactory redisConnectionFactory;
    @Test
    public void testRedis(){
        //使用Jedis连接redis
        Jedis jedis = new Jedis("http://localhost:6379");
        //打印一行log
        System.out.println("连接成功");
        //连通测试
        System.out.println("服务正在运行: "+jedis.ping());
        //刷新
        jedis.flushDB();
        //给数据库中添加数据
        jedis.set("aismall","我是女神");
        //打印数据库的容量
        System.out.println(jedis.dbSize());
        //输出添加的数据
        System.out.println(jedis.get("aismall"));
    }

    @Test
    void testRedis01(){
        ValueOperations<String, String> operations = redisTemplate.opsForValue();
        operations.set("hello","world");
        String hello = operations.get("hello");
        System.out.println(hello);

        System.out.println(redisConnectionFactory.getClass());
    }
}
