# springboot2

环境要求：

- Java8及以上

```java
java -version:java version "1.8.0_261"
```

- Maven 3.3及以上

```java
mvn -V:Apache Maven 3.3.9
```

# 第一季：SpringBoot2核心技术

## 1、SpringBoot2基础入门

### 1.1、Spring能做什么？

Spring：

- 宏观指得是Spring全家桶，Spring生态去哪，具体可以看Spring官网

- 微观指的是Spring  Framework框架

### 1.2、什么是SpringBoot？

- 凌驾于Spring其他框架上的一个更高级的框架，可以组合spring全家桶里面的各种框架，相当于一个Boot(启动)。
- 简单来说：就是可以组装各种Spring框架：Spring Framework  Spring Security，Spring Data等
- 由于Spring5对Spring做了重大升级，引入了响应式编程，所以SpringBoot2和SpringBoot1也有诸多不同(知道就行了。。。)。

![image-20210510164429096](springboot02.assets/image-20210510164429096.png)

#### 1.2.1、SpringBoot优点

- Create stand-alone Spring applications

- - 创建独立Spring应用

- Embed Tomcat, Jetty or Undertow directly (no need to deploy WAR files)

- - 内嵌web服务器

- Provide opinionated 'starter' dependencies to simplify your build configuration

- - 自动starter依赖，简化构建配置

- Automatically configure Spring and 3rd party libraries whenever possible

- - 自动配置Spring以及第三方功能

- Provide production-ready features such as metrics, health checks, and externalized configuration

- - 提供生产级别的监控、健康检查及外部化配置

- Absolutely no code generation and no requirement for XML configuration

- - 无代码生成、无需编写XML

#### 1.2.2、SpringBoot缺点

- 人称版本帝，迭代快，需要时刻关注变化
- 封装太深，内部原理复杂，不容易精通

#### 1.2.3、微服务

- 微服务是一种架构风格

- 一个应用拆分为一组小型服务

- 每个服务运行在自己的进程内，也就是可独立部署和升级

- 服务之间使用轻量级HTTP交互

- 服务围绕业务功能拆分

- 可以由全自动部署机制独立部署

- 去中心化，服务自治。服务可以使用不同的语言、不同的存储技术

#### 1.2.4、分布式的困难

- 远程调用
- 服务发现
- 负载均衡
- 服务容错
- 配置管理
- 服务监控
- 链路追踪
- 日志管理
- 任务调度
- ......

#### 1.2.5、分布式的解决

- SpringBoot + SpringCloud

#### 1.2.6、如何学习SpringBoot？

- 学习的最好方式就是会查看官方文档：https://spring.io/projects/spring-boot#learn
- 每个版本更新的日志在这里查看([the project release notes section](https://github.com/spring-projects/spring-boot/wiki#release-notes) )：https://spring.io/projects/spring-boot#overview

### 1.3、快速体验SpringBoot

- 可根据官方文档的这个章节：2.4. Developing Your First Spring Boot Application，手动创建一个springboot项目(从创建一个Maven管理的项目，一步一步的改造成SpringBoot项目)
- 注意：

```
在终端使用这个命令可以查看导入包的依赖关系：mvn dependency:tree

在这个章节：Common Application properties，可以查看application.properties file的配置方法，例如，如何更改端口
```

- 在pom文件中添加插件，然后说使用命令就可以吧我们的SpringBoot项目打成jar包

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-maven-plugin</artifactId>
         </plugin>
    </plugins>
</build>
```

- 我们做测试，直接运行main方法即可。

### 1.4、自动配置原理入门(难点)

#### 1.4.1、SpringBoot特点

##### 1.4.1.1、依赖管理

- 父项目做依赖版本管理，引入父项目之后，后面引入的依赖就无需要指定版本号，其实真正做版本管理的是父POM文件引入的父POM文件

```xml
<!--我们引入的父POM-->
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.3.4.RELEASE</version>
</parent>

<!--引入父POM文件中引入的父POM文件-->
<parent>
   <groupId>org.springframework.boot</groupId>
    <!--几乎声明了所有开发中常用的依赖的版本号,自动版本仲裁机制-->
   <artifactId>spring-boot-dependencies</artifactId>
   <version>2.3.4.RELEASE</version>
</parent>
```

- 开发导入starter场景启动器

```xml
1、见到很多 spring-boot-starter-* ： *就某种场景
2、只要引入starter，这个场景的所有常规需要的依赖我们都自动引入
3、SpringBoot所有支持的场景可以在参考文档中的这个章节：3.1.5. Starters查看到
4、见到的  *-spring-boot-starter： 第三方为我们提供的简化开发的场景启动器。

5、所有场景启动器最底层的依赖，每个场景启动器都会包含这个org.springframework.boot依赖:
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter</artifactId>
  <version>2.3.4.RELEASE</version>
  <scope>compile</scope>
</dependency>
```

- 无需关注版本号，自动版本仲裁：遵循就近原则

```
1、引入依赖默认都可以不写版本，在spring-boot-dependencies中已经指点好了默认的版本

2、引入非版本仲裁的jar，我们要写版本号，即覆盖spring-boot-dependencies中的版本号
```

- 我们可以修改默认版本号：遵循就近原则

```xml
1、查看spring-boot-dependencies里面规定当前依赖的版本 用的 key。
2、在当前项目里面重写配置,
<properties>
	<mysql.version>5.1.43</mysql.version>
</properties>
```

##### 1.4.1.2、自动配置

关于导入web启动器的自动配置：

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

- 自动配好Tomcat：
  - 引入Tomcat依赖
  - 配置Tomcat

- 自动配好SpringMVC：
  - 引入SpringMVC全套组件
  - 自动配好SpringMVC常用组件（功能）

- 自动配好Web常见功能，如：字符编码问题
  - SpringBoot帮我们配置好了所有web开发的常见场景

- 默认的包结构：默认的包扫描规则

- - 主程序(`main方法`)所在包及其下面的所有子包里面的组件都会被默认扫描进来
  - 无需以前的包扫描配置，即在主配置类中添加各种包扫描@ComponentScan
  - 想要改变扫描路径，
    - 第一种：@SpringBootApplication(scanBasePackages=**"com.aismall"**)

- - - 第二种：@ComponentScan 指定扫描路径

```java
@SpringBootApplication 等同于：
	
    @SpringBootConfiguration+@EnableAutoConfiguration+@ComponentScan("com.aismall.boot")
```

- 各种配置拥有默认值：可以通过配置文件修改这个值。

- - 默认配置最终都是映射到某个类上，如：MultipartProperties
  - 配置文件的值最终会绑定每个类上，这个类会在容器中创建对象

- 按需加载所有自动配置项，如果我们当前POM文件的dependency中不引用那个场景启动器，那个场景的启动器不会开启。

- - 非常多的starter
  - 引入了哪些场景这个场景的自动配置才会开启
  - SpringBoot所有的自动配置功能都在 `场景启动器.spring-boot-starter.spring-boot-autoconfigure` 包里面
  - 

- ......

#### 1.4.2、容器功能

##### 1.4.2.1、组件添加：spring注解版

@Configuration：表明一个类是配置类，给容器中添加组件

@Bean、@Component、@Controller、@Service、@Repository

@ComponentScan、@Import

@Conditional：按田间添加组件，判断为true的时候才执行

@ImportResource：原生配置文件引入

```java
@Configuration(proxyBeanMethods = true)
```

- **Full模式与Lite模式**

- - 配置类组件之间无依赖关系用Lite模式加速容器启动过程，减少判断
  - 配置类组件之间有依赖关系，方法会被调用得到之前单实例组件，用Full模式

```javascript
/*
 *1、配置类里面使用@Bean标注在方法上给容器注册组件，默认也是单实例的
 *
 *2、配置类本身也是组件
 *
 *3、默认@Configuration中的proxyBeanMethods属性为true：
 *    proxyBeanMethods：代理bean的方法
 *      Full(proxyBeanMethods = true)、保证每个@Bean方法被调用多少次返回的组件都是单实例的
 *      Lite(proxyBeanMethods = false)、每个@Bean方法被调用多少次返回的组件都是新创建的
 *      组件依赖必须使用Full模式默认。其他默认是否Lite模式
 *
 *4、@Import({User.class,Cat.class})
 *      给容器中自动创建出这两个类型的组件、默认组件的名字就是全类名
 *
 *5、@ImportResource("classpath:beans.xml")导入Spring的配置文件，
 *
 *6、按条件添加的注解，当容器中有这个Bean的时候才进行注入
 *  @ConditionalOnBean(value = {Cat.class})
 *
 * */

/*
 * @EnableConfigurationProperties(Car.class)：这个注解要写在配置类中
 * 1、开启Car配置绑定功能
 * 2、把这个Car这个组件自动注册到容器中*/

@Configuration //表明这是一个配置类
public class MainConfig01 {
    @Bean //给容器中添加组件。以方法名作为组件的id。返回类型就是组件类型。返回的值，就是组件在容器中的实例
    public User user(){
        return new User("AISMALL",18);
    }
    
    @ConditionalOnBean(value = {User.class})
    @Bean(value = "tomcat")
    public Cat cat(){
        return new Cat("tomcat");
    }
}
```

##### 1.4.2.2、原生配置文件引入

@ImportResource：原生配置文件引入

```java
@ImportResource("classpath:beans.xml")
```

##### 1.4.2.3、配置绑定

如何使用Java读取到properties文件中的内容，并且把它封装到JavaBean中，以供随时使用

- 之前的方法，读取配置文件中的内容

```java
/*获取配置类中的内容*/
public class GetProperties {
    public static void main(String[] args) throws FileNotFoundException,IOException{
        Properties pps = new Properties();
        String path="E:\\File\\Idea_File\\springboot2\\boot-01-helloworld-02\\src\\main\\resources\\application.properties";
        pps.load(new FileInputStream(path));
        Enumeration enum1 = pps.propertyNames();//得到配置文件的名字
        while(enum1.hasMoreElements()) {
            String strKey = (String) enum1.nextElement();
            String strValue = pps.getProperty(strKey);
            System.out.println(strKey + "=" + strValue);
            //封装到JavaBean。
        }
    }
}
```

- 使用注解：@Component+@ConfigurationProperties

```java
//在Car类上进行标注：自动获取配置文件中前缀为myCar的属性，绑定到Car的属性上
//注意：只有在容器中的组件才可以使用注解
@Component
@ConfigurationProperties(prefix = "myCar")
```

- 使用注解：@EnableConfigurationProperties + @ConfigurationProperties

```java
/*
 * @EnableConfigurationProperties(Car.class)：这个注解要写在配置类中
 * 1、开启Car配置绑定功能
 * 2、把这个Car这个组件自动注册到容器中*/

@EnableConfigurationProperties(Car.class):标注在配置类上
@ConfigurationProperties(prefix = "myCar")：标注在Bean类上
```

#### 1.4.3、自动配置原理入门

##### 1.4.3.1、引导加载自动配置类

毋庸置疑：先看启动main方法上的注解：`@SpringBootApplication`

```java
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(excludeFilters = { @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
        @Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
public @interface SpringBootApplication{}
```

- @SpringBootConfiguration

```
@Configuration。代表当前是一个配置类
```

- @ComponentScan

```
指定扫描哪些，Spring注解；
```

- @EnableAutoConfiguration：点进来看看

```java
@AutoConfigurationPackage
@Import(AutoConfigurationImportSelector.class)
public @interface EnableAutoConfiguration {}
```

- @AutoConfigurationPackage：点进来看看，自动配置包，指定了默认的包规则

```java
@Import(AutoConfigurationPackages.Registrar.class)  //给容器中导入一个组件
public @interface AutoConfigurationPackage {}
//利用Registrar给容器中导入一系列组件
//将指定的一个包下的所有组件导入进来？MainApplication 所在包下。
```

- @Import(AutoConfigurationImportSelector.class)

```
1、利用getAutoConfigurationEntry(annotationMetadata);给容器中批量导入一些组件
2、调用List<String> configurations = getCandidateConfigurations(annotationMetadata, attributes)获取到所有需要导入到容器中的配置类
3、利用工厂加载 Map<String, List<String>> loadSpringFactories(@Nullable ClassLoader classLoader)；得到所有的组件
4、从META-INF/spring.factories位置来加载一个文件。
    默认扫描我们当前系统里面所有META-INF/spring.factories位置的文件
    spring-boot-autoconfigure-2.3.4.RELEASE.jar包里面也有META-INF/spring.factories
```

```
文件里面写死了spring-boot一启动就要给容器中加载的所有配置类，在这个文件里面：
spring-boot-autoconfigure-2.3.4.RELEASE.jar/META-INF/spring.factories

里面一共100多个配置类
```

##### 1.4.3.2、按需开启自动配置项：条件装配

```java
虽然我们127个场景的所有自动配置，在项目启动的时候，默认全部加载。xxxxAutoConfiguration
但是，通过观察被自动加载的包中的类可以发现，里面的类是按需加载的，进行了条件控制，如果不导入满足条件的类，这个包就不会被加载
也就是：按照条件装配规则（@Conditional），最终会按需配置。

例如：org.springframework.boot.autoconfigure.aop.AopAutoConfiguration.AspectJAutoProxyingConfiguration
//没有Advice这个类这个AspectJAutoProxyingConfiguration就不会被加载
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(Advice.class)
static class AspectJAutoProxyingConfiguration {}
```

- 如何查看容器中有没有某个类：可以通过这个查看某个配置类是否会生效（条件判断）

```java
//main方法下
//判断容器中是否有某个类
String[] beanTypes=run.getBeanNamesForType(CacheAspectSupport.class);
System.out.println("========"+beanTypes.length+"=============");
```

##### 1.4.3.3、修改默认配置

```java
/*
功能：如果用户注册的MultipartResolver.class类型的组件的名字不是multipartResolver，则对其进行改名，改为multipartResolver
*/
@Bean
//容器中有这个类型组件:MultipartResolver.class
@ConditionalOnBean(MultipartResolver.class)  
//容器中没有这个名字:multipartResolver 组件
@ConditionalOnMissingBean(name = DispatcherServlet.MULTIPART_RESOLVER_BEAN_NAME)
//
public MultipartResolver multipartResolver(MultipartResolver resolver) {
    //给@Bean标注的方法传入了对象参数，这个参数的值就会从容器中找。MultipartResolver类型的值
    //在容器中找到这个类型的值，然后赋值给resolver，这时候MultipartResolver类型组件的名字就变成了multipartResolver
    //可以防止有些用户配置的文件上传解析器不符合规范
       return resolver;
     }
```

`SpringBoot默认会在底层配好所有的组件。但是如果用户自己配置了以用户的优先`

```java
@Bean
@ConditionalOnMissingBean
//这个注解就有效解决了优先级问题，用户配了之后会注册到容器，如果容器中有这个类型的组件，SpringBoot的自动配置就不会生效
public CharacterEncodingFilter characterEncodingFilter() {
    }
```



总结：

- SpringBoot先加载所有的自动配置类 ：xxxxxAutoConfiguration
- 每个自动配置类按照条件进行生效，默认都会绑定配置文件指定的值。xxxxProperties里面拿。xxxProperties和配置文件进行了绑定
- 生效的配置类就会给容器中装配很多组件
- 只要容器中有这些组件，相当于这些功能就有了
- 定制化配置

- - 用户直接自己@Bean替换底层的组件
  - 用户去看这个组件是获取的配置文件什么值就去修改。

**xxxxxAutoConfiguration ---> 组件  --->** **xxxxProperties里面拿值  ----> application.properties**

- 组件需要的值都被抽取出来了，我们直接修改配置文件，组件会去配置文件里面取值。

##### 1.4.3.4、最佳实践

- 引入场景依赖

- - SpringBoot所有支持的场景可以在参考文档中的这个章节：3.1.5. Starters查看到

- 查看自动配置了哪些（选做）

- - 自己分析，引入场景对应的自动配置一般都生效了
  - 配置文件中debug=true开启自动配置报告。Negative（不生效）\Positive（生效）

- 是否需要修改

- - 参照文档修改配置项

- - - https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html#common-application-properties
    - 自己分析。xxxxProperties绑定了配置文件的哪些。

- - 自定义加入或者替换组件

- - - @Bean、@Component。。。

- - 自定义器  **XXXXXCustomizer**；
  - ......

#### 14.4、开发小技巧

##### 1.4.4.1、Lombok

- 简化JavaBean开发

```xml
<!-- 第一步：引入依赖idea中搜索安装lombok插件-->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
</dependency>

<!-- 第二步：在idea中搜索安装lombok插件-->

安装完之后，项目在编译的时候会自动帮我们生成getter和setter方法，我们项目中的bean组件中不用再写了，看着舒服
```

```
@Data  :属性赋值
@ToString	:toString方法
@AllArgsConstructor	:全参构造
@NoArgsConstructor	:无参构造

@Slf4j	:简化日志开发，log.info("log....");
```

##### 1.4.4.2、dev-tools

- 项目或者页面修改以后，不用重启项目，使用：Ctrl+F9，就会生效

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

##### 1.4.4.3、Spring Initailizr（项目初始化向导）

- IDE都支持使用Spring的项目创建向导快速创建一个Spring Boot项目

- 在IDEA中使用`Spring Initializer`快速创建Spring Boot项目（`要联网`）

- 步骤：
  第一步：创建一个新项目(Create New Project)

  注意：Artifact中的内容不可以大小写混着写

  第二步：选择 Spring Initializer，最后点击Finish完成项目创建

- 如果没有`Spring Initializer`选项，解决办法如下：
  在settings -> Plugins 里面搜索spring boot，勾选上，然后再重启下idea，就可以了。

- 项目resource文件夹介绍
  `static`：保存所有的静态资源； js css images；
  `templates`：保存所有的模板页面，（Spring Boot默认jar包使用嵌入式的Tomcat，默认不支持JSP页面），可以使用模板引（freemarker、thymeleaf）
  `application.properties`：Spring Boot应用的配置文件，可以修改一些默认设置

## 2、SpringBoot2核心功能

### 2.1、配置文件

#### 2.1.1、文件类型

##### 2.1.1.1、properties

- 同之前的properties配置文件用法

##### 1.2.1.2、yaml

- 基本语法

```
k:(空格)v：表示一对键值对（空格必须有）；

如果有层级关系，以空格的缩进来控制层级关系，只要是左对齐的一列数据，都是同一个层级的

区分大小写
```

```xml
server:
	port: 8088
	path: /hello
```

- 数据类型
  - 字面量：单个的、不可再分的值。date、boolean、string、number、null。
    - 字符串`默认不用加上单引号或者双引号`，也可以加，但是单引号和双引号的功能有点区别
    - 双引号：会转义特殊字符
    - 单引号；不会转义字符串里面的特殊字符，特殊字符最终只是一个普通的字符串数据
  - 对象：键值对的集合。map、hash、set、object 
  - 数组：一组按次序排列的值。array、list、queue

  ```java
  字面量：
  	k: v
      
  对象：
   	行内写法：  
   		k: {k1:v1,k2:v2,k3:v3}
   	行间写法
  		k: 
    		   k1: v1
    		   k2: v2
    		   k3: v3
  数组：
  	行内写法：  
          k: [v1,v2,v3]
  	行间写法：
          k:
   	      - v1
   		  - v2
   		  - v3
  ```

  

#### 2.1.2、配置提示

- 自定义的类和配置文件绑定一般没有提示

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>


 <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>
                            <groupId>org.springframework.boot</groupId>
                            <artifactId>spring-boot-configuration-processor</artifactId>
                        </exclude>
                    </excludes>
                </configuration>
            </plugin>
        </plugins>
    </build>
```

#### 2.1.3、配置文件占位符

- 这个配置文件占位符的设置在`properties配置文件`中和`yml配置文件`中都可以使用
- 占位符的格式为：`${占位符}`
  例如：`person.last-name=小仙女${random.int[10,20]}`
- 随机数占位符

```
${random.value}
${random.int}：使用一个随机的整数来作为替代值
${random.long}：使用一个随机的长整数来作为替代值
${random.int(10)}：使用0-10之间的随机整数来作为替代值
${random.int[10,20]}：使用10-20之间的整数来最为替代值
```

- 占位符获取之前配置的值，`如果没有可以是用:指定默认值`

```
person.dog.name=${person.hello:hello}_dog
```

#### 2.1.4、配置文件中的Profile

- Profile：是Spring对不同环境提供不同配置功能的支持，可以通过激活指定参数等方式快速切换环境

- 1，多Profile模式：使用多个配置文件，默认使用application.properties配置文件

  - 格式：

  ```
  application-{profile}.properties/yml
  ```

  - 例如：

  ```
  application-dev.properties：开发环境
  application-prod.properties：生产环境
  ```

  2，多Profile模式（yml支持多文档块方式）：使用文档块,文档块之间使用`---`隔开

  - 注意：properties类型的配置文件不支持文档块模式，配置文件的优先级是perproties类型大于yml类型
  - 我们可以在默认环境的profiles属性中设置active属性用来激活不同的环境，如果这个值为空，就激活默认环境，不为空激活对应环境
  - 除了在配置文件里面使用active激活响应的环境之外，我们还有两种激活环境的方式：

  ```
  1.启动项目的时候设置虚拟机参数：
   -D:spring.profiles.active=环境名称
  
  2.将项目打成jar包，启动的时候设置参数
   java -jar 文件名.jar --spring.profiles.active=环境名称
  ```

  

  ```xml
  # 默认环境
  spring:
    profiles:
      active:
  
  server:
    port: 8081
  ---
  # 开发环境
  spring:
    profiles: dev
  
  server:
    port: 8081
  ---
  # 测试环境
  spring:
    profiles: test
  server:
    port: 8082
  ```

#### 2.1.5、配置文件加载位置

- springboot启动会扫描以下位置的application.properties或者application.yml文件作为Spring boot的`默认配置文件`

```
–项目根目录:./config/
–项目根目录:./
–classpath:/config/
–classpath:/
```

![20200831095705870](springboot02.assets/20200831095705870.png)



- 优先级由高到底，高优先级的配置会覆盖低优先级的配置
- `SpringBoot会从这四个位置全部加载主配置文件，互补配置`
- 我们还可以通过spring.config.location来改变默认的配置文件位置（`项目打包好之后使用此命令`）
  - 启动打包好的项目的时候来指定配置文件的新位置，`java -jar 文件名.jar --spring.config.location=G:/application.properties`，
  - 指定配置文件和默认加载的这些配置文件共同起作用形成互补配置

#### 2.1.6、对比分析

- `@Value`获取值和`@ConfigurationProperties`获取值比较

| @ConfigurationProperties | @Value                   |            |
| ------------------------ | ------------------------ | ---------- |
| 功能                     | 批量注入配置文件中的属性 | 一个个指定 |
| 松散绑定（松散语法）     | 支持                     | 不支持     |
| SpEL                     | 不支持                   | 支持       |
| JSR303数据校验           | 支持                     | 不支持     |
| 复杂类型封装             | 支持                     | 不支持     |

- 配置文件@ConfigurationProperties和@value注解的选择
  - 如果说，我们只是在某个业务逻辑中需要获取一下配置文件中的`某项值`，`使用@Value`
  - 如果说，我们专门编写了一个javaBean来和配置文件进行映射，我们就直接使用`@ConfigurationProperties`

- @PropertySource注解：

  - `@PropertySource`注解：加载指定的配置文件
  - 这个注解作用在Bean类上，这个Bean会加载指定的配置文件

- @ImportResource注解（`不推荐`）：

  - `@ImportResource`注解：`给springboot容器中添加组件`，导入Spring的配置文件，让配置文件里面的内容生效

  - 注意：Spring Boot里面没有Spring的配置文件，我们自己编写的配置文件，也不能自动识别，想让我们自己配置的Spring的配置文件生效可以使用@ImportResource注解，将这个注解标注在一个配置类上即可
  - 在resource文件夹下编写一个spring配置文件：`beans.xml`
  - 分析：
    - 其实就是先把要添加的组件(`bean`)配置到spring的配置文件中(`XML配置文件`)，然后通过在`配置类`上添加@ImportResource注解将XML配置文件中的bean添加到springboot的容器中。
    - 对比下面的`@Bean`注解会发现，上面的注解需要多写一个XML配置文件



### 2.2、web开发

#### 2.2.1、SpringMVC自动配置概览

- 参考文档：4.7：Developing Web Applications

- 参考文档：4.7.1： The “Spring Web MVC Framework”.Spring MVC Auto-configuration

- 文档原文：

```java
Spring Boot provides auto-configuration for Spring MVC that works well with most applications.
The auto-configuration adds the following features on top of Spring’s defaults:

  • Inclusion of ContentNegotiatingViewResolver and BeanNameViewResolver beans.
  • Support for serving static resources, including support for WebJars (covered later in this
document)).
  • Automatic registration of Converter, GenericConverter, and Formatter beans.
  • Support for HttpMessageConverters (covered later in this document).
  • Automatic registration of MessageCodesResolver (covered later in this document).
  • Static index.html support.
  • Automatic use of a ConfigurableWebBindingInitializer bean (covered later in this document).

If you want to keep those Spring Boot MVC customizations and make more MVC customizations
(interceptors, formatters, view controllers, and other features), you can add your own
@Configuration class of type WebMvcConfigurer but without @EnableWebMvc.

If  you  want  to  provide  custom  instances  of  RequestMappingHandlerMapping,
RequestMappingHandlerAdapter, or ExceptionHandlerExceptionResolver, and still keep the Spring Boot
MVC customizations, you can declare a bean of type WebMvcRegistrations and use it to provide
custom instances of those components.

If you want to take complete control of Spring MVC, you can add your own @Configuration
annotated with @EnableWebMvc, or alternatively add your own @Configuration-annotated
DelegatingWebMvcConfiguration as described in the Javadoc of @EnableWebMvc
```

- 文档翻译：

```
SpringBoot为SpringMVC提供了自动配置功能，可以很好地与大多数应用程序配合使用。自动配置在Spring默认设置的基础上添加了以下功能:
 
  •包括内  容协商视图解析器 和 BeanName视图解析器
  •支持提供静态资源，包括对WebJars的支持
  •自动注册 Converter，GenericConverter，Formatter
  •支持 HttpMessageConverters （后来我们配合内容协商理解原理）
  •自动注册 MessageCodesResolver （国际化用）
  •静态index.html 页支持
  •自定义 Favicon
  •自动使用 ConfigurableWebBindingInitializer ，（DataBinder负责将请求数据绑定到JavaBean上）

不用@EnableWebMvc注解。使用 @Configuration + WebMvcConfigurer 自定义规则

声明 WebMvcRegistrations 改变默认底层组件

使用 @EnableWebMvc+@Configuration+DelegatingWebMvcConfiguration 全面接管SpringMVC
```

#### 2.2.2、简单功能分析

##### 2.2.2.1、静态资源访问

- 静态资源目录

```
只要静态资源放在类路径下（resources文件夹下）：
	/static 
	/public
	/resources
	/META-INF/resources

访问 ：
	当前项目根路径/静态资源名
	例如：localhost:8080/public.jpg
```

```
原理： 
	请求进来，先去找Controller看能不能处理，就是看看@RestController中有没有对应的@RequestMapping("/resource")。
	不能处理的所有请求又都交给静态资源处理器。静态资源也找不到则响应404页面
	
	静态映射/**：双星是默认拦截所有的Controller处理不了的请求，
```

- 改变默认的静态资源路径：在配置文件中做如下配置，给静态资源访问加前缀
  - 注意：如果不配置。默认是无前缀的。
  - 配置完之后，访问静态资源必须是加上res前缀
  - 当前项目 + static-path-pattern + 静态资源名 
  - 例如：localhost:8080/res/public.jpg

```xml
spring:
  mvc:
    static-path-pattern: /res/**
```

- 指定静态资源的文件夹为：aismall：`其他的默认存放静态资源的文件夹失效`
  - 当前项目 + static-path-pattern + 静态资源名 ，访问方式不变，就是静态文件夹被指定了
  - 访问路径：localhost:8080/res/aismall.jpg
  - 注意：指定的静态资源文件夹可以为多个：`static-locations: [classpath:/aismall01,classpath:/aismall02]`

```
spring:
  mvc:
    static-path-pattern: /res/**
  resources:
    static-locations: classpath:/aismall
```

- webjar (了解)
  - 首先要添加依赖
  - 使用webjar自动映射： /webjars/**
  - 访问地址：http://localhost:8080/webjars/**jquery/3.5.1/jquery.js**  后面地址要按照依赖里面的包路径

```xml
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>jquery</artifactId>
    <version>3.5.1</version>
</dependency>
```

##### 2.2.2.2、欢迎页支持

- 欢迎页：就是访问项目根路径的时候回直接跳转到欢迎页

- SpringBoot支持两种方式的欢迎页：静态资源方式，模板方式

- 静态资源方式：静态资源路径下 ：index.html

- - 可以配置静态资源路径
  - 但是`不可以配置静态资源的访问前缀`，否则导致 index.html不能被默认访问
  - controller能处理 /index

```
spring:
  resources:
    static-locations: [classpath:/aismall]
```

##### 2.2.2.3、自定义 Favicon

- favicon.ico 放在静态资源目录下即可
- 注意：名字不能改
- 静态路径访问前缀会影像这个图标的加载，需要不指定前缀。

##### 2.2.2.4、静态资源配置原理

- 1、SpringBoot启动默认加载： xxxAutoConfiguration 类（自动配置类）

- 2、SpringMVC功能的自动配置类：  ，生效

```java
@Configuration(proxyBeanMethods = false)
//是不是web类型的应用，servlet就是web类型的
@ConditionalOnWebApplication(type = Type.SERVLET)
//有没有这几个类，导入有关web的依赖，就会包含这些类
@ConditionalOnClass({ Servlet.class, DispatcherServlet.class, WebMvcConfigurer.class })
//容器中没有这个类型的组件：WebMvcConfigurationSupport
@ConditionalOnMissingBean(WebMvcConfigurationSupport.class)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE + 10)
@AutoConfigureAfter({ DispatcherServletAutoConfiguration.class, TaskExecutionAutoConfiguration.class,
        ValidationAutoConfiguration.class })
public class WebMvcAutoConfiguration {}
```

- 给容器中配了什么，在这个类中有个静态内部类：WebMvcAutoConfigurationAdapter

```java
@Configuration(proxyBeanMethods = false)
@Import(EnableWebMvcConfiguration.class)
//和属性相关的配置类：这个注解将这两个类注入到容器中，用于绑定属性信息
@EnableConfigurationProperties({ WebMvcProperties.class, ResourceProperties.class })
@Order(0)
public static class WebMvcAutoConfigurationAdapter implements WebMvcConfigurer {}
```

- 配置文件的相关属性和xxx进行了绑定：
  - WebMvcProperties==**spring.mvc**
  - ResourceProperties==**spring.resources**

###### 1、配置类中只有一个有参构造器

- WebMvcAutoConfigurationAdapter：配置类只有一个有参构造器，`这个类的部分内容如下`：
- 有参构造器所有参数的值都会从容器中确定

```java
//获取和spring.resources绑定的所有的值的对象
ResourceProperties resourceProperties; 
//获取和spring.mvc绑定的所有的值的对象
WebMvcProperties mvcProperties;
//Spring的beanFactory
ListableBeanFactory beanFactory;
//找到所有的HttpMessageConverters
HttpMessageConverters 
//找到资源处理器的自定义器。
ResourceHandlerRegistrationCustomizer
//处理的路径
DispatcherServletPath  
// 给应用注册Servlet、Filter....
ServletRegistrationBean  

public WebMvcAutoConfigurationAdapter(ResourceProperties resourceProperties,
                                      WebMvcProperties mvcProperties,
                                      ListableBeanFactory beanFactory, 
                                      ObjectProvider<HttpMessageConverters> messageConvertersProvider,
                                      ObjectProvider<ResourceHandlerRegistrationCustomizer> resourceHandlerRegistrationCustomizerProvider,
                                      ObjectProvider<DispatcherServletPath> dispatcherServletPath,
                                      ObjectProvider<ServletRegistrationBean<?>> servletRegistrations) {
    this.resourceProperties = resourceProperties;
    this.mvcProperties = mvcProperties;
    this.beanFactory = beanFactory;
    this.messageConvertersProvider = messageConvertersProvider;
    this.resourceHandlerRegistrationCustomizer = resourceHandlerRegistrationCustomizerProvider.getIfAvailable();
    this.dispatcherServletPath = dispatcherServletPath;
    this.servletRegistrations = servletRegistrations;
        }
```

###### 2、资源处理的默认规则

- WebMvcAutoConfigurationAdapter. addResourceHandlers方法：

```java
@Override
public void addResourceHandlers(ResourceHandlerRegistry registry) {
    //这放方法返回false下面的配置不生效，所有的静态资源规则不生效
    if (!this.resourceProperties.isAddMappings()) {
        logger.debug("Default resource handling disabled");
        return;
    }
    //下面的代码都是配置静态资源规则的：
    //1.设置缓存的时间
    Duration cachePeriod = this.resourceProperties.getCache().getPeriod();
    CacheControl cacheControl = this.resourceProperties.getCache().getCachecontrol().toHttpCacheControl();
    //webjars的规则
    if (!registry.hasMappingForPattern("/webjars/**")) {
        customizeResourceHandlerRegistration(registry.addResourceHandler("/webjars/**")
                                             .addResourceLocations("classpath:/META-INF/resources/webjars/")
                                             .setCachePeriod(getSeconds(cachePeriod)).setCacheControl(cacheControl));
    }
    //静态资源路径配置规则，不配置默认值是/**
    String staticPathPattern = this.mvcProperties.getStaticPathPattern();
    if (!registry.hasMappingForPattern(staticPathPattern)) {
        customizeResourceHandlerRegistration(registry.addResourceHandler(staticPathPattern)
                                             .addResourceLocations(getResourceLocations(this.resourceProperties.getStaticLocations()))
                                             .setCachePeriod(getSeconds(cachePeriod)).setCacheControl(cacheControl));
    }
}
```

- ResourceProperties.staticLocations方法：

```java
private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/META-INF/resources/",
            "classpath:/resources/", "classpath:/static/", "classpath:/public/" };
private String[] staticLocations = CLASSPATH_RESOURCE_LOCATIONS;
```

- 配置静态资源规则

```java
spring:
  mvc:
    static-path-pattern: /res/**
  resources:
    add-mappings: false   禁用所有静态资源规则
```

###### 3、欢迎页的处理规则

- WebMvcAutoConfigurationAdapter.welcomePageHandlerMapping方法：
- HandlerMapping：处理器映射。保存了每一个Handler能处理哪些请求。  

```java
@Bean
public WelcomePageHandlerMapping welcomePageHandlerMapping(ApplicationContext applicationContext,
                                                           FormattingConversionService mvcConversionService, ResourceUrlProvider mvcResourceUrlProvider) {
    WelcomePageHandlerMapping welcomePageHandlerMapping = new WelcomePageHandlerMapping(
        new TemplateAvailabilityProviders(applicationContext), applicationContext, getWelcomePage(),
        this.mvcProperties.getStaticPathPattern());
    welcomePageHandlerMapping.setInterceptors(getInterceptors(mvcConversionService, mvcResourceUrlProvider));
    welcomePageHandlerMapping.setCorsConfigurations(getCorsConfigurations());
    return welcomePageHandlerMapping;
}

WelcomePageHandlerMapping(TemplateAvailabilityProviders templateAvailabilityProviders,
                          ApplicationContext applicationContext, Optional<Resource> welcomePage, String staticPathPattern) {
    if (welcomePage.isPresent() && "/**".equals(staticPathPattern)) {
        //要用欢迎页功能，静态文件路径必须是/**
        logger.info("Adding welcome page: " + welcomePage.get());
        setRootViewName("forward:index.html");
    }
    else if (welcomeTemplateExists(templateAvailabilityProviders, applicationContext)) {
        // 前面的if处理不了，就调用Controller看看能不能处理/index
        logger.info("Adding welcome page template: index");
        setRootViewName("index");
    }
}
```

#### 2.2.3、请求参数处理

##### 2.2.3.1、请求映射

###### 1、rest使用与原理

- Rest风格支持（使用**HTTP**请求方式动词来表示对资源的操作）

  - 以前：/getUser  ：获取用户，/deleteUser： 删除用户 ，/editUser：修改用户 ，/saveUser：保存用户
  - 现在： /user   GET：获取用户，  DELETE：删除用户，PUT：修改用户，POST：保存用户 
  - 表单只能发Get和Post类型的请求，后台会根据隐藏域中的_method的值来把请求方式替换掉，`这个替换需要手动开启`。

  ```html
  <form action="/user" method="get">
      <input value="REST-GET 提交" type="submit"/>
  </form>
  <form action="/user" method="post">
      <input value="REST-POST 提交" type="submit"/>
  </form>
  <form action="/user" method="post">
      <input name="_method" type="hidden" value="delete"/>
      <input value="REST-DELETE 提交" type="submit"/>
  </form>
  <form action="/user" method="post">
      <input name="_method" type="hidden" value="PUT"/>
      <input value="REST-PUT 提交" type="submit"/>
  </form>
  ```

  - 核心Filter：HiddenHttpMethodFilter：修改隐藏参数的类，需要在SpringBoot中手动配置才能开启

  ```java
  @Bean
  //如果我们手动配置了，这个自动配置就不会起作用
  @ConditionalOnMissingBean(HiddenHttpMethodFilter.class)
  @ConditionalOnProperty(prefix = "spring.mvc.hiddenmethod.filter", name = "enabled", matchIfMissing = false)
  public OrderedHiddenHttpMethodFilter hiddenHttpMethodFilter() {
      return new OrderedHiddenHttpMethodFilter();
  }
  ```

  ```
  #配置文件中
  spring:
      mvc:
          hiddenmethod:
            filter:
              enabled: true  #开启页面表单的Rest功能
  ```

  ```java
  @RequestMapping(value = "/user",method = RequestMethod.GET)
  public String getUser(){
      return "GET-张三";
  }
  
  @RequestMapping(value = "/user",method = RequestMethod.POST)
  public String saveUser(){
      return "POST-张三";
  }
  
  
  @RequestMapping(value = "/user",method = RequestMethod.PUT)
  public String putUser(){
      return "PUT-张三";
  }
  
  @RequestMapping(value = "/user",method = RequestMethod.DELETE)
  public String deleteUser(){
      return "DELETE-张三";
  }
  
  ```

  - 扩展：如何把_method 这个名字换成我们自己喜欢的。

  ```java
  //自定义filter,来更改这个值
  @Bean
  public HiddenHttpMethodFilter hiddenHttpMethodFilter(){
      HiddenHttpMethodFilter methodFilter = new HiddenHttpMethodFilter();
      methodFilter.setMethodParam("_m");
      return methodFilter;
  }
  ```

- Rest原理（表单提交要使用REST的时候）

  - 表单提交会带上**_method=PUT**
  - **请求过来被**HiddenHttpMethodFilter拦截

  - - 请求是否正常，并且是POST

  - - - 获取到**_method**的值。
      - 兼容以下请求；**PUT**.**DELETE**.**PATCH**
      - **原生request（post），包装模式requesWrapper重写了getMethod方法，返回的是传入的值。**
      - **过滤器链放行的时候用wrapper。以后的方法调用getMethod是调用****requesWrapper的。**

- RestFul风格注解替换注解：

```java
@RequestMapping(value = "/user",method = RequestMethod.GET)=@GetMapping("/user")

@RequestMapping(value = "/user",method = RequestMethod.POST)=@PostMapping("/user")

@RequestMapping(value = "/user",method = RequestMethod.PUT)=@PutMapping("/user")

@RequestMapping(value = "/user",method = RequestMethod.DELETE)=@DeleteMapping("/user")
```

- 使用客户端工具发送REST请求，就不需要Filter，此Filter之只对表单提交的REST风格的请求：
  - 如PostMan客户端发送工具：直接发送Put、delete等方式请求，无需Filter。
  - 因为表单只能写Get和Post请求，所以发送其他请求类型需要带隐藏值，被后端接收到后进行替换，客户端发送工具可以发送任何形式的请求，所以不需要进行包装，自然也就不需要拦截。
  - `所以这个Filter是选择性开启，如果不是做表单开发，就不需要开启`。

###### 2、请求映射原理

- 继承关系：HttpServlet-->HttpServletBean-->FrameworkServlet-->DispatcherServlet
- SpringMVC功能分析都从 :org.springframework.web.servlet.DispatcherServlet-->doDispatch()部分代码如下：

```java
protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpServletRequest processedRequest = request;
		HandlerExecutionChain mappedHandler = null;
		boolean multipartRequestParsed = false;

		WebAsyncManager asyncManager = WebAsyncUtils.getAsyncManager(request);

		try {
			ModelAndView mv = null;
			Exception dispatchException = null;

			try {
				processedRequest = checkMultipart(request);
				multipartRequestParsed = (processedRequest != request);

				// 决定哪个Controller处理当前请求，进入这个方法
				mappedHandler = getHandler(processedRequest);
				if (mappedHandler == null) {
					noHandlerFound(processedRequest, response);
					return;
				}

				//HandlerMapping：处理器映射。/xxx->>xxxx
```

```java
//DispatcherServlet.doDispatch()中
mappedHandler = getHandler(processedRequest);  //判断出有5个HandlerMapping，并进行遍历
//AbstractHandlerMapping.getHandler()中
HandlerExecutionChain handler = mapping.getHandler(request);
//AbstractHandlerMapping.getHandler()中
Object handler = getHandlerInternal(request);
//RequestMappingInfoHandlerMapping.getHandlerInternal()中
return super.getHandlerInternal(request);
//AbstractHandlerMethodMapping<T>.getHandlerInternal()中
String lookupPath = getUrlPathHelper().getLookupPathForRequest(request);
```

```java
//5个HandlerMapping
RequestMappingHandlerMapping
WelcomePageHandlerMapping
BeanNameUrlHandlerMapping
RouterFunctionMapping
SimpleUrlHandlerMapping
```

```java
RequestMappingHandlerMapping：保存了所有@RequestMapping 和handler的映射规则
//在DispatcherServlet.doDispatch()中
protected HandlerExecutionChain getHandler(HttpServletRequest request) throws Exception {
    	//判断出有5个HandlerMapping，并进行遍历
		if (this.handlerMappings != null) {
			for (HandlerMapping mapping : this.handlerMappings) {
                //第一次循环执行到这个位置的时候，获取到RequestMappingHandlerMapping
                //在Variables框中：mapping->mappingRegistry->mappingLookup中我们可以看到所有
                //使用@RequestMapping()注解注入的路径
				HandlerExecutionChain handler = mapping.getHandler(request);
				if (handler != null) {
					return handler;
				}
			}
		}
		return null;
	}
```

- 所有的请求映射都在HandlerMapping中。

  - SpringBoot自动配置欢迎页的 ：WelcomePageHandlerMapping 。访问 `/`能访问到index.html；
  - SpringBoot自动配置了默认的 ：RequestMappingHandlerMapping
  - 请求进来，挨个尝试所有的HandlerMapping看是否有请求信息。

  - - 如果有就找到这个请求对应的handler
    - 如果没有就是下一个 HandlerMapping

  - 我们需要一些自定义的映射处理，我们也可以自己给容器中放**HandlerMapping**。自定义 **HandlerMapping**

##### 2.2.3.2、普通参数与基本注解

###### 1、注解

```
@PathVariable：路径变量，获取请求路径中的变量值
@RequestHeader:获取请求头
@ModelAttribute
@RequestParam:获取请求参数
@MatrixVariable:矩阵变量
@CookieValue:获取cookie值
@RequestBody:获取请求体[POST]，只有post请求中才会有请求体
```

```java
/**
 * 数据绑定：页面提交的请求数据（GET、POST）都可以和对象属性进行绑定
 */

@RestController
public class ParameterTestController {

    @PostMapping("/saveuser")
    public Person saveuser(Person person){
        return person;
    }

    //方法一：单个获取
    @GetMapping("/car/{id}/owner/{username}")
    public Map<String,Object> getCar(@PathVariable("id") Integer id,
                                     @PathVariable("username") String name,
                                     @RequestHeader("User-Agent") String userAgent,
                                     @RequestParam("age") Integer age,
                                     @RequestParam("inters") List<String> inters){
        //创建一个Map存放获取到的值
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        map.put("userAgent",userAgent);
        map.put("age",age);
        map.put("inters",inters);
        return map;
    }
    //方法二：使用map获取
    @GetMapping("/cat/{id}/owner/{username}")
    public Map<String,Object> getCat(@PathVariable Map<String,String> pv,
                                     @RequestHeader Map<String,String> header,
                                     @RequestParam Map<String,String> params,
                                     @CookieValue("NMTID") String _ga,
                                     @CookieValue("NMTID") Cookie cookie){
        //创建一个Map存放获取到的值
        Map<String,Object> map = new HashMap<>();
        map.put("pv",pv);
        map.put("headers",header);
        map.put("params",params);
        map.put("_ga",_ga);
        System.out.println(cookie.getName()+"===>"+cookie.getValue());
        return map;
    }

    //测试获取请求体
    @PostMapping("/save")
    public Map postMethod(@RequestBody String content){
        Map<String,Object> map = new HashMap<>();
        map.put("content",content);
        return map;
    }

    //测试矩阵变量的功能
    //1、语法： 请求路径：/cars/sell;low=34;brand=byd,audi,yd
    //2、SpringBoot默认是禁用了矩阵变量的功能
    //      手动开启：原理。对于路径的处理。UrlPathHelper进行解析。
    //              removeSemicolonContent（移除分号内容）支持矩阵变量的
    //3、矩阵变量必须有url路径变量才能被解析
    @GetMapping("/cars/{path}")
    public Map carsSell(@MatrixVariable("low") Integer low,
                        @MatrixVariable("brand") List<String> brand,
                        @PathVariable("path") String path){
        Map<String,Object> map = new HashMap<>();
        map.put("low",low);
        map.put("brand",brand);
        map.put("path",path);
        return map;
    }

    // /boss/1;age=20/2;age=10
    @GetMapping("/boss/{bossId}/{empId}")
    public Map boss(@MatrixVariable(value = "age",pathVar = "bossId") Integer bossAge,
                    @MatrixVariable(value = "age",pathVar = "empId") Integer empAge){
        Map<String,Object> map = new HashMap<>();

        map.put("bossAge",bossAge);
        map.put("empAge",empAge);
        return map;
    }
}
```

###### 2、Servlet API：原生ServletAPI

- 下面的这些参数，如何解析：

```
WebRequest,	ServletRequest,	MultipartRequest,	HttpSessionjavax.servlet.http.PushBuilder,	Principal
InputStream,	Reader,	HttpMethod,	Locale,	TimeZone,	ZoneId
```

ServletRequestMethodArgumentResolver.supportsParameter()方法：解析上面的部分参数

```java
@Override
public boolean supportsParameter(MethodParameter parameter) {
    Class<?> paramType = parameter.getParameterType();
    return (WebRequest.class.isAssignableFrom(paramType) ||
            ServletRequest.class.isAssignableFrom(paramType) ||
            MultipartRequest.class.isAssignableFrom(paramType) ||
            HttpSession.class.isAssignableFrom(paramType) ||
            (pushBuilder != null && pushBuilder.isAssignableFrom(paramType)) ||
            Principal.class.isAssignableFrom(paramType) ||
            InputStream.class.isAssignableFrom(paramType) ||
            Reader.class.isAssignableFrom(paramType) ||
            HttpMethod.class == paramType ||
            Locale.class == paramType ||
            TimeZone.class == paramType ||
            ZoneId.class == paramType);
}
```

###### 3、复杂参数

```
Map、Model（map、model里面的数据会被放在request的请求域  request.setAttribute）、Errors/BindingResult、RedirectAttributes（ 重定向携带数据）、ServletResponse（response）、SessionStatus、UriComponentsBuilder、ServletUriComponentsBuilder
```

```
Map<String,Object> map,  Model model, HttpServletRequest request 都是可以给request域中放数据，
request.getAttribute();
```

```
Map、Model类型的参数，会返回 mavContainer.getModel（）；---> BindingAwareModelMap 是Model 也是Map
mavContainer.getModel(); 获取到值的
```

###### 4、自定义对象参数

- 可以自动类型转换与格式化，可以级联封装。

```java
/**
 *     姓名： <input name="userName"/> <br/>
 *     年龄： <input name="age"/> <br/>
 *     生日： <input name="birth"/> <br/>
 *     宠物姓名：<input name="pet.name"/><br/>
 *     宠物年龄：<input name="pet.age"/>
 */
@Data
public class Person {
    
    private String userName;
    private Integer age;
    private Date birth;
    private Pet pet;
    
}

@Data
public class Pet {

    private String name;
    private String age;

}

result
```

##### 2.2.3.3、POJO封装过程



##### 2.2.3.4、参数处理原理

- HandlerMapping中找到能处理请求的Handler（Controller.method()）

- 为当前Handler 找一个适配器 HandlerAdapter； **RequestMappingHandlerAdapter**

- 适配器执行目标方法并确定方法参数的每一个值

#### 2.2.4、数据响应与内容协商

- 响应：Response

- 数据响应分为：响应页面（发送请求返回页面），响应数据（发送请求返回数据）
- 响应数据有很多种形式：JSON(目前常用)，XML，XLS，图片。音频，视屏，自定义协议数据等。
- 目前的前后端分离项目，一般都是前端发来请求，后端给前端返回数据，前端拿到数据只后进行解析，然后填充页面。

##### 2.2.4.1、响应JSON

###### 1、@ResponseBody

- 首先我们要引用有关web场景的启动器，这个启动器会帮我们自动引入有关JSON场景的。

```xml
<!--web场景自动引入了json场景-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-json</artifactId>
    <version>2.3.4.RELEASE</version>
    <scope>compile</scope>
</dependency>
```

- 使用方式：在方法上标注注解：@ResponseBody

```java
@ResponseBody  //利用返回值处理器里面的消息转换器进行处理
@GetMapping(value = "/test/person")
public Person getPerson(){
    Person person = new Person();
    person.setAg e(18);
    person.setBirth(new Date());
    person.setUserName("aismall");
    return person;
}
```

- 我们使用`返回值解析器`来处理返回的数据，并封装成JSON数据

- 返回值解析器原理：
  - 1、返回值处理器判断是否支持这种类型返回值 supportsReturnType
  - 2、返回值处理器调用 handleReturnValue 进行处理
  - 3、RequestResponseBodyMethodProcessor 可以处理返回值标了@ResponseBody 注解的。

  - - 1、利用 MessageConverters 进行处理 将数据写为json

  - - - 1、内容协商（浏览器默认会以请求头的方式告诉服务器他能接受什么样的内容类型）
      - 2、服务器最终根据自己自身的能力，决定服务器能生产出什么样内容类型的数据，
      - 3、SpringMVC会挨个遍历所有容器底层的 HttpMessageConverter ，看谁能处理？

  - - - - 1、得到MappingJackson2HttpMessageConverter可以将对象写为json
        - 2、利用MappingJackson2HttpMessageConverter将对象转为json再写出去

###### 2、SpringMVC支持那些返回值解析器

- 支持那些返回值类型

```
ModelAndView
Model
View
ResponseEntity 
ResponseBodyEmitter
StreamingResponseBody
HttpEntity
HttpHeaders
Callable
DeferredResult
ListenableFuture
CompletionStage
WebAsyncTask
有 @ModelAttribute 且为对象类型的
@ResponseBody 注解 ---> RequestResponseBodyMethodProcessor；
```

##### 2.2.4.2、内容协商

###### 1、引入xml依赖

###### 2、postman分别测试返回json和xml

###### 3、开启浏览器参数方式内容协商功能

###### 4、内容协商原理

###### 5、自定义 MessageConverter

#### 2.2.5、视图解析与模板引擎

2.2.5.1、视图解析

2.2.5.2、模板引擎

2.2.5.3、thymeleaf使用(了解)

2.2.5.4、构建后台管理系统

#### 2.2.6、拦截器

##### 2.2.6.1、HandlerInterceptor 接口

##### 2.2.6.2、配置拦截器

##### 2.2.6.3、拦截器原理

#### 2.2.7、文件上传

##### 2.2.7.1、表单页面

##### 2.2.7.2、文件上传代码

##### 2.2.7.3、自动配置原理

#### 2.2.8、异常处理

#### 2.2.9、Web原生组件注入（Servlet、Filter、Listener）

#### 2.2.10、嵌入式Servlet容器

#### 2.2.11、定制化原理

### 2.3、数据访问

### 2.4、Junit5单元测试

### 2.5、生产指标监控

### 2.6、SpringBoot核心原理剖析

## 3、SpringBoot场景整合

### 3.1、虚拟化技术

### 3.2、安全控制

### 3.3、缓存技术

### 3.4、消息中间件

### 3.5、分布式入门

# 第二季：SpringBoot2响应是编程

## 1、响应式编程

### 响应式编程模型

### 使用Reactor开发

## 2、Webflux开发web应用

### Webflux构建Restful应用

### 函数式构建Restful应用

### RestTemplate与WebClient

## 3、响应式访问持久层

### 响应式访问MySql

### 响应式访问Redis

## 4、响应式安全开发

### spring Security Reactive 构建安全服务

## 5、响应式原理

### 几种IO模型

### Netty-Reactor

### 数据流处理原理























