package com.aismall.service.impl;

import com.aismall.dao.AccountDao;
import com.aismall.domain.Account;
import com.aismall.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("accountService")
public class AccountServiceImpl  implements AccountService {
    @Autowired
    private AccountDao accountDao;
    @Override
    public List<Account> findAll() {
        System.out.println("业务层查询所账户。。。。");
        return accountDao.findAll();
    }

    @Override
    public void saveAccount(Account account) {
        System.out.println("业务层保存账户信息。。。。");
        accountDao.saveAccount(account);
    }
}
