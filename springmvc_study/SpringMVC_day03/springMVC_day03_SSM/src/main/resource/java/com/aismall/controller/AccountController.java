package com.aismall.controller;

import com.aismall.domain.Account;
import com.aismall.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private AccountService accoutService;
    /**
     * 查询所有的数据
     * @return
     */
    @RequestMapping("/findAll")
    public String findAll(Model model) {
        System.out.println("控制类查询所有账户...");
        List<Account> list= accoutService.findAll();
        for (Account account : list) {
            System.out.println(account);
        }
        model.addAttribute("list",list);
        return "success";
    }
    /**
     * 保存账户*/
    @RequestMapping("/saveAccount")
    public String  saveAccount(Account account){
        System.out.println("控制类保存执行了，，，，");
        accoutService.saveAccount(account);
        return "success";
    }
}
