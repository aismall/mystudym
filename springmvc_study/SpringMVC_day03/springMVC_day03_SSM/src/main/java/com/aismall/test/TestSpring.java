package com.aismall.test;
import com.aismall.domain.Account;
import com.aismall.service.AccountService;
import com.aismall.service.impl.AccountServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class TestSpring {
    @Test
    public void testSpring01(){
        //加载spring的配置文件
        ApplicationContext ac=new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        //获取此注解下的实例
        AccountService as=(AccountService)ac.getBean("accountService");
        //调用此实例的方法
        as.findAll();
        }
    @Test
    public void test(){
        AccountServiceImpl ac=new AccountServiceImpl();
        ac.findAll();

    }
}
