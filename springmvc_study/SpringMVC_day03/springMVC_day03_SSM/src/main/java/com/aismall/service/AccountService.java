package com.aismall.service;

import com.aismall.domain.Account;

import java.util.List;

public interface AccountService  {
    //查询方法
    public List<Account> findAll();
    //保存账户信息
    public void saveAccount(Account account);
}
