package com.aismall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller("MyController")
@RequestMapping("/test")
public class MyController {
    @RequestMapping("/interception")
    public String testInterception()throws Exception{
        System.out.println("testInterception方法执行了，，，");
        return "success";
    }
}
