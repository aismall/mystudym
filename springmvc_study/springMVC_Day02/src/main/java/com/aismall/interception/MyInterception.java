package com.aismall.interception;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义拦截器*/
public class MyInterception implements HandlerInterceptor {
    /**
     * 拦截器接口中的预处理方法：在控制器方法执行前先执行
     * return true 放行，执行下一个拦截器，如果没有下一个拦截器，则执行controller中的方法
     * return false 不放行，不放行的时候需要使用形参request或者response进行页面跳转
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("preHandle方法执行了。。。。");
        return true;
    }

    /**
     * 拦截器接口中的方法：在控制器方法执行后再执行
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("postHandler方法执行了，，，，");
    }

    /**
     * 拦截器接口中的方法，在成功页面success.jsp执行后再执行
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("afterCompletion方法执行后再执行，，，，");
    }
}
