package com.aismall.controller;

import com.aismall.domain.Account;
import com.aismall.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.xml.transform.Source;
import java.util.List;

@Controller("UserController")
@RequestMapping(path="user")
public class UserController {
    @RequestMapping(path="userAccount")
    public String userAccount(User user){
        System.out.println("POJO 类中包含集合类型作为绑定参数");
        System.out.println(user.getUserName());
        List<Account> accounts=user.getAccounts();
        for (Account account:accounts) {
            System.out.println(account.getAccountName());
            System.out.println(account.getAccountId());
        }
        return  "success";
    }
}
