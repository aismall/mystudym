package com.aismall.domain;

import java.util.List;

public class User {
    private String userName;

    private List<Account> accounts;
    //添加Getter和Setter方法

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
