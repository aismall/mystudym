package com.aismall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/*第四步：创建对象*/
/**
 * 控制器类
 * */
@Controller("Hello")
public class HelloController {
    @RequestMapping(path="/hello")
    public String hello(){
        System.out.println("hello方法执行了。。。。");
        return "success";
    }
}
