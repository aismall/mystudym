package com.aismall.controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test {
    public static void func(int[] nums){
        int n=nums.length;
        int left=0;
        int right=0;
        while(right<n){
            if(nums[right]!=0){
                swaps(nums,left,right);
                left++;
            }
            right++;
        }
    }
    public static void swaps(int [] nums,int left,int right){
        int temp =nums[left];
        nums[left]=nums[right];
        nums[right]=temp;
    }

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
       int[] nums=new int[]{1,2,0,2,3,0};
       for(int i=0;i<nums.length;i++){
           System.out.print(nums[i]);
       }
        System.out.println();
       func(nums);
        for(int i=0;i<nums.length;i++){
            System.out.print(nums[i]);
        }
    }
}
