package com.aismall.controller;

import com.aismall.domain.Account;
        import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.*;

@Controller("AccountController")
@RequestMapping("/account")
public class AccountController {
    @RequestMapping("/addAccount")
    public String addAccount(Account account){
        System.out.println("accountName="+account.getAccountName());
        System.out.println("accountID="+account.getAccountId());
        return "success";
    }
    @ModelAttribute
    private Account findAccount (Integer accountId){
        System.out.println("findAccount方法执行了，，，");
        /*模拟数据库查询：根据此accountId查询到accountName如下*/
        String findAccountName="aaa";

        Account account=new Account();
        account.setAccountName(findAccountName);
        account.setAccountId(accountId);
        return account;
    }
}
