package com.aismall.domain;

public class Account {
    private String accountName;
    private Integer accountId;
    //添加Getter和Setter方法



    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}
