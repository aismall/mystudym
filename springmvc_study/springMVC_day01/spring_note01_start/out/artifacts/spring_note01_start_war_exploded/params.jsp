<%--
  Created by IntelliJ IDEA.
  User: AISMALL
  Date: 2020/6/14
  Time: 20:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Params</title>
    </head>
    <body>
    <%--请求参数绑定：把数据封装到方法参数列表中--%>
    <%--
    <a href="param/paramsMethod?username=hehe&password=123">请求参数绑定</a>
    --%>

    <%--请求参数绑定：把数据封装User类中--%>
    <%--
    <form action="params/saveUser" method="post">
        姓名：<input type="text" name="name" /><br/>
        年龄：<input type="text" name="age" /><br/>
        <input type="submit" value="提交" />
    </form>
    --%>

    <%--请求参数绑定：把数据封装Account_User类中--%>
    <%--
    <form action="params/saveAccount_User" method="post">
        姓名：<input type="text" name="username" /><br/>
        密码：<input type="text" name="password" /><br/>
        金额：<input type="text" name="money" /><br/>
        用户姓名：<input type="text" name="user.uname" /><br/>
        用户年龄：<input type="text" name="user.age" /><br/>
        <input type="submit" value="提交" />
    </form>
    --%>

    <%--把数据封装Account_Collection类中，类中存在list和map的集合--%>
    <%--
    <form action="param/saveAccount_Collection" method="post">
        姓名：<input type="text" name="username" /><br/>
        密码：<input type="text" name="password" /><br/>
        金额：<input type="text" name="money" /><br/>

        用户姓名：<input type="text" name="list[0].uname" /><br/>
        用户年龄：<input type="text" name="list[0].age" /><br/>

        用户姓名：<input type="text" name="map['one'].uname" /><br/>
        用户年龄：<input type="text" name="map['one'].age" /><br/>
        <input type="submit" value="提交" />
    </form>
    --%>

    <%--自定义类型转换器
    <form action="param/saveUser_Date" method="post">
        用户姓名：<input type="text" name="uname" /><br/>
        用户年龄：<input type="text" name="age" /><br/>
        用户生日：<input type="text" name="date" /><br/>
        <input type="submit" value="提交" />
    </form>
    --%>

    <a href="param/testServlet">Servlet原生的API</a>

    </body>
</html>
