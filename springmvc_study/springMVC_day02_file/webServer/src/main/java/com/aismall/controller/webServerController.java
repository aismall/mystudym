package com.aismall.controller;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Controller("webServerController")
@RequestMapping("/file")
public class webServerController {
    @RequestMapping("/fileUpload")
    public String fileUpload(MultipartFile upload)throws Exception{
        System.out.println("SpringMVC跨服务器文件上传");
        //定义上传服务器的路径
        String path="http://localhost:9090/uploads/";

        //获取上传文件的名称
        String filename=upload.getOriginalFilename();
        //把文件名称变成唯一值（避免上传时文件覆盖）
        String uuid= UUID.randomUUID().toString().replace("-","");
        //System.out.println(filename);

        //完成上传文件
        //创建客户端对象
        Client client=Client.create();
        //和文件服务器进行连接
        WebResource webResource=client.resource(path+filename);
        //上传文件
        webResource.put(upload.getBytes());
        return "success";
    }
}
